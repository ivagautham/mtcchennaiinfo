//
//  ViewControllerManager.m
//  MTCChennaiInfo
//
//  Created by Gautham on 23/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "ViewControllerManager.h"
#import "HomeViewController.h"
#import "StageViewController.h"
#import "RouteViewController.h"
#import "SearchViewController.h"
#import "MapViewController.h"
#import "AboutViewController.h"
#import "KeyLegendViewController.h"
#import "MRTSViewController.h"
#import "MRTSMapViewController.h"
#import "AutoViewController.h"

@implementation ViewControllerManager
@synthesize homeViewController;
@synthesize stageViewController;
@synthesize routeViewController;
@synthesize searchViewController;
@synthesize mapViewController;
@synthesize aboutViewController;
@synthesize keyLegendViewController;
@synthesize MRTSViewController;
@synthesize MRTSMapViewController;
@synthesize autoViewController;

-(HomeViewController *)getHomeViewController
{
    if(!self.homeViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"HomeViewController@2x";
        else
            nibName = @"HomeViewController";
        
        HomeViewController *_homeViewController = [[HomeViewController alloc]initWithNibName:nibName bundle:nil];
        self.homeViewController = _homeViewController;
        _homeViewController = nil;
    }
    return self.homeViewController;
}

-(StageViewController *)getStageViewController
{
    if(!self.stageViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"StageViewController@2x";
        else
            nibName = @"StageViewController";
        
        StageViewController *_stageViewController = [[StageViewController alloc]initWithNibName:nibName bundle:nil];
        self.stageViewController = _stageViewController;
        _stageViewController = nil;
    }
    return stageViewController;
}

-(RouteViewController *)getRouteViewController
{
    if(!self.routeViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"RouteViewController@2x";
        else
            nibName = @"RouteViewController";
        
        RouteViewController *_routeViewController = [[RouteViewController alloc]initWithNibName:nibName bundle:nil];
        self.routeViewController = _routeViewController;
        _routeViewController = nil;
    }
    return routeViewController;
}

-(SearchViewController *)getSearchViewController
{
    if(!self.searchViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"SearchViewController@2x";
        else
            nibName = @"SearchViewController";
        
        SearchViewController *_searchViewController = [[SearchViewController alloc]initWithNibName:nibName bundle:nil];
        self.searchViewController = _searchViewController;
        _searchViewController = nil;
    }
    return searchViewController;
}

-(MapViewController *)getMapViewController
{
    if(!self.mapViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"MapViewController@2x";
        else
            nibName = @"MapViewController";
        
        MapViewController *_mapViewController = [[MapViewController alloc]initWithNibName:nibName bundle:nil];
        self.mapViewController = _mapViewController;
        _mapViewController = nil;
    }
    return mapViewController;
}

-(AboutViewController *)getAboutViewController
{
    if(!self.aboutViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"AboutViewController@2x";
        else
            nibName = @"AboutViewController";
        
        AboutViewController *_aboutViewController = [[AboutViewController alloc]initWithNibName:nibName bundle:nil];
        self.aboutViewController = _aboutViewController;
        _aboutViewController = nil;
    }
    return aboutViewController;
}

-(KeyLegendViewController *)getKeyLegendViewController
{
    if(!self.keyLegendViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"KeyLegendViewController@2x";
        else
            nibName = @"KeyLegendViewController";
        
        KeyLegendViewController *_keyLegendViewController = [[KeyLegendViewController alloc]initWithNibName:nibName bundle:nil];
        self.keyLegendViewController = _keyLegendViewController;
        _keyLegendViewController = nil;
    }
    return keyLegendViewController;
}

-(MRTSViewController *)getMRTSViewController
{
    if(!self.MRTSViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"MRTSViewController@2x";
        else
            nibName = @"MRTSViewController";
        
        MRTSViewController *_MRTSViewController = [[MRTSViewController alloc]initWithNibName:nibName bundle:nil];
        self.MRTSViewController = _MRTSViewController;
        _MRTSViewController = nil;
    }
    return MRTSViewController;

}

-(MRTSMapViewController *)getMRTSMapViewController
{
    if(!self.MRTSMapViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"MRTSMapViewController@2x";
        else
            nibName = @"MRTSMapViewController";
        
        MRTSMapViewController *_MRTSMapViewController = [[MRTSMapViewController alloc]initWithNibName:nibName bundle:nil];
        self.MRTSMapViewController = _MRTSMapViewController;
        _MRTSMapViewController = nil;
    }
    return MRTSMapViewController;
}

-(AutoViewController *)getAutoViewController
{
    if(!self.autoViewController)
    {
        if (IS_IPHONE_5)
            nibName = @"AutoViewController@2x";
        else
            nibName = @"AutoViewController";
        
        AutoViewController *_AutoViewController = [[AutoViewController alloc]initWithNibName:nibName bundle:nil];
        self.autoViewController = _AutoViewController;
        _AutoViewController = nil;
    }
    return autoViewController;
}

@end
