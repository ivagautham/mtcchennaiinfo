//
//  Singleton.m
//  MTCChennaiInfo
//
//  Created by Gautham on 12/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "Singleton.h"

static Singleton* instance;

@implementation Singleton
@synthesize allRoutes;
@synthesize routeInfo;
@synthesize routeDetails;
@synthesize allStages;
@synthesize stageDetails;
@synthesize stageBuses;
@synthesize routeTypes;
@synthesize bgColor;
@synthesize elementColor1;
@synthesize elementColor2;
@synthesize textColor1;
@synthesize textColor2;
@synthesize allMRTSStations;
@synthesize stationDetails;
@synthesize allMRTSRoutes;
@synthesize allMRTSStationsDetails;
#pragma mark -

+ (Singleton *) getSingletonInstance
{
	@synchronized([Singleton class])
    {
		if ( instance == nil )
        {
			instance = [[Singleton alloc] init];
		}
	}
	return instance;
}

#pragma mark -
#pragma mark Global Methods

- (BOOL)isConnectedToInternet
{
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
        return NO;
    
    return YES;
}

-(NSDate *)getCurrentDeviceTime
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE,MM-dd-yyyy HH:mm:ss"];
    [dateFormat stringFromDate:date];
    return date;
}

-(void)showNoInternetAlert
{
    UIAlertView *noInternetAlertView = [[UIAlertView alloc] initWithTitle:@"Action Failed" message:@"No Internet connection available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [noInternetAlertView show];
}

-(void) loadMTCDataFromResource
{
    //Loading MTC details
    NSString* path = [[NSBundle mainBundle] pathForResource:@"Route" ofType:@"json"];
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [allRoutes removeAllObjects];
    NSError *jsonError;
    allRoutes = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_ROUTE object:nil];
    
    path = [[NSBundle mainBundle] pathForResource:@"RouteInfo" ofType:@"json"];
    content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [routeInfo removeAllObjects];
    routeInfo = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_ROUTE_INFO object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_ROUTE_DUMP object:nil];
    
    path = [[NSBundle mainBundle] pathForResource:@"Stage" ofType:@"json"];
    content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [allStages removeAllObjects];
    allStages = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_STAGE object:nil];
    
    path = [[NSBundle mainBundle] pathForResource:@"RouteByPlace" ofType:@"json"];
    content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [stageBuses removeAllObjects];
    stageBuses = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_STAGE_INFO object:nil];
    
    path = [[NSBundle mainBundle] pathForResource:@"StageDump" ofType:@"json"];
    content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    data = [content dataUsingEncoding:NSUTF8StringEncoding];
    [stageDetails removeAllObjects];
    stageDetails = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    
    if (!routeTypes)
        routeTypes = [[NSMutableArray alloc] init];
    [routeTypes removeAllObjects];
    
    for (NSDictionary *dict in routeInfo)
    {
        if (![routeTypes containsObject:[dict valueForKey:@"field_stage_type"]])
            [routeTypes addObject:[dict valueForKey:@"field_stage_type"]];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_STAGE_DUMP object:nil];
}

-(void) loadMRTSDataFromResource
{
    //Loading MRTS details
    NSString * path = [[NSBundle mainBundle] pathForResource:@"StationsDetails" ofType:@"json"];
    NSString * content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSData* data = [content dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSMutableArray *stnDtls = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError] mutableCopy];
    
    path = [[NSBundle mainBundle] pathForResource:@"mrts" ofType:@"db"];
    SQLHelper *sqh = [[SQLHelper alloc] initWithContentsOfFile:path];
	if (!allMRTSStations)
    {
        allMRTSStations = [[NSMutableArray alloc] initWithArray:[sqh executeQuery:@"SELECT * FROM mrts_station"]];
    }
    else
    {
        [allMRTSStations removeAllObjects];
        [allMRTSStations addObjectsFromArray:[sqh executeQuery:@"SELECT * FROM mrts_station"]];
    }
    //use addEntriesFromDictionary
    for (NSMutableDictionary *dict1  in allMRTSStations)
    {
        NSString *name1 = [dict1 valueForKey:@"station_name"];
        NSMutableDictionary *toRemove = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *toRemove2 = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *toUpdate = [[NSMutableDictionary alloc] init];
        for (NSMutableDictionary *dict2 in stnDtls)
        {
            [toRemove removeAllObjects];
            [toRemove2 removeAllObjects];
            [toUpdate removeAllObjects];
            
            NSString *name2 = [dict2 valueForKey:@"name"];
            NSString *name3 = [dict2 valueForKey:@"station"];
            if ([name1 isEqualToString:name2]) {
                [toUpdate setValue:[[dict2 valueForKey:@"loc"] valueForKey:@"lat"] forKey:@"lattitude"];
                [toUpdate setValue:[[dict2 valueForKey:@"loc"] valueForKey:@"lng"] forKey:@"longitude"];
                [toUpdate setValue:[dict2 valueForKey:@"vicinity"] forKey:@"vicinity"];
                [dict1 addEntriesFromDictionary:toUpdate];
                break;
            }
            else if ([name1 isEqualToString:name3]) {
                [toUpdate setValue:[[dict2 valueForKey:@"loc"] valueForKey:@"lat"] forKey:@"lattitude"];
                [toUpdate setValue:[[dict2 valueForKey:@"loc"] valueForKey:@"lng"] forKey:@"longitude"];
                [toUpdate setValue:[dict2 valueForKey:@"vicinity"] forKey:@"vicinity"];
                [dict1 addEntriesFromDictionary:toUpdate];
                break;
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_STATION object:nil];
    
    
    if (!allMRTSRoutes)
    {
        allMRTSRoutes = [[NSMutableArray alloc] initWithArray:[sqh executeQuery:@"SELECT * FROM mrts_route"]];
    }
    else
    {
        [allMRTSRoutes removeAllObjects];
        [allMRTSRoutes addObjectsFromArray:[sqh executeQuery:@"SELECT * FROM mrts_route"]];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_STATION_DETAILS object:nil];
    
    if (!allMRTSStationsDetails)
    {
        allMRTSStationsDetails = [[NSMutableArray alloc] initWithArray:[sqh executeQuery:@"SELECT * FROM mrts_schedule"]];
    }
    else
    {
        [allMRTSStationsDetails removeAllObjects];
        [allMRTSStationsDetails addObjectsFromArray:[sqh executeQuery:@"SELECT * FROM mrts_schedule"]];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SUCCESS_FETCH_STATION_SCHEDULE object:nil];
}

-(void) setupColors
{
    if (!bgColor)
    {
        bgColor = [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(220.0/255.0) alpha:1];
        //bgColor = [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(0.0/255.0) alpha:1];
    }
    if (!elementColor1)
        elementColor1 = [[UIColor alloc] initWithRed:(70.0/255.0) green:(170.0/255.0) blue:(40.0/255.0) alpha:1];
        //elementColor1 = [[UIColor alloc] initWithRed:(2.0/255.0) green:(89.0/255.0) blue:(134.0/255.0) alpha:1];
    if (!elementColor2)
        elementColor2 = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:1];
    if (!textColor1)
        textColor1 = [[UIColor alloc] initWithRed:(85.0/255.0) green:(85.0/255.0) blue:(85.0/255.0) alpha:1];
    if (!textColor2)
        textColor2 = [[UIColor alloc] initWithRed:(132.0/255.0) green:(132.0/255.0) blue:(132.0/255.0) alpha:1];
}

-(UIFont *)getFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ChalkboardSE-Regular" size:size];
}

-(UIFont *)getBoldFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ChalkboardSE-Bold" size:size];
}

-(UIFont *)getItalicFontOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ChalkboardSE-Light" size:size];
}

-(NSString *)getServiceTypeforRoute:(NSString *)route
{
    for (NSDictionary *dict in routeInfo)
    {
        if ([[[dict objectForKey:FIELD_STAGE_NUMBER] uppercaseString] isEqual:[route uppercaseString]])
            return [[dict objectForKey:@"field_stage_type"] capitalizedString];
    }
    return nil;
}

#pragma mark -
#pragma mark Manager Instance

- (AppDelegate *)getAppDelegateInstance
{
    return ((AppDelegate *) [UIApplication sharedApplication].delegate);
}

-(FileManager *) getFileManagerInstance
{
    if(!mFileManager)
        mFileManager = [[FileManager alloc] init];
    
    return mFileManager;
}

-(WebScrapingManager *) getWebScrapingManagerInstance
{
    if(!mWebScrapingManager)
        mWebScrapingManager = [[WebScrapingManager alloc] init];
    
    return mWebScrapingManager;
}

-(ViewControllerManager *)getViewControllerManagerInstance
{
    if(!mViewControllerManager)
        mViewControllerManager = [[ViewControllerManager alloc] init];
    
    return mViewControllerManager;
}

@end
