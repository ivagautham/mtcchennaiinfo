//
//  Singleton.h
//  MTCChennaiInfo
//
//  Created by Gautham on 12/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "FileManager.h"
#import "Reachability.h"
#import "WebScrapingManager.h"
#import "Constants.h"
#import "ViewControllerManager.h"
#import "SQLHelper.h"

@interface Singleton : NSObject
{
    FileManager *mFileManager;
    WebScrapingManager *mWebScrapingManager;
    ViewControllerManager *mViewControllerManager;
    
    //MTC data
    NSMutableArray *allRoutes;
    NSMutableArray *routeInfo;
    NSMutableArray *routeDetails;
    
    NSMutableArray *allStages;
    NSMutableArray *stageDetails;
    NSMutableArray *stageBuses;
    
    NSMutableArray *routeTypes;
    
    //MRTS data
    NSMutableArray *allMRTSStations;
    NSMutableDictionary *stationDetails;
    NSMutableArray *allMRTSRoutes;
    NSMutableArray *allMRTSStationsDetails;


    UIColor *bgColor;
    UIColor *elementColor1;
    UIColor *elementColor2;
    UIColor *textColor1;
    UIColor *textColor2;
    
}

@property(nonatomic, strong) NSMutableArray *allRoutes;
@property(nonatomic, strong) NSMutableArray *routeInfo;
@property(nonatomic, strong) NSMutableArray *routeDetails;

@property(nonatomic, strong) NSMutableArray *allStages;
@property(nonatomic, strong) NSMutableArray *stageDetails;
@property(nonatomic, strong) NSMutableArray *stageBuses;

@property(nonatomic, strong) NSMutableArray *routeTypes;

@property(nonatomic, strong) NSMutableArray *allMRTSStations;
@property(nonatomic, strong) NSMutableArray *allMRTSRoutes;
@property(nonatomic, strong) NSMutableArray *allMRTSStationsDetails;
@property(nonatomic, strong) NSMutableDictionary *stationDetails;

@property(nonatomic, strong) UIColor *bgColor;
@property(nonatomic, strong) UIColor *elementColor1;
@property(nonatomic, strong) UIColor *elementColor2;
@property(nonatomic, strong) UIColor *textColor1;
@property(nonatomic, strong) UIColor *textColor2;


+(Singleton *) getSingletonInstance;

-(BOOL) isConnectedToInternet;
-(NSDate *) getCurrentDeviceTime;
-(void) showNoInternetAlert;
-(void) loadMTCDataFromResource;
-(void) loadMRTSDataFromResource;
-(void) setupColors;

-(UIFont *)getFontOfSize:(CGFloat)size;
-(UIFont *)getBoldFontOfSize:(CGFloat)size;
-(UIFont *)getItalicFontOfSize:(CGFloat)size;

-(FileManager *) getFileManagerInstance;
-(WebScrapingManager *) getWebScrapingManagerInstance;
-(ViewControllerManager *)getViewControllerManagerInstance;
@end
