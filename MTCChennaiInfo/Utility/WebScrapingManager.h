//
//  WebScrapingManager.h
//  MTCChennaiInfo
//
//  Created by Gautham on 12/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebScrapingManager : NSObject
{
    NSURLConnection *stageDetailConnection;
    NSURLConnection *routeDetailConnection;
    NSURLConnection *routeInfoDetailConnection;
    NSURLConnection *routeByPlaceDetailConnection;
    NSURLConnection *alternateNameDetailConnection;
    
    NSMutableData *stageData;
    NSMutableData *routeData;
    NSMutableData *routeInfoData;
    NSMutableData *routeByPlaceData;
    NSMutableData *alternateNameData;
    
    NSMutableArray *stageArray;
    NSMutableArray *routeArray;
    NSMutableArray *routeInfoArray;
    NSMutableArray *routeByPlaceArray;
    NSMutableArray *alternateNameArray;
    
}

-(void) fetchStageDetails;
-(void) fetchRouteDetails;
-(void) fetchRouteInfoDetails;
-(NSArray *) fetchRouteByPlaceDetails;
-(void) fetchAlternateNameDetails;


@end
