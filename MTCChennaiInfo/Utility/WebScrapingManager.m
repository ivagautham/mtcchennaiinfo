//
//  WebScrapingManager.m
//  MTCChennaiInfo
//
//  Created by Gautham on 12/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "WebScrapingManager.h"
#import "TFHpple.h"
#import "Singleton.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

static NSString* baseURL = @"http://www.mtcbus.org";
//static NSString* alternateURL = @"http://my.metrocommute.in";

@implementation WebScrapingManager

-(void)fetchStageDetails
{
    if (![mSingleton isConnectedToInternet])
    {
        [mSingleton showNoInternetAlert];
        return ;
    }
    
    NSURL *sourceUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Places.asp",baseURL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:sourceUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    stageDetailConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [stageDetailConnection start];
}

-(void)fetchRouteDetails
{
    if (![mSingleton isConnectedToInternet])
    {
        [mSingleton showNoInternetAlert];
        return ;
    }
    
    NSURL *sourceUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Routes.asp",baseURL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:sourceUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    routeDetailConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [routeDetailConnection start];
}

-(void)fetchRouteInfoDetails
{
    if (![mSingleton isConnectedToInternet])
    {
        [mSingleton showNoInternetAlert];
        return ;
    }
    if (!routeArray)
        return ;
    
    if(!routeInfoArray)
        routeInfoArray = [[NSMutableArray alloc] init];
    [routeInfoArray removeAllObjects];
    
    [self nextRouteInfoRequest];
}

-(void)nextRouteInfoRequest
{
    NSString *route = [routeArray objectAtIndex:0];
    NSURL *sourceUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Routes.asp?cboRouteCode=%@&submit=Search",baseURL,route]];
    NSURLRequest *request = [NSURLRequest requestWithURL:sourceUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    routeInfoDetailConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [routeArray removeObjectAtIndex:0];
    [routeInfoDetailConnection start];
}

-(NSArray *)fetchRouteByPlaceDetails
{
    if (![mSingleton isConnectedToInternet])
    {
        [mSingleton showNoInternetAlert];
        return nil;
    }
    if (!stageArray)
        return nil;
    
    if(!routeByPlaceArray)
        routeByPlaceArray = [[NSMutableArray alloc] init];
    [routeByPlaceArray removeAllObjects];
    
    for (NSString *stage in stageArray)
    {
        NSString *urlString = [stage mutableCopy];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"(" withString:@"%28"];
        urlString = [urlString stringByReplacingOccurrencesOfString:@")" withString:@"%29"];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"//" withString:@"%2F"];

        NSURL *sourceUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Places.asp?cboSourceStageName=%@&submit=Search.&cboDestStageName=",baseURL,urlString]];
        NSData *HtmlData = [NSData dataWithContentsOfURL:sourceUrl options:NSDataReadingMappedIfSafe error:nil];
        TFHpple *Parser = [TFHpple hppleWithHTMLData:HtmlData];
        NSString *XpathQueryString = @" //a | //td[@align='left'] ";
        NSArray *Nodes = [Parser searchWithXPathQuery:XpathQueryString];
        NSMutableArray *detailsArray = [[NSMutableArray alloc] init];
        for (TFHppleElement *element in Nodes)
        {
            NSString *sampleText = [element.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ];
            if (sampleText && sampleText.length > 0 )
                [detailsArray addObject:sampleText];
        }
        NSMutableDictionary *routeByPlaceDictionary = [[NSMutableDictionary alloc] init];
        [routeByPlaceDictionary setObject:stage forKey:FIELD_SOURCE_STAGE];
        [routeByPlaceDictionary setObject:@"" forKey:FIELD_DESTINATION_STAGE];
        NSMutableArray *listOfRoutes = [[NSMutableArray alloc] init];
        NSMutableDictionary *newRoute;// = [[NSMutableDictionary alloc] init];
        for (int i=0; i<detailsArray.count; i++)
        {
            int mod = i%4;
            switch (mod)
            {
                case 0:
                    newRoute = [[NSMutableDictionary alloc] init];
                    [newRoute removeAllObjects];
                    [newRoute setValue:[detailsArray objectAtIndex:i] forKey:FIELD_STAGE_NUMBER];
                    break;
                case 1:
                    [newRoute setValue:[detailsArray objectAtIndex:i] forKey:FIELD_STAGE_TYPE];
                    break;
                case 2:
                    [newRoute setValue:[detailsArray objectAtIndex:i] forKey:FIELD_STAGE_SOURCE];
                    break;
                case 3:
                    [newRoute setValue:[detailsArray objectAtIndex:i] forKey:FIELD_STAGE_DESTINATION];
                    [listOfRoutes addObject:newRoute];
                    break;
                default:
                    break;
            }
        }
        if ([listOfRoutes count] == 0) {
            NSLog(@"No Details for:%@",stage);
        }
        [routeByPlaceDictionary setObject:listOfRoutes forKey:FIELD_LIST_OF_ROUTES];
        [routeByPlaceArray addObject:routeByPlaceDictionary];
        NSLog(@"%@",routeByPlaceArray);
    }
    return routeByPlaceArray;
}

-(void) fetchAlternateNameDetails
{
    if (![mSingleton isConnectedToInternet])
    {
        [mSingleton showNoInternetAlert];
        return ;
    }
    
    //    if(!alternateNameArray)
    //        alternateNameArray = [[NSMutableArray alloc] init];
    //    [alternateNameArray removeAllObjects];
    ////http://my.metrocommute.in/StageList?s_offset=0
    //
    //    NSURL *sourceUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/StageList?s_offset=0",alternateURL]];
    //    NSData *HtmlData = [NSData dataWithContentsOfURL:sourceUrl options:NSDataReadingMappedIfSafe error:nil];
    //    TFHpple *Parser = [TFHpple hppleWithHTMLData:HtmlData];
    //    NSString *XpathQueryString = @" //td | //a ";
    //    NSArray *Nodes = [Parser searchWithXPathQuery:XpathQueryString];
    //    for (TFHppleElement *element in Nodes)
    //    {
    //        NSString *sampleText = element.text;
    //        NSLog(sampleText);
    //    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
{
    if(connection == stageDetailConnection)
    {
        if(!stageData)
            stageData = [[NSMutableData alloc]  init];
    }
    if(connection == routeDetailConnection)
    {
        if(!routeData)
            routeData = [[NSMutableData alloc]  init];
    }
    if(connection == routeInfoDetailConnection)
    {
        if(!routeInfoData)
            routeInfoData = [[NSMutableData alloc]  init];
    }
}

- (void)connection:(NSURLConnection *) connection didReceiveData:(NSData *)data {
    if(connection == stageDetailConnection)
    {
        if(!stageData)
            stageData = [[NSMutableData alloc]  init];
        [stageData appendData:data];
    }
    if(connection == routeDetailConnection)
    {
        if(!routeData)
            routeData = [[NSMutableData alloc]  init];
        [routeData appendData:data];
    }
    if(connection == routeInfoDetailConnection)
    {
        if(!routeInfoData)
            routeInfoData = [[NSMutableData alloc]  init];
        [routeInfoData appendData:data];
    }
}

-(void)connectionDidFinishLoading: (NSURLConnection *)connection
{
    if(connection == stageDetailConnection)
    {
        TFHpple *Parser = [TFHpple hppleWithHTMLData:stageData];
        
        NSString *XpathQueryString = @"//select/option";
        NSArray *Nodes = [Parser searchWithXPathQuery:XpathQueryString];
        if(!stageArray)
            stageArray = [[NSMutableArray alloc] init];
        [stageArray removeAllObjects];
        for (TFHppleElement *elemet in Nodes)
        {
            if(elemet.text && ![stageArray containsObject:elemet.text])
                [stageArray addObject:elemet.text];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOAD_STAGE_SUCCESS object:stageArray];
    }
    else if(connection == routeDetailConnection)
    {
        TFHpple *Parser = [TFHpple hppleWithHTMLData:routeData];
        
        NSString *XpathQueryString = @"//select/option";
        NSArray *Nodes = [Parser searchWithXPathQuery:XpathQueryString];
        if(!routeArray)
            routeArray = [[NSMutableArray alloc] init];
        [routeArray removeAllObjects];
        for (TFHppleElement *elemet in Nodes)
        {
            if(elemet.text && ![routeArray containsObject:elemet.text])
                [routeArray addObject:elemet.text];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOAD_ROUTE_SUCCESS object:routeArray];
    }
    else if(connection == routeInfoDetailConnection)
    {
        TFHpple *Parser = [TFHpple hppleWithHTMLData:routeInfoData];
        NSString *XpathQueryString = @" //td[@align='left'] | //td[@align='middle']";
        NSArray *Nodes = [Parser searchWithXPathQuery:XpathQueryString];
        NSMutableArray *detailsArray = [[NSMutableArray alloc] init];
        for (TFHppleElement *element in Nodes)
        {
            NSString *sampleText = [element.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ];
            if (sampleText && sampleText.length > 0 )
                [detailsArray addObject:sampleText];
        }
        NSMutableDictionary *routeNodeInfo = [[NSMutableDictionary alloc] init];
        [routeNodeInfo setObject:[detailsArray objectAtIndex:0] forKey:FIELD_STAGE_NUMBER];
        [routeNodeInfo setObject:[detailsArray objectAtIndex:1] forKey:FIELD_STAGE_TYPE];
        [routeNodeInfo setObject:[detailsArray objectAtIndex:2] forKey:FIELD_STAGE_SOURCE];
        [routeNodeInfo setObject:[detailsArray objectAtIndex:3] forKey:FIELD_STAGE_DESTINATION];
        [routeNodeInfo setObject:[detailsArray objectAtIndex:4] forKey:FIELD_STAGE_DURATION];
        [detailsArray removeObjectsInRange:NSMakeRange(0, 5)];
        [routeNodeInfo setObject:detailsArray forKey:FIELD_STAGE_ROUTE];
        
        [routeInfoArray addObject:routeNodeInfo];
        if (routeArray.count > 0)
        {
            [self performSelectorOnMainThread:@selector(nextRouteInfoRequest) withObject:nil waitUntilDone:NO];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOAD_ROUTE_INFO_SUCCESS object:routeInfoArray];
        }
    }
}
@end
