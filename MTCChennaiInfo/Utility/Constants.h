//
//  Constants.h
//  MTCChennaiInfo
//
//  Created by Gautham on 13/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <Foundation/Foundation.h>


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0f)

//constant macros
#define FIELD_STAGE_NUMBER          @"field_stage_number"
#define FIELD_STAGE_TYPE            @"field_stage_type"
#define FIELD_STAGE_SOURCE          @"field_stage_source"
#define FIELD_STAGE_DESTINATION     @"field_stage_destination"
#define FIELD_STAGE_DURATION        @"field_stage_duration"
#define FIELD_STAGE_ROUTE           @"field_stage_route"

#define FIELD_SOURCE_STAGE          @"field_source_stage"
#define FIELD_DESTINATION_STAGE     @"field_destination_stage"
#define FIELD_LIST_OF_ROUTES        @"field_list_of_routes"

#define FIELD_SEARCH_RESULT         @"field_search_result"
#define FIELD_SEARCH_TYPE           @"field_search_type"
#define FIELD_GOOGLE_PLACES_SEARCH  @"field_google_place_search"
#define FIELD_ROUTE_SEARCH          @"field_route_search"
#define FIELD_STAGE_SEARCH          @"field_stage_search"

#define STN_CGL2THI                 @"stn_cgl2thi"
#define STN_MSB2TBM                 @"stn_msb2tbm"
#define STN_MSB2VEL                 @"stn_msb2vel"
#define STN_MAS2TRT                 @"stn_mas2trt"
#define STN_TBM2CGL                 @"stn_tbm2cgl"

//notifications
#define NOTIFICATION_LOAD_STAGE_SUCCESS         @"notification_load_stage_success"
#define NOTIFICATION_LOAD_ROUTE_SUCCESS         @"notification_load_route_success"
#define NOTIFICATION_LOAD_ROUTE_INFO_SUCCESS         @"notification_load_route_info_success"

#define NOTIFICATION_SUCCESS_FETCH_ROUTE            @"notification_success_fetch_route"
#define NOTIFICATION_SUCCESS_FETCH_ROUTE_INFO       @"notification_success_fetch_route_info"
#define NOTIFICATION_SUCCESS_FETCH_ROUTE_DUMP       @"notification_success_fetch_route_dump"
#define NOTIFICATION_SUCCESS_FETCH_STAGE            @"notification_success_fetch_stage"
#define NOTIFICATION_SUCCESS_FETCH_STAGE_INFO       @"notification_success_fetch_stage_info"
#define NOTIFICATION_SUCCESS_FETCH_STAGE_DUMP       @"notification_success_fetch_dump"
#define NOTIFICATION_SUCCESS_FETCH_STATION          @"notification_success_fetch_station"
#define NOTIFICATION_SUCCESS_FETCH_STATION_DETAILS  @"notification_success_fetch_station_details"
#define NOTIFICATION_SUCCESS_FETCH_STATION_SCHEDULE  @"notification_success_fetch_station_schedule"

#define NOTIFICATION_HIDE_QOS                       @"notification_hide_qos"
#define NOTIFICATION_SHOW_QOS                       @"notification_show_qos"
#define NOTIFICATION_SHOW_LOADING_VIEW_1            @"notification_show_loading_view_1"
#define NOTIFICATION_SHOW_LOADING_VIEW_2            @"notification_show_loading_view_2"



