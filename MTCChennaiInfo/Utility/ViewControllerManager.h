//
//  ViewControllerManager.h
//  MTCChennaiInfo
//
//  Created by Gautham on 23/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HomeViewController;
@class StageViewController;
@class RouteViewController;
@class SearchViewController;
@class MapViewController;
@class AboutViewController;
@class KeyLegendViewController;
@class MRTSViewController;
@class MRTSMapViewController;
@class AutoViewController;

@interface ViewControllerManager : NSObject
{
    NSString *nibName;
}

@property(nonatomic) HomeViewController *homeViewController;
@property(nonatomic) StageViewController *stageViewController;
@property(nonatomic) RouteViewController *routeViewController;
@property(nonatomic) SearchViewController *searchViewController;
@property(nonatomic) MapViewController *mapViewController;
@property(nonatomic) AboutViewController *aboutViewController;
@property(nonatomic) KeyLegendViewController *keyLegendViewController;
@property(nonatomic) MRTSViewController *MRTSViewController;
@property(nonatomic) MRTSMapViewController *MRTSMapViewController;
@property(nonatomic) AutoViewController *autoViewController;

-(HomeViewController *)getHomeViewController;
-(StageViewController *)getStageViewController;
-(RouteViewController *)getRouteViewController;
-(SearchViewController *)getSearchViewController;
-(MapViewController *)getMapViewController;
-(AboutViewController *)getAboutViewController;
-(KeyLegendViewController *)getKeyLegendViewController;
-(MRTSViewController *)getMRTSViewController;
-(MRTSMapViewController *)getMRTSMapViewController;
-(AutoViewController *)getAutoViewController;

@end
