//
//  KeyLegendViewController.h
//  MTCChennaiInfo
//
//  Created by Gautham on 21/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Singleton.h"
#import "iCarousel.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@interface KeyLegendViewController : UIViewController<iCarouselDataSource, iCarouselDelegate>
{
    IBOutlet UITableView *keyTable;
    IBOutlet UIActivityIndicatorView *loading;
    IBOutlet UILabel *loadingLabel;
    NSMutableArray *keyResult;
    NSMutableArray *tempArray;
    
    IBOutlet iCarousel *carousel;
    
    NSMutableArray *allStages;
    NSMutableArray *allRoutes;
    NSMutableArray *routeInfo;
    NSMutableArray *stageDetails;
}

@property (nonatomic, strong) UITableView *keyTable;
@property (nonatomic, strong) UIActivityIndicatorView *loading;
@property (nonatomic, strong) UILabel *loadingLabel;
@property (strong, nonatomic) NSMutableArray *keyResult;
@property (strong, nonatomic) NSMutableArray *tempArray;

@property (nonatomic, strong) iCarousel *carousel;

@property(nonatomic, strong) NSMutableArray *allStages;
@property(nonatomic, strong) NSMutableArray *allRoutes;
@property(nonatomic, strong) NSMutableArray *routeInfo;
@property(nonatomic, strong) NSMutableArray *stageDetails;

-(IBAction)onStage:(id)sender;
-(IBAction)onRoute:(id)sender;

-(IBAction)onBack:(id)sender;

@end
