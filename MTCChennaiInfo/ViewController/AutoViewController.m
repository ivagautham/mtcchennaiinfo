//
//  AutoViewController.m
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 30-3-14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

#import "AutoViewController.h"
#import "Singleton.h"
#import "SearchViewController.h"
#import "KeyLegendViewController.h"

@interface AutoViewController ()
{
    BOOL didLoadfromResource;
    NSMutableArray *duplicateArray;
}

@end

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@implementation AutoViewController
@synthesize firstLaunch;
@synthesize mapView;
@synthesize currenDist;
@synthesize sourceField;
@synthesize destinationField;
@synthesize currentLocationBtn;
@synthesize swapLocationsBtn;
@synthesize searchBtn;
@synthesize fareBtn;
@synthesize nightServiceSwth;
@synthesize resultView;
@synthesize fareLabel;
@synthesize distanceLabel;
@synthesize searchAssistTable;
@synthesize infoView1;
@synthesize infoView2;
@synthesize infoButtonBtn;
@synthesize filteredListContent;
@synthesize stageDetails;
@synthesize sourcePoint;
@synthesize destinationPoint;
@synthesize loadingView;
@synthesize loadingText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.layer.cornerRadius = 10;
        UISwipeGestureRecognizer *swipeUpforHome = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onBack:)];
        swipeUpforHome.direction = UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer: swipeUpforHome];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (!didLoadfromResource)
        [self loadStagesFromResource];
    
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:YES];
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    firstLaunch=YES;
    
    infoView1.layer.cornerRadius = 5;
    infoView1.layer.borderWidth = 0.25;
    
    infoView2.layer.cornerRadius = 5;
    infoView2.layer.borderWidth = 0.25;
    
    sourceField.layer.cornerRadius = 5;
    sourceField.layer.borderWidth = 2;
    sourceField.layer.borderColor = mSingleton.elementColor1.CGColor;
    
    destinationField.layer.cornerRadius = 5;
    destinationField.layer.borderWidth = 2;
    destinationField.layer.borderColor = mSingleton.elementColor1.CGColor;
    
    searchAssistTable.layer.cornerRadius = 5;
    searchAssistTable.layer.borderWidth = 1;
    searchAssistTable.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    resultView.layer.cornerRadius = 5;
    fareLabel.layer.cornerRadius = 5;
    distanceLabel.layer.cornerRadius = 5;
    
    //[resultView setHidden:TRUE];
    
    UIImage *originalImage1 = [UIImage imageNamed:@"button-white-bg30.png"];
    UIEdgeInsets insets1 = UIEdgeInsetsMake(5, 5, 5, 5);
    UIImage *stretchableImage1 = [originalImage1 resizableImageWithCapInsets:insets1];
    [fareBtn setBackgroundImage:stretchableImage1 forState:UIControlStateNormal];
    
    UIImage *originalImage2 = [UIImage imageNamed:@"button-white-bg-highlighted30.png"];
    UIEdgeInsets insets2 = UIEdgeInsetsMake(5, 5, 5, 5);
    UIImage *stretchableImage2 = [originalImage2 resizableImageWithCapInsets:insets2];
    [fareBtn setBackgroundImage:stretchableImage2 forState:UIControlStateHighlighted];
    
    [sourceField setDelegate:self];
    [destinationField setDelegate:self];
    [searchAssistTable setDelegate:self];
    [searchAssistTable setDataSource:self];
    
    [loadingText setHidden:TRUE];
    [loadingView setHidden:TRUE];
    [loadingView stopAnimating];
    
    //[self.view setBackgroundColor:mSingleton.bgColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!didLoadfromResource)
        [self loadStagesFromResource];
    
    [locationManager startUpdatingLocation];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!didLoadfromResource)
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadStagesFromResource) object:nil];
    
    [locationManager stopUpdatingLocation];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(CGFloat)calculateFareForDistance:(CGFloat)distance isNight:(BOOL)isNightCharges
{
    CGFloat fare = 25.0;
    CGFloat journeyDistance = distance;
    
    //excluding minimal distance
    journeyDistance = journeyDistance - 1.8;
    if (journeyDistance <= 0)
    {
        if (isNightCharges)
            fare = fare + (fare/2.0);
        
        return fare;
    }
    
    journeyDistance = ceilf(journeyDistance);
    
    fare = fare + (journeyDistance * 12);
    
    //Considering traffic into account
    if (journeyDistance < 3.0)
        fare = fare + (fare/20.0);
    else if (journeyDistance < 10.0)
        fare = fare + (fare/15.0);
    else
        fare = fare + (fare/10.0);
    
    if (isNightCharges)
        fare = fare + (fare/2.0);
    
    return fare;
}

-(void)loadStagesFromResource
{
    didLoadfromResource = FALSE;
    
    [stageDetails removeAllObjects];
    stageDetails = [mSingleton.stageDetails mutableCopy];
    
    if (!filteredListContent)
        filteredListContent = [[NSMutableArray alloc] init];
    [filteredListContent removeAllObjects];
    
    for (NSDictionary *dict in stageDetails)
    {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        
        NSString *latitude = [childDict valueForKey:@"latitude"];
        NSString *longitude = [childDict valueForKey:@"latitude"];
        
        if ((![latitude isKindOfClass:[NSNull class]] && latitude) && (![longitude isKindOfClass:[NSNull class]] && longitude))
        {
            NSString *stage = [[dict valueForKey:@"fields"] valueForKey:@"display_name"];
            [filteredListContent addObject:[stage uppercaseString]];
        }
    }
    
    if (!duplicateArray)
        duplicateArray = [[NSMutableArray alloc] init];
    [duplicateArray removeAllObjects];
    [duplicateArray addObjectsFromArray:filteredListContent];
    
    didLoadfromResource = TRUE;
}

-(MapPoint *)getLocationDetailsForStage:(NSString *)stage
{
    MapPoint *Marker;
    if ([[stage uppercaseString] isEqual:@"CURRENT LOCATION"])
    {
        Marker = [[MapPoint alloc] init];
        Marker.name = @"Current Location";
        Marker.type = MapPointTypePlace;
        Marker.coordinate = CLLocationCoordinate2DMake(currentCentre.latitude, currentCentre.longitude);
        return Marker;
    }
    CLLocationDegrees latitude;
    CLLocationDegrees longitude;
    for (NSDictionary *dict in stageDetails)
    {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[MapPoint alloc] init];
            Marker.name = stage;
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                latitude = [[childDict valueForKey:@"latitude"] doubleValue];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                longitude = [[childDict valueForKey:@"longitude"] doubleValue];
            }
        }
    }
    Marker.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    return Marker;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [self.filteredListContent removeAllObjects]; // First clear the filtered array.
    for (NSString *section in duplicateArray) {
        if ([[section uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound)
        {
            [self.filteredListContent addObject:section];
        }
    }
    
    if([self.filteredListContent count] == 0)
    {
        [searchAssistTable setHidden:TRUE];
    }
    else
    {
        if (searchAssistTable.hidden)
            [searchAssistTable setHidden:FALSE];
    }
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSString *string in self.filteredListContent)
    {
        if ([string hasPrefix:searchText])
            [tempArray addObject:string];
    }
    
    [self.filteredListContent removeObjectsInArray:tempArray];
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [tempArray count])];
    [self.filteredListContent insertObjects:tempArray atIndexes:indexSet];
    
    [self.searchAssistTable reloadData];
}

-(IBAction)onBack:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromBottom;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    NSUInteger count = self.navigationController.viewControllers.count;
    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:(count - 2)];
    if ([vc isMemberOfClass:[SearchViewController class]] || [vc isMemberOfClass:[KeyLegendViewController class]])
    {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(count - 3)] animated:NO];
        return;
    }
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)onCurrentLocation:(id)sender
{
    [resultView setHidden:TRUE];
    [sourceField resignFirstResponder];
    [destinationField resignFirstResponder];
    
    if (!destinationPoint && sourcePoint && ![[sourceField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
    {
        destinationPoint = [[MapPoint alloc] init];
        MapPoint *swapMP = sourcePoint;
        destinationPoint = swapMP;
        sourcePoint = nil;
        
        [destinationField setText:destinationPoint.name];
        
        [sourceField setText:@"Current Location"];
    }
    
    if (!sourcePoint)
        sourcePoint = [[MapPoint alloc] init];
    
    [sourceField setTextColor:mSingleton.elementColor1];
    [sourceField setText:@"Current Location"];
    
    sourcePoint.name = @"Current Location";
    sourcePoint.type = MapPointTypePlace;
    sourcePoint.coordinate = CLLocationCoordinate2DMake(currentCentre.latitude, currentCentre.longitude);
    
    
    [self plotSorce:sourcePoint andDestination:destinationPoint];
}

-(IBAction)onSwapLocations:(id)sender
{
    [resultView setHidden:TRUE];
    [sourceField resignFirstResponder];
    [destinationField resignFirstResponder];
    
    if (!sourcePoint)
    {
        sourcePoint = [[MapPoint alloc] init];
        if (!destinationPoint)
        {
            destinationPoint = [[MapPoint alloc] init];
            [sourceField setText:@"Current Location"];
            
            sourcePoint.name = @"Current Location";
            sourcePoint.type = MapPointTypePlace;
            sourcePoint.coordinate = CLLocationCoordinate2DMake(currentCentre.latitude, currentCentre.longitude);
            
        }
        else
        {
            [destinationField setText:@"Current Location"];
            
            sourcePoint = destinationPoint;
            destinationPoint.name = @"Current Location";
            destinationPoint.type = MapPointTypePlace;
            destinationPoint.coordinate = CLLocationCoordinate2DMake(currentCentre.latitude, currentCentre.longitude);
        }
    }
    else if(destinationPoint)
    {
        NSString *tempString = sourceField.text;
        sourceField.text = destinationField.text;
        destinationField.text = tempString;
        
        MapPoint *swapMP = destinationPoint;
        destinationPoint = sourcePoint;
        sourcePoint = swapMP;
    }
    else if (sourcePoint)
    {
        if (!destinationPoint && ![[sourceField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
        {
            destinationPoint = [[MapPoint alloc] init];
            MapPoint *swapMP = sourcePoint;
            destinationPoint = swapMP;
            sourcePoint = nil;
            
            [destinationField setText:destinationPoint.name];
            
            [sourceField setText:@"Current Location"];
            
            sourcePoint = [[MapPoint alloc] init];
            sourcePoint.name = @"Current Location";
            sourcePoint.type = MapPointTypePlace;
            sourcePoint.coordinate = CLLocationCoordinate2DMake(currentCentre.latitude, currentCentre.longitude);
            
        }
    }
    
    if ([[sourceField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
        [sourceField setTextColor:mSingleton.elementColor1];
    else
        [sourceField setTextColor:[UIColor blackColor]];
    
    if ([[destinationField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
        [destinationField setTextColor:mSingleton.elementColor1];
    else
        [destinationField setTextColor:[UIColor blackColor]];
    
    [self plotSorce:sourcePoint andDestination:destinationPoint];
}

-(IBAction)onSearch:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.subtype = kCATransitionFromTop;
    transition.type = @"cameraIris";
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[SearchViewController class]]) {
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    
    SearchViewController *searchViewController = [[mSingleton getViewControllerManagerInstance] getSearchViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchViewController animated:NO];
    
}

-(IBAction)onCalculateFare:(id)sender
{
    [resultView setHidden:TRUE];
    
    if ((sourcePoint.coordinate.latitude == destinationPoint.coordinate.latitude) && (sourcePoint.coordinate.longitude == destinationPoint.coordinate.longitude))
    {
        destinationPoint = nil;
    }
    
    if ([mSingleton isConnectedToInternet] && sourcePoint && destinationPoint)
    {
        [self performSelectorOnMainThread:@selector(setResultViewHidden:) withObject:[NSNumber numberWithInt:0] waitUntilDone:YES];
        
        [self performSelectorInBackground:@selector(calculateInBG) withObject:nil];
        return;
    }
    
    if (sender && ![mSingleton isConnectedToInternet])
    {
        UIAlertView *noInternetAlert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Application cannot load few data since your iPhone is not connected to the Internet. \nYou can turn Internet on or off at Settings > Cellular" delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [noInternetAlert show];
        return;
    }
    
    if (!sourcePoint && sourceField.text.length > 0)
    {
        UIAlertView *sourceInvalid = [[UIAlertView alloc] initWithTitle:@"Invalid Place" message:[NSString stringWithFormat:@"Could not get the location of '%@'. \nDo you want to search this place", sourceField.text] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Search" ,nil];
        sourceInvalid.tag = 11;
        [sourceInvalid show];
        return;
    }
    
    if (!destinationPoint && destinationField.text.length > 0)
    {
        UIAlertView *destinationInvalid = [[UIAlertView alloc] initWithTitle:@"Invalid Place" message:[NSString stringWithFormat:@"Could not get the location of '%@'. \nDo you want to search this place", destinationField.text] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Search" ,nil];
        destinationInvalid.tag = 22;
        [destinationInvalid show];
        return;
    }
}

-(void)searchPlaceWithString:(NSString *)toSearch
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.subtype = kCATransitionFromTop;
    transition.type = @"cameraIris";
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[SearchViewController class]]) {
            SearchViewController *currentVC = (SearchViewController *)vc;
            [currentVC searchPlace:toSearch];
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    
    SearchViewController *searchViewController = [[mSingleton getViewControllerManagerInstance] getSearchViewController];
    [searchViewController searchPlace:toSearch];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchViewController animated:NO];
}


-(void)calculateInBG
{
    NSMutableArray *overlays = [[NSMutableArray alloc] init];
    BOOL status = [self showRouteFrom:sourcePoint to:destinationPoint inArray:overlays];
    [self.mapView performSelectorOnMainThread:@selector(removeOverlays:) withObject:self.mapView.overlays waitUntilDone:TRUE];
    if (!status)
    {
        [self performSelectorOnMainThread:@selector(setResultViewHidden:) withObject:[NSNumber numberWithInt:2] waitUntilDone:YES];
        return;
    }
    [self.mapView performSelectorOnMainThread:@selector(addOverlays:) withObject:overlays waitUntilDone:FALSE];
    
    CGFloat todalDistance = 0.0;
    if (sourcePoint && destinationPoint)
        todalDistance = [self getDistanceBetweenSource:sourcePoint andDestination:destinationPoint];
    
    
    if (todalDistance > 0.0)
    {
        [self performSelectorOnMainThread:@selector(setResultViewHidden:) withObject:[NSNumber numberWithInt:1] waitUntilDone:YES];
        
        CGFloat totalFare = [self calculateFareForDistance:todalDistance isNight:nightServiceSwth.isOn];
        [fareLabel setText:[NSString stringWithFormat:@"%.2f", totalFare]];
        [distanceLabel setText:[NSString stringWithFormat:@"%.2f km",todalDistance]];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(setResultViewHidden:) withObject:[NSNumber numberWithInt:2] waitUntilDone:YES];
    }
}

-(void)setResultViewHidden:(NSNumber *)number
{
    if ([number isEqualToNumber:[NSNumber numberWithInt:0]])
    {
        [resultView setHidden:TRUE];
        [loadingText setHidden:FALSE];
        [loadingText setText:@"Caclulating fare, Please wait..."];
        [loadingView setHidden:FALSE];
        [loadingView startAnimating];
    }
    else if ([number isEqualToNumber:[NSNumber numberWithInt:1]])
    {
        [resultView setHidden:FALSE];
        [loadingText setHidden:TRUE];
        [loadingView setHidden:TRUE];
        [loadingView stopAnimating];
    }
    else if ([number isEqualToNumber:[NSNumber numberWithInt:2]])
    {
        [resultView setHidden:TRUE];
        [loadingText setHidden:FALSE];
        [loadingText setText:@"Could not calculate fare, Please try again later"];
        [loadingView setHidden:TRUE];
        [loadingView stopAnimating];
    }
}

-(IBAction)onNightSwitch:(id)sender
{
    [self onCalculateFare:fareBtn];
}

-(CGFloat)getDistanceBetweenSource:(MapPoint *)source andDestination:(MapPoint *)destination
{
    CLLocationCoordinate2D sourceCoord = sourcePoint.coordinate;
    CLLocationCoordinate2D destiCoord = destination.coordinate;
    /*
     NSString *currentDistance = @"N/A";
     CGFloat todalDistance = 0.0;
     if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
     {
     CLLocation *placeLocation = [[CLLocation alloc] initWithLatitude:destiCoord.latitude longitude:destiCoord.longitude];
     CLLocation *currLocation = [[CLLocation alloc] initWithLatitude:sourceCoord.latitude longitude:sourceCoord.longitude];
     
     todalDistance = [placeLocation distanceFromLocation:currLocation]/1000;
     
     currentDistance = [NSString stringWithFormat:@"%f",todalDistance];
     
     NSString *distanceURL = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=walking&sensor=false",sourceCoord.latitude,sourceCoord.longitude,destiCoord.latitude,destiCoord.longitude];
     NSURL *googleAPIurl = [NSURL URLWithString:distanceURL];
     NSData* data = [NSData dataWithContentsOfURL: googleAPIurl];
     if (!data)
     return todalDistance;
     
     NSError* error;
     NSDictionary* json = [NSJSONSerialization
     JSONObjectWithData:data
     
     options:kNilOptions
     error:&error];
     
     NSArray *distArray = [json valueForKey:@"rows"];
     if ([distArray count] > 0)
     {
     NSDictionary *distDict = [distArray objectAtIndex:0];
     NSArray *distArray2 = [distDict valueForKey:@"elements"];
     if ([distArray2 count] > 0)
     {
     NSDictionary *distDict2 = [distArray2 objectAtIndex:0];
     currentDistance = [[distDict2 valueForKey:@"distance"] valueForKey:@"text"] ;
     todalDistance = [currentDistance floatValue];
     }
     }
     }
     return todalDistance;*/
    
    CGFloat todalDistance = 0.0;
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", sourceCoord.latitude, sourceCoord.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", destiCoord.latitude, destiCoord.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    NSError* error = nil;
    NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSASCIIStringEncoding error:&error];
    
    if (error)
        return todalDistance;
    
    NSRegularExpression *distRegEx=[NSRegularExpression regularExpressionWithPattern:@"\"([^\"]*)\""options:0 error:NULL];
    NSTextCheckingResult *distmatch=[distRegEx firstMatchInString:apiResponse options:0 range:NSMakeRange(0, [apiResponse length])];
    
    NSString *dist= [apiResponse substringWithRange:[distmatch rangeAtIndex:0]];
    dist = [dist stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    dist = [dist stringByReplacingOccurrencesOfString:@" " withString:@""];
    dist = [dist stringByReplacingOccurrencesOfString:@"(" withString:@""];
    dist = [dist stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSArray *components = [dist componentsSeparatedByString:@"/"];
    dist = [components objectAtIndex:0];
    
    NSRange beginningOfNumber = [dist rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if (beginningOfNumber.location == NSNotFound)
        return todalDistance;
    
    NSString *key           = [dist substringToIndex:beginningOfNumber.location];
    NSString *stringValue   = [dist substringFromIndex:beginningOfNumber.location];
    todalDistance = [key floatValue];
    
    if ([stringValue isEqualToString:@"m"])
        todalDistance = todalDistance/1000;
    
    return todalDistance;
}


-(IBAction)onInfo:(id)sender
{
    [infoView1 setHidden:!infoView1.hidden];
    [infoView2 setHidden:!infoView2.hidden];
}

#pragma mark-
-(void)plotStage:(MapPoint *)sourceMapPoint
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    
    MapPoint *placeObject = sourceMapPoint;
    
    [mapView addAnnotation:placeObject];
    [mapView selectAnnotation:placeObject animated:YES];
    
    [self centerMap];
}

-(void)plotSorce:(MapPoint *)sourceMapPoint andDestination:(MapPoint *)destinationMapPoint
{
    [mapView removeAnnotations:self.mapView.annotations];
    [mapView removeOverlays:self.mapView.overlays];
    if (sourceMapPoint)
    {
        if (sourceMapPoint.coordinate.latitude > 0 || sourceMapPoint.coordinate.longitude > 0)
        {
            MapPoint *placeObject = sourceMapPoint;
            [mapView addAnnotation:placeObject];
            [mapView selectAnnotation:placeObject animated:YES];
        }
        else
        {
            sourcePoint = nil;
        }
    }
    if (destinationMapPoint)
    {
        if (destinationMapPoint.coordinate.latitude > 0 || destinationMapPoint.coordinate.longitude > 0)
        {
            MapPoint *placeObject = destinationMapPoint;
            [mapView addAnnotation:placeObject];
            [mapView selectAnnotation:placeObject animated:YES];
        }
        else
        {
            destinationPoint = nil;
        }
    }
    
    [self centerMap];
    if (sourcePoint && destinationPoint)
    {
        [self onCalculateFare:nil];
        return;
    }
}

-(void)updateLocationDetails
{
    if ((sourcePoint.coordinate.latitude == destinationPoint.coordinate.latitude) && (sourcePoint.coordinate.longitude == destinationPoint.coordinate.longitude))
    {
        destinationPoint = nil;
    }
    
    if (sourcePoint)
        sourceField.text = sourcePoint.name;
    else
        sourceField.text = @"";
    
    if (destinationPoint)
        destinationField.text = destinationPoint.name;
    else
        destinationField.text = @"";
    
    if ([[sourceField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
        [sourceField setTextColor:mSingleton.elementColor1];
    else
        [sourceField setTextColor:[UIColor blackColor]];
    
    if ([[destinationField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
        [destinationField setTextColor:mSingleton.elementColor1];
    else
        [destinationField setTextColor:[UIColor blackColor]];
    
    [self plotSorce:sourcePoint andDestination:destinationPoint];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark tableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == searchAssistTable)
    {
        CGRect tableHeight = searchAssistTable.frame;
        if (IS_IPHONE_5)
        {
            if ([filteredListContent count] < 5)
            {
                tableHeight.size.height = 44 * [filteredListContent count];
            }
            else
            {
                tableHeight.size.height = 204;
                [searchAssistTable flashScrollIndicators];
            }
        }
        else
        {
            if ([filteredListContent count] < 3)
            {
                tableHeight.size.height = 44 * [filteredListContent count];
            }
            else
            {
                tableHeight.size.height = 116;
                [searchAssistTable flashScrollIndicators];
            }
        }
        searchAssistTable.frame = tableHeight;
        return [filteredListContent count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == searchAssistTable)
    {
        if (indexPath.row >= filteredListContent.count)
            return nil;
        
        static NSString * CellIdentifier = @"searchAssistTableCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        cell.textLabel.text = [filteredListContent objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        
        return cell;
    }
    return nil;
}

#pragma mark-
#pragma mark tableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [resultView setHidden:TRUE];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == searchAssistTable)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (!searchAssistTable.hidden)
            searchAssistTable.hidden = TRUE;
        
        if ([sourceField isFirstResponder])
        {
            sourceField.text = cell.textLabel.text;
            
            if ([[sourceField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
                [sourceField setTextColor:mSingleton.elementColor1];
            else
                [sourceField setTextColor:[UIColor blackColor]];
            
            if ([[destinationField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
                [destinationField setTextColor:mSingleton.elementColor1];
            else
                [destinationField setTextColor:[UIColor blackColor]];
            
            [sourceField resignFirstResponder];
            //[destinationField becomeFirstResponder];
            
            if (!sourcePoint)
                sourcePoint = [[MapPoint alloc] init];
            
            sourcePoint = [self getLocationDetailsForStage:[sourceField.text uppercaseString]];
            
        }
        else if ([destinationField isFirstResponder])
        {
            destinationField.text = cell.textLabel.text;
            
            if ([[sourceField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
                [sourceField setTextColor:mSingleton.elementColor1];
            else
                [sourceField setTextColor:[UIColor blackColor]];
            
            if ([[destinationField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
                [destinationField setTextColor:mSingleton.elementColor1];
            else
                [destinationField setTextColor:[UIColor blackColor]];
            
            [destinationField resignFirstResponder];
            
            if (!destinationPoint)
                destinationPoint = [[MapPoint alloc] init];
            
            destinationPoint = [self getLocationDetailsForStage:[destinationField.text uppercaseString]];
        }
        [self plotSorce:sourcePoint andDestination:destinationPoint];
    }
}

#pragma mark-
#pragma mark TextFiled Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint tableOffset = CGPointZero;
    if (textField == sourceField)
    {
        tableOffset = CGPointMake(10, 92);
        [sourceField setFont:[mSingleton getFontOfSize:14]];
        [sourceField setTextColor:[UIColor blackColor]];
        [self filterContentForSearchText:sourceField.text scope:nil];
    }
    else if (textField == destinationField)
    {
        tableOffset = CGPointMake(10, 136);
        if (destinationField.text.length == 0)
            destinationField.text = @"";
        
        [destinationField setFont:[mSingleton getFontOfSize:14]];
        [destinationField setTextColor:[UIColor blackColor]];
        
        [self filterContentForSearchText:destinationField.text scope:nil];
    }
    [searchAssistTable setFrame:CGRectMake(tableOffset.x, tableOffset.y, searchAssistTable.frame.size.width, searchAssistTable.frame.size.height)];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == sourceField || textField == destinationField)
        [self filterContentForSearchText:[textField.text stringByReplacingCharactersInRange:range withString:string] scope:nil];
    
    if (textField == sourceField && [[textField.text stringByReplacingCharactersInRange:range withString:string] length] == 0)
    {
        destinationField.text = @"";
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [resultView setHidden:TRUE];
    [textField resignFirstResponder];
    
    if (!searchAssistTable.hidden)
        searchAssistTable.hidden = TRUE;
    
    if (([self.filteredListContent count] > 0) && (![self.filteredListContent containsObject:[textField.text uppercaseString]]))
    {
        BOOL didgetPrefix = FALSE;
        for (NSString *string in self.filteredListContent)
        {
            if ([string hasPrefix:textField.text])
            {
                textField.text = string;
                didgetPrefix = TRUE;
                break;
            }
        }
        if (!didgetPrefix)
            textField.text = [self.filteredListContent objectAtIndex:0];
    }
    
    if (textField == sourceField)
    {
        if ([[sourceField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
            [sourceField setTextColor:mSingleton.elementColor1];
        else
            [sourceField setTextColor:[UIColor blackColor]];
        
        if ([[destinationField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
            [destinationField setTextColor:mSingleton.elementColor1];
        else
            [destinationField setTextColor:[UIColor blackColor]];
        
        [sourceField resignFirstResponder];
        [destinationField becomeFirstResponder];
        
        if (!sourcePoint)
            sourcePoint = [[MapPoint alloc] init];
        
        sourcePoint = [self getLocationDetailsForStage:[sourceField.text uppercaseString]];
    }
    else if (textField == destinationField)
    {
        if ([[sourceField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
            [sourceField setTextColor:mSingleton.elementColor1];
        else
            [sourceField setTextColor:[UIColor blackColor]];
        
        if ([[destinationField.text uppercaseString] isEqual:@"CURRENT LOCATION"])
            [destinationField setTextColor:mSingleton.elementColor1];
        else
            [destinationField setTextColor:[UIColor blackColor]];
        
        
        if (!destinationPoint)
            destinationPoint = [[MapPoint alloc] init];
        
        destinationPoint = [self getLocationDetailsForStage:[sourceField.text uppercaseString]];
    }
    [self plotSorce:sourcePoint andDestination:destinationPoint];
    
    return YES;
}

#pragma mark - CLLocationManager methods.
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
    {
        currentCentre = newLocation.coordinate;
    }
    else
    {
        currentCentre.latitude = 13.052413900000000000;
        currentCentre.longitude = 80.250824599999990000;
    }
}

#pragma mark - MKMapViewDelegate methods.

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    CLLocationCoordinate2D centre = [mv centerCoordinate];
    MKCoordinateRegion region;
    if (firstLaunch)
    {
        CLLocationCoordinate2D coord;
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
        {
            currentCentre.latitude = 13.052413900000000000;
            currentCentre.longitude = 80.250824599999990000;
            coord = currentCentre;
        }
        else
        {
            coord = locationManager.location.coordinate;
        }
        
        region = MKCoordinateRegionMakeWithDistance(coord,500,500);
        firstLaunch=NO;
    }
    else
    {
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
        {
            currentCentre.latitude = 13.052413900000000000;
            currentCentre.longitude = 80.250824599999990000;
            centre = currentCentre;
            currenDist = 30000;
        }
        //Set the center point to the visible region of the map and change the radius to match the search radius passed to the Google query string.
        region = MKCoordinateRegionMakeWithDistance(centre,currenDist,currenDist);
    }
    region.span.latitudeDelta = 0.5;
    region.span.longitudeDelta = 1.0;
    [mv setRegion:region animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    //Define our reuse indentifier.
    static NSString *identifier = @"MapPoint";
    if ([annotation isKindOfClass:[MapPoint class]])
    {
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        } else {
            annotationView.annotation = annotation;
        }
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.animatesDrop = YES;
        
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    //Get the east and west points on the map so we calculate the distance (zoom level) of the current map view.
    MKMapRect mRect = self.mapView.visibleMapRect;
    MKMapPoint eastMapPoint = MKMapPointMake(MKMapRectGetMinX(mRect), MKMapRectGetMidY(mRect));
    MKMapPoint westMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), MKMapRectGetMidY(mRect));
    
    //Set our current distance instance variable.
    currenDist = MKMetersBetweenMapPoints(eastMapPoint, westMapPoint);
    
    //Set our current centre point on the map instance variable.
    // currentCentre = self.mapView.centerCoordinate;
}

-(BOOL) showRouteFrom:(MapPoint*)f to:(MapPoint*)t inArray:(NSMutableArray *)overlays
{
    if (!(f && t))
        return FALSE;
    
    routes = [self calculateRoutesFrom:f.coordinate to:t.coordinate];
    if (!routes)
        return FALSE;
    
    NSInteger numberOfSteps = routes.count;
    
    CLLocationCoordinate2D coordinates[numberOfSteps];
    for (NSInteger index = 0; index < numberOfSteps; index++)
    {
        CLLocation *location = [routes objectAtIndex:index];
        CLLocationCoordinate2D coordinate = location.coordinate;
        coordinates[index] = coordinate;
    }
    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:coordinates count:numberOfSteps];
    [overlays addObject:polyLine];
    //[self.mapView addOverlay:polyLine level:MKOverlayLevelAboveLabels];
    return TRUE;
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    NSError* error = nil;
    NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSASCIIStringEncoding error:&error];
    
    if (error)
        return nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"points:\\\"([^\\\"]*)\\\"" options:0 error:NULL];
    NSTextCheckingResult *match = [regex firstMatchInString:apiResponse options:0 range:NSMakeRange(0, [apiResponse length])];
    NSString *encodedPoints = [apiResponse substringWithRange:[match rangeAtIndex:1]];
    return [self decodePolyLine:[encodedPoints mutableCopy]];
}

- (NSMutableArray *)decodePolyLine: (NSMutableString *)encoded
{
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\" options:NSLiteralSearch range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len)
    {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        //printf("[%f,", [latitude doubleValue]);
        //printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:loc];
    }
    return array;
}

-(void) centerMap
{
    MKCoordinateRegion region;
    CLLocationDegrees maxLat = -90.0;
    CLLocationDegrees maxLon = -180.0;
    CLLocationDegrees minLat = 90.0;
    CLLocationDegrees minLon = 180.0;
    NSMutableArray *pointArray = [[NSMutableArray alloc] init];
    
    if (sourcePoint)
        [pointArray addObject:sourcePoint];
    
    if (destinationPoint)
        [pointArray addObject:destinationPoint];
    
    if (pointArray.count == 0)
    {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
        {
            MapPoint *newMapPoint = [[MapPoint alloc] init];
            newMapPoint.name = @"Current Location";
            newMapPoint.type = MapPointTypePlace;
            newMapPoint.coordinate = CLLocationCoordinate2DMake(currentCentre.latitude, currentCentre.longitude);
            [pointArray addObject:newMapPoint];
        }
    }
    
    for(int idx = 0; idx < pointArray.count; idx++)
    {
        MapPoint *mp = [pointArray objectAtIndex:idx];
        double lat = mp.coordinate.latitude;
        double lon = mp.coordinate.longitude;
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(lat, lon);
        if(coord.latitude > maxLat)
            maxLat = coord.latitude;
        if(coord.latitude < minLat)
            minLat = coord.latitude;
        if(coord.longitude > maxLon)
            maxLon = coord.longitude;
        if(coord.longitude < minLon)
            minLon = coord.longitude;
    }
    region.center.latitude     = (maxLat + minLat) / 2.0;
    region.center.longitude    = (maxLon + minLon) / 2.0;
    
    region.span.latitudeDelta  = ((maxLat - minLat)<=0.0)?0.1:(maxLat - minLat);
    region.span.longitudeDelta = ((maxLon - minLon)<=0.0)?0.1:(maxLon - minLon);
    
    region.span.latitudeDelta  = region.span.latitudeDelta + 0.005;
    region.span.longitudeDelta  = region.span.longitudeDelta + 0.005;
    
    if (pointArray.count > 0)
        [mapView setRegion:region animated:YES];
}

#pragma mark MKPolyline delegate functions

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolyline *route = overlay;
        MKPolylineRenderer *routeRenderer = [[MKPolylineRenderer alloc] initWithPolyline:route];
        routeRenderer.strokeColor = [UIColor purpleColor];
        routeRenderer.lineWidth = 5.0;
        return routeRenderer;
    }
    return nil;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor purpleColor];
    polylineView.lineWidth = 5.0;
    return polylineView;
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    if(firstLaunch)
    {
        CLLocationCoordinate2D coord;
        coord.latitude = 13.052413900000000000;
        coord.longitude = 80.250824599999990000;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 10000,10000);
        [self.mapView setRegion:region animated:FALSE];
        firstLaunch = FALSE;
    }
}

#pragma mark UIAlertView delegate functions
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSString *stringToSearch = @"";
        if (alertView.tag == 11)
            stringToSearch = sourceField.text;
        else if (alertView.tag == 22)
            stringToSearch = destinationField.text;
        
        [self searchPlaceWithString:stringToSearch];
    }
}

@end
