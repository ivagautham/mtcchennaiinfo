//
//  HomeViewController.h
//  MTCChennaiInfo
//
//  Created by Gautham on 20/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>

@class ExpandableNavigation;
@interface HomeViewController : UIViewController<MFMailComposeViewControllerDelegate,UIScrollViewDelegate>
{
    
    IBOutlet UIButton* search;
    IBOutlet UIButton* route;
    IBOutlet UIButton* service;
    IBOutlet UIButton* map;
    IBOutlet UIButton* stage;
    IBOutlet UIButton* autoButton;

    IBOutlet UILabel* searchlabel;
    IBOutlet UILabel* routelabel;
    IBOutlet UILabel* servicelabel;
    IBOutlet UILabel* maplabel;
    IBOutlet UILabel* stagelabel;

    IBOutlet UILabel* ratelabel;
    IBOutlet UILabel* helplabel;
    IBOutlet UILabel* aboutlabel;
    IBOutlet UILabel* sharelabel;
    IBOutlet UILabel* feedbacklabel;
    
    IBOutlet UIButton* heavyRefresh;
    IBOutlet UIButton* restoreDefaults;
    IBOutlet UIButton* feedback;
    IBOutlet UIButton* help;
    IBOutlet UIButton* about;
    IBOutlet UIButton* rate;
    IBOutlet UIButton* share;
    IBOutlet UIButton* main;
    ExpandableNavigation* navigation;
        
    IBOutlet UIView *helpView;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIButton *closeHelp;
    
    IBOutlet UIView *loadingView;
    IBOutlet UIView *loadingView2;
}

@property (nonatomic, strong) UIButton* search;
@property (nonatomic, strong) UIButton* route;
@property (nonatomic, strong) UIButton* service;
@property (nonatomic, strong) UIButton* map;
@property (nonatomic, strong) UIButton* stage;
@property (nonatomic, strong) UIButton* autoButton;

@property (nonatomic, strong) UILabel* searchlabel;
@property (nonatomic, strong) UILabel* routelabel;
@property (nonatomic, strong) UILabel* servicelabel;
@property (nonatomic, strong) UILabel* maplabel;
@property (nonatomic, strong) UILabel* stagelabel;

@property (nonatomic, strong) UILabel* ratelabel;
@property (nonatomic, strong) UILabel* helplabel;
@property (nonatomic, strong) UILabel* aboutlabel;
@property (nonatomic, strong) UILabel* sharelabel;
@property (nonatomic, strong) UILabel* feedbacklabel;


@property (nonatomic, strong) IBOutlet UIButton *heavyRefresh;
@property (nonatomic, strong) IBOutlet UIButton *restoreDefaults;
@property (nonatomic, strong) IBOutlet UIButton *feedback;
@property (nonatomic, strong) IBOutlet UIButton *help;
@property (nonatomic, strong) IBOutlet UIButton *about;
@property (nonatomic, strong) IBOutlet UIButton *rate;
@property (nonatomic, strong) IBOutlet UIButton *share;
@property (nonatomic, strong) IBOutlet UIButton *main;
@property (strong) ExpandableNavigation* navigation;

@property (nonatomic, strong) UIView *helpView;
@property (nonatomic, strong)  UIScrollView *scrollView;
@property (nonatomic, strong)  UIPageControl *pageControl;
@property (nonatomic, strong) UIButton *closeHelp;

@property (nonatomic, strong) UIView *loadingView;
@property (nonatomic, strong) UIView *loadingView2;

-(IBAction)onStage:(id)sender;
-(IBAction)onRoute:(id)sender;
-(IBAction)onSearch:(id)sender;
-(IBAction)onMap:(id)sender;
-(IBAction)onMRTS:(id)sender;
-(IBAction)onAuto:(id)sender;

-(IBAction)onCloseHelp:(id)sender;

-(void)setlabelHidden:(BOOL)flag;

@end
