//
//  SearchViewController.m
//  MTCChennaiInfo
//
//  Created by Gautham on 20/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "SearchViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Singleton.h"
#import "MRTSViewController.h"
#import "StageViewController.h"
#import "RouteViewController.h"
#import "SearchDetailsCell.h"
#import "MapViewController.h"
#import "SearchResultCell.h"
#import "Flurry.h"
#import "MRTSViewController.h"
#import "AutoViewController.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@interface SearchViewController ()
{
    BOOL didLoadfromResource;
    NSIndexPath *tappedIndexPath;
    NSDictionary *placeSearchDetails;
    CLLocationDistance closestDistance;
    CLLocationDistance closestMRTS;
    NSString *currentDistance;
    NSMutableArray *pointArray;
}

@end

@implementation SearchViewController
@synthesize mapView;
@synthesize searchBox;
@synthesize searchResult;
@synthesize allStages, allRoutes;
@synthesize searchResultTable;
@synthesize noMatchLabel;
@synthesize routeInfo;
@synthesize stageDetails,allMRTSStations;
@synthesize filter;
@synthesize loading;
@synthesize backButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.layer.cornerRadius = 10;
        UISwipeGestureRecognizer *swipeDownforHome = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onHome:)];
        swipeDownforHome.direction = UISwipeGestureRecognizerDirectionUp;
        [self.view addGestureRecognizer: swipeDownforHome];
        }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(!searchResult)
        searchResult =[[NSMutableArray alloc] init];
    
    if(!tappedIndexPath)
        tappedIndexPath = [[NSIndexPath alloc] init];
    
    if(!placeSearchDetails)
        placeSearchDetails = [[NSMutableDictionary alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_SHOW_QOS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_HIDE_QOS object:nil];
    
    [searchResult removeAllObjects];
    
    searchBox.layer.cornerRadius = 5;
    searchBox.layer.borderWidth = 2;
    searchBox.layer.borderColor = mSingleton.elementColor1.CGColor;
    searchBox.delegate = self;
    
    //    searchResultTable.layer.cornerRadius = 10;
    //    searchResultTable.layer.borderWidth = 2;
    searchResultTable.layer.borderColor = [mSingleton.bgColor CGColor];
    [searchResultTable setHidden:TRUE];
    [noMatchLabel setHidden:TRUE];
    [loading setHidden:TRUE];
    
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:YES];
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    //Set the first launch instance variable to allow the map to zoom on the user location when first launched.
    firstLaunch=YES;
    [self.view setBackgroundColor:mSingleton.bgColor];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!didLoadfromResource)
        [self performSelectorInBackground:@selector(loadStagesFromResource) withObject:nil];
    [locationManager startUpdatingLocation];
    
    [searchResultTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [searchBox resignFirstResponder];
    [locationManager stopUpdatingLocation];
    
}

-(void)loadStagesFromResource
{
    didLoadfromResource = FALSE;
    [allStages removeAllObjects];
    allStages = [mSingleton.allStages mutableCopy];
    
    [allRoutes removeAllObjects];
    allRoutes = [mSingleton.allRoutes mutableCopy];
    
    [routeInfo removeAllObjects];
    routeInfo = [mSingleton.routeInfo mutableCopy];
    
    [stageDetails removeAllObjects];
    stageDetails = [mSingleton.stageDetails mutableCopy];
    
    [allMRTSStations removeAllObjects];
    allMRTSStations = [mSingleton.allMRTSStations mutableCopy];
    
    didLoadfromResource = TRUE;
}

-(void) searchStages:(NSString *)searchText
{
    for (NSString *section in self.allStages) {
        if ([[section uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:section forKey:FIELD_SEARCH_RESULT];
            [dict setValue:FIELD_STAGE_SEARCH forKey:FIELD_SEARCH_TYPE];
            [searchResult addObject:dict];
        }
    }
}

-(void) searchRoutes:(NSString *)searchText
{
    BOOL smallBus = FALSE;
    if ([[searchText uppercaseString] isEqualToString:@"SMALLBUS"] || [[searchText uppercaseString] isEqualToString:@"MINIBUS"] || [[searchText uppercaseString] isEqualToString:@"SMALL BUS"] || [[searchText uppercaseString] isEqualToString:@"MINI BUS"])
    {
        smallBus = TRUE;
    }
    
    for (NSString *section in self.allRoutes) {
        if ([[section uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:section forKey:FIELD_SEARCH_RESULT];
            [dict setValue:FIELD_ROUTE_SEARCH forKey:FIELD_SEARCH_TYPE];
            [searchResult addObject:dict];
        }
        if (smallBus)
        {
            NSDictionary *requiredDict = [self getDetailsforRoute:section];
            if ([[requiredDict valueForKey:FIELD_STAGE_TYPE] isEqualToString:@"Small Bus"])
            {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:section forKey:FIELD_SEARCH_RESULT];
                [dict setValue:FIELD_ROUTE_SEARCH forKey:FIELD_SEARCH_TYPE];
                [searchResult addObject:dict];
            }
        }
    }
}

-(void) queryGooglePlaces: (NSString *) googlePlace withToken:(NSString *)nextToken
{
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
    {
        currentCentre.latitude = 13.052413900000000000;
        currentCentre.longitude = 80.250824599999990000;
    }
    
    CLLocationCoordinate2D homCoord;
    homCoord.latitude = 13.052413900000000000;
    homCoord.longitude = 80.250824599999990000;
    CLLocation *homeLocation = [[CLLocation alloc] initWithLatitude:homCoord.latitude longitude:homCoord.longitude];
    CLLocation *destiLocation = [[CLLocation alloc] initWithLatitude:currentCentre.latitude longitude:currentCentre.longitude];
    CLLocationDistance meters = [homeLocation distanceFromLocation:destiLocation];
    if (meters > 50000)
    {
        currentCentre.latitude = 13.052413900000000000;
        currentCentre.longitude = 80.250824599999990000;
    }
    NSString *url = nil;
    if (!nextToken || nextToken.length == 0)
    {
        //Remove any existing custom annotations but not the user location blue dot.
        for (id<MKAnnotation> annotation in mapView.annotations)
        {
            if ([annotation isKindOfClass:[MapPoint class]])
            {
                [mapView removeAnnotation:annotation];
            }
        }
        url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=100000&name=%@&sensor=false&key=%@", currentCentre.latitude, currentCentre.longitude, googlePlace, kGOOGLE_API_KEY];
    }
    else
    {
        url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=100000&name=%@&sensor=false&key=%@&pagetoken=%@", currentCentre.latitude, currentCentre.longitude, googlePlace, kGOOGLE_API_KEY,nextToken];
        
    }
    url =[url stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSURL *googleRequestURL=[NSURL URLWithString:url];
    
    // Retrieve the results of the URL.
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    });
}

- (void)fetchedData:(NSData *)responseData {
    if (!responseData)
    {
        [loading setHidden:TRUE];
        tappedIndexPath = nil;
        [searchResultTable setHidden:FALSE];
        [searchResultTable setContentOffset:CGPointZero animated:NO];
        [searchResultTable reloadData];
        searchBox.userInteractionEnabled = TRUE;
        searchBox.alpha = 1.0;
        filter.userInteractionEnabled = TRUE;
        filter.alpha = 1.0;
        return;
    }
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          
                          options:kNilOptions
                          error:&error];
    
    NSArray* places = [json objectForKey:@"results"];
    NSLog(@"Google Data: %@", places);
    
    if (![[json valueForKey:@"next_page_token"] isKindOfClass:[NSNull class]])
        [self plotPositions:places withToken:[json valueForKey:@"next_page_token"]];
}

- (void)plotPositions:(NSArray *)data withToken:(NSString *)token
{
    //Loop through the array of places returned from the Google API.
    if(!pointArray)
    {
        pointArray = [[NSMutableArray alloc] init];
        [pointArray removeAllObjects];
    }
    for (int i=0; i<[data count]; i++)
    {
        //Retrieve the NSDictionary object in each index of the array.
        NSDictionary* place = [data objectAtIndex:i];
        NSDictionary *geo = [place objectForKey:@"geometry"];
        NSString *name = [place objectForKey:@"name"];
        NSString *vicinity = [place objectForKey:@"vicinity"];
        NSDictionary *loc = [geo objectForKey:@"location"];
        
        CLLocationCoordinate2D placeCoord;
        placeCoord.latitude=[[loc objectForKey:@"lat"] doubleValue];
        placeCoord.longitude=[[loc objectForKey:@"lng"] doubleValue];
        
        CLLocationCoordinate2D homCoord;
        
        homCoord.latitude = 13.052413900000000000;
        homCoord.longitude = 80.250824599999990000;
        
        CLLocation *homeLocation = [[CLLocation alloc] initWithLatitude:homCoord.latitude longitude:homCoord.longitude];
        CLLocation *destiLocation = [[CLLocation alloc] initWithLatitude:placeCoord.latitude longitude:placeCoord.longitude];
        
        CLLocationDistance meters = [homeLocation distanceFromLocation:destiLocation];
        if (meters < 50000)
        {
            MapPoint *placeObject = [[MapPoint alloc] initWithName:name address:vicinity coordinate:placeCoord andtype:MapPointTypePlace];
            [mapView addAnnotation:placeObject];
            [pointArray addObject:placeObject];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:placeObject forKey:FIELD_SEARCH_RESULT];
            [dict setValue:FIELD_GOOGLE_PLACES_SEARCH forKey:FIELD_SEARCH_TYPE];
            [searchResult addObject:dict];
        }
    }
    if (!token || token.length == 0)
    {
        [loading setHidden:TRUE];
        tappedIndexPath = nil;
        [searchResultTable setHidden:FALSE];
        [searchResultTable setContentOffset:CGPointZero animated:NO];
        [searchResultTable reloadData];
        searchBox.userInteractionEnabled = TRUE;
        searchBox.alpha = 1.0;
        filter.userInteractionEnabled = TRUE;
        filter.alpha = 1.0;
        [self centerMap];
    }
    else
    {
        if ([searchBox.text isEqualToString:@"POPULAR PLACES"])
            searchBox.text = @"";
        
        NSString *trimmedString = [searchBox.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (trimmedString.length <= 0)
            searchBox.text = @"POPULAR PLACES";
        
        [self queryGooglePlaces:trimmedString withToken:token];
    }
}

-(NSDictionary *)getDetailsforRoute:(NSString *)route
{
    for (NSDictionary *dict in routeInfo) {
        if ([[[dict objectForKey:FIELD_STAGE_NUMBER] uppercaseString] isEqual:[route uppercaseString]]) {
            return dict;
        }
    }
    return nil;
}

-(BOOL)isLocationAvailableforStage:(NSString *)stage
{
    for (NSDictionary *dict in stageDetails) {
        NSString *dictValue = [[dict valueForKey:@"fields"] valueForKey:@"display_name"];
        id latitude = [[dict valueForKey:@"fields"] valueForKey:@"latitude"];
        id longitude = [[dict valueForKey:@"fields"] valueForKey:@"longitude"];
        
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]] && ![latitude isMemberOfClass:[NSNull class]] && ![longitude isMemberOfClass:[NSNull class]]) {
            return YES;
        }
    }
    return NO;
}

-(NSMutableDictionary *)getDetailsForStage:(NSString *)stage
{
    NSMutableDictionary *Marker;
    for (NSDictionary *dict in stageDetails) {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[NSMutableDictionary alloc] init];
            [Marker setValue:stage forKey:@"stage"];
            if (![[childDict valueForKey:@"importance"] isKindOfClass:[NSNull class]])
            {
                id importance = [childDict valueForKey:@"importance"];
                [Marker setValue:importance forKey:@"importance"];
            }
            if (![[childDict valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
            {
                id is_terminus = [childDict valueForKey:@"is_terminus"];
                [Marker setValue:is_terminus forKey:@"is_terminus"];
            }
        }
        
    }
    return Marker;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)onRoute:(NSString *)route
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromTop;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        RouteViewController *routeViewController = (RouteViewController *)vc;
        if ([vc isMemberOfClass:[RouteViewController class]]) {
            [routeViewController updateDetailsTableForRoute:route];
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    RouteViewController *routeViewController = [[mSingleton getViewControllerManagerInstance] getRouteViewController];
    [routeViewController updateDetailsTableForRoute:route];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:routeViewController animated:NO];
}

-(IBAction)onStage:(NSString *)source
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.subtype = kCATransitionFromTop;
    transition.type = @"cube";
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[StageViewController class]]) {
            StageViewController *stageViewController = (StageViewController *)vc;
            [stageViewController updateDetailsTableForSource:source andDestination:nil];
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    StageViewController *stageViewController = [[mSingleton getViewControllerManagerInstance] getStageViewController];
    [stageViewController updateDetailsTableForSource:source andDestination:nil];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:stageViewController animated:NO];
}

-(IBAction)onStation:(NSString *)source
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.subtype = kCATransitionFromTop;
    transition.type = @"cube";
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[MRTSViewController class]]) {
            MRTSViewController *mrtsViewController = (MRTSViewController *)vc;
            [mrtsViewController updateDetailsTableForSource:source];
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    MRTSViewController *mrtsViewController = [[mSingleton getViewControllerManagerInstance] getMRTSViewController];
    [mrtsViewController updateDetailsTableForSource:source];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mrtsViewController animated:NO];
}

-(IBAction)onHome:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = @"cube";
    transition.subtype = kCATransitionFromTop;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    //for (UIViewController *vc in self.navigationController.viewControllers) {
    for (int idx = ([self.navigationController.viewControllers count] - 1); idx > 0; idx --) {
        UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:idx];
        if ([vc isMemberOfClass:[MRTSViewController class]]) {
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
        else if ([vc isMemberOfClass:[StageViewController class]]) {
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
        else if ([vc isMemberOfClass:[RouteViewController class]]) {
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(IBAction)filterdidChange:(id)sender
{
    if ([searchBox.text isEqualToString:@"POPULAR PLACES"])
        searchBox.text = @"";
    
    searchBox.userInteractionEnabled = FALSE;
    searchBox.alpha = 0.6;
    filter.userInteractionEnabled = FALSE;
    filter.alpha = 0.6;
    [searchBox resignFirstResponder];
    [searchResult removeAllObjects];
    NSString *trimmedString = [searchBox.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [loading setHidden:FALSE];
    [searchResultTable setHidden:TRUE];
    [noMatchLabel setHidden:TRUE];
    
    NSDictionary *articleParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     trimmedString, @"search_String",
     nil];
    
    [Flurry logEvent:@"Searched_String" withParameters:articleParams];
    
    if (filter.selectedSegmentIndex == 0)
    {
        if (trimmedString.length <= 0)
            searchBox.text = @"POPULAR PLACES";
        
        [self searchRoutes:trimmedString];
        [self searchStages:trimmedString];
        [self queryGooglePlaces:trimmedString withToken:nil];
    }
    else if (filter.selectedSegmentIndex == 1)
    {
        if (trimmedString.length <= 0)
            searchBox.text = @"POPULAR PLACES";
        
        [self queryGooglePlaces:trimmedString withToken:nil];
    }
    else if (filter.selectedSegmentIndex == 2)
    {
        if (trimmedString.length <= 0)
            searchBox.text = @"";
        
        [self searchStages:trimmedString];
        [loading setHidden:TRUE];
        tappedIndexPath = nil;
        [searchResultTable setHidden:FALSE];
        [searchResultTable setContentOffset:CGPointZero animated:NO];
        [searchResultTable reloadData];
        searchBox.userInteractionEnabled = TRUE;
        searchBox.alpha = 1.0;
        filter.userInteractionEnabled = TRUE;
        filter.alpha = 1.0;
    }
    else if (filter.selectedSegmentIndex == 3)
    {
        if (trimmedString.length <= 0)
            searchBox.text = @"";
        
        [self searchRoutes:trimmedString];
        [loading setHidden:TRUE];
        tappedIndexPath = nil;
        [searchResultTable setHidden:FALSE];
        [searchResultTable setContentOffset:CGPointZero animated:NO];
        [searchResultTable reloadData];
        searchBox.userInteractionEnabled = TRUE;
        searchBox.alpha = 1.0;
        filter.userInteractionEnabled = TRUE;
        filter.alpha = 1.0;
    }
}
-(IBAction)onAutoButton:(UIButton *)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cameraIris";
    transition.subtype = kCATransitionFromTop;
    
    MapPoint *stageMapPoint = nil;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[AutoViewController class]]) {
            AutoViewController *autoViewController = (AutoViewController *)vc;
            if ([sender isMemberOfClass:[UIButton class]])
            {
                
                NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
                SearchDetailsCell *cell = (SearchDetailsCell*)[searchResultTable cellForRowAtIndexPath:myIP];
                for (NSDictionary *dict in searchResult)
                {
                    NSString *resultType = [dict objectForKey:FIELD_SEARCH_TYPE];
                    if ([resultType isEqualToString:FIELD_STAGE_SEARCH])
                    {
                        NSString *result = [dict objectForKey:FIELD_SEARCH_RESULT];
                        if ([result isEqualToString:cell.contentLabel.text])
                        {
                            stageMapPoint = [self getLocationMarkerForStage:cell.contentLabel.text];
                            break;
                        }
                    }
                    else
                    {
                        MapPoint *details = [dict objectForKey:FIELD_SEARCH_RESULT];
                        if ([details.name isEqualToString:cell.contentLabel.text])
                        {
                            stageMapPoint = details;
                            break;
                        }
                    }
                }
                if (stageMapPoint)
                {
                    if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
                    {
                        autoViewController.destinationPoint = autoViewController.sourcePoint;
                        autoViewController.sourcePoint = stageMapPoint;
                        autoViewController.sourcePoint.address = nil;
                    }
                    else
                    {
                        autoViewController.sourcePoint = stageMapPoint;
                        autoViewController.sourcePoint.address = nil;
                    }
                    
                    [autoViewController updateLocationDetails];
                }
            }
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:autoViewController animated:NO];
            return;
        }
    }
    AutoViewController *autoViewController = [[mSingleton getViewControllerManagerInstance] getAutoViewController];
    if ([sender isMemberOfClass:[UIButton class]])
    {
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        SearchDetailsCell *cell = (SearchDetailsCell*)[searchResultTable cellForRowAtIndexPath:myIP];
        for (NSDictionary *dict in searchResult)
        {
            NSString *resultType = [dict objectForKey:FIELD_SEARCH_TYPE];
            if ([resultType isEqualToString:FIELD_STAGE_SEARCH])
            {
                NSString *result = [dict objectForKey:FIELD_SEARCH_RESULT];
                if ([result isEqualToString:cell.contentLabel.text])
                {
                    stageMapPoint = [self getLocationMarkerForStage:cell.contentLabel.text];
                    break;
                }
            }
            else
            {
                MapPoint *details = [dict objectForKey:FIELD_SEARCH_RESULT];
                if ([details.name isEqualToString:cell.contentLabel.text])
                {
                    stageMapPoint = details;
                    break;
                }
            }
        }
        if (stageMapPoint)
        {
            if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
            {
                autoViewController.destinationPoint = autoViewController.sourcePoint;
                autoViewController.sourcePoint = stageMapPoint;
                autoViewController.sourcePoint.address = nil;
            }
            else
            {
                autoViewController.sourcePoint = stageMapPoint;
                autoViewController.sourcePoint.address = nil;
            }
            
            [autoViewController updateLocationDetails];
        }
    }
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:autoViewController animated:NO];
    
}

-(IBAction)onMapButton:(UIButton *)sender
{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SearchDetailsCell *cell = (SearchDetailsCell*)[searchResultTable cellForRowAtIndexPath:myIP];
    NSDictionary *requiredDict = nil;
    for (NSDictionary *dict in searchResult)
    {
        NSString *resultType = [dict objectForKey:FIELD_SEARCH_TYPE];
        if ([resultType isEqualToString:FIELD_ROUTE_SEARCH] || [resultType isEqualToString:FIELD_STAGE_SEARCH] )
        {
            NSString *result = [dict objectForKey:FIELD_SEARCH_RESULT];
            if ([result isEqualToString:cell.contentLabel.text])
            {
                requiredDict = dict;
                break;
            }
        }
        else
        {
            MapPoint *details = [dict objectForKey:FIELD_SEARCH_RESULT];
            if ([details.name isEqualToString:cell.contentLabel.text])
            {
                requiredDict = dict;
                break;
            }
        }
    }
    
    if (requiredDict)
    {
        if ([[requiredDict objectForKey:FIELD_SEARCH_TYPE] isEqualToString:FIELD_ROUTE_SEARCH])
        {
            NSString *route = [requiredDict objectForKey:FIELD_SEARCH_RESULT];
            NSDictionary *routeDetailDictionay = [NSDictionary dictionaryWithDictionary:[self getDetailsforRoute:route]];
            [self onMapforRoute:routeDetailDictionay];
        }
        else if([[requiredDict objectForKey:FIELD_SEARCH_TYPE] isEqualToString:FIELD_STAGE_SEARCH])
        {
            NSString *stage = [requiredDict objectForKey:FIELD_SEARCH_RESULT];
            [self onMapforStage:stage];
        }
        else
        {
            MapPoint *details = [requiredDict objectForKey:FIELD_SEARCH_RESULT];
            [self onMapforPlace:details];
        }
    }
}

-(void)onStageDetailsButton:(UIButton *)sender
{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SearchResultCell *cell = (SearchResultCell *)[searchResultTable cellForRowAtIndexPath:myIP];
    [self onStage: [cell.nearestStage titleForState:UIControlStateNormal]];
}

-(void)onStationDetailsButton:(UIButton *)sender
{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SearchResultCell *cell = (SearchResultCell *)[searchResultTable cellForRowAtIndexPath:myIP];
    [self onStation: [cell.nearestStation titleForState:UIControlStateNormal]];
}

-(void)onMapforPlace:(MapPoint *)point
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    [mapViewController plotPlace:point];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
}

-(void)onMapforRoute:(NSDictionary *)routedetail
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    NSMutableArray *pathArray = [[NSMutableArray alloc] init];
    for (NSString *path in [routedetail objectForKey:FIELD_STAGE_ROUTE]) {
        if ([self isLocationAvailableforStage:path]) {
            [pathArray addObject:[self getLocationDetailsForStage:path]];
        }
    }
    [mapViewController plotRoute:pathArray];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
}

-(void)onMapforStage:(NSString *)stage
{
    NSMutableDictionary *stageMarker = [self getLocationDetailsForStage:stage];
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    
    [mapViewController plotStage:stageMarker];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
    
}

-(NSMutableDictionary *)getLocationDetailsForStage:(NSString *)stage
{
    NSMutableDictionary *Marker;
    for (NSDictionary *dict in stageDetails) {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[NSMutableDictionary alloc] init];
            [Marker setValue:stage forKey:@"stage"];
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                id latitude = [childDict valueForKey:@"latitude"];
                [Marker setValue:latitude forKey:@"latitude"];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                id longitude = [childDict valueForKey:@"longitude"];
                [Marker setValue:longitude forKey:@"longitude"];
            }
            if (![[childDict valueForKey:@"importance"] isKindOfClass:[NSNull class]])
            {
                id importance = [childDict valueForKey:@"importance"];
                [Marker setValue:importance forKey:@"importance"];
            }
            if (![[childDict valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
            {
                id is_terminus = [childDict valueForKey:@"is_terminus"];
                [Marker setValue:is_terminus forKey:@"is_terminus"];
            }
        }
        
    }
    return Marker;
}

-(MapPoint *)getLocationMarkerForStage:(NSString *)stage
{
    MapPoint *Marker;
    
    CLLocationDegrees latitude;
	CLLocationDegrees longitude;
    for (NSDictionary *dict in stageDetails)
    {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[MapPoint alloc] init];
            Marker.name = stage;
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                latitude = [[childDict valueForKey:@"latitude"] doubleValue];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                longitude = [[childDict valueForKey:@"longitude"] doubleValue];
            }
        }
    }
    Marker.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    return Marker;
}

-(NSDictionary *)getNearestStagefromCoord:(CLLocationCoordinate2D)coord
{
    CLLocationDistance nearest = 999999;
    NSMutableDictionary *requiredDictionary = nil;
    CLLocation *searchLocation = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
    for (NSDictionary *dict in stageDetails)
    {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        if ((![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]]) && (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]]))
        {
            if ([allStages containsObject:[[childDict valueForKey:@"mtc_name"] uppercaseString]])
            {
                double latti = [[childDict valueForKey:@"latitude"] doubleValue];
                double longi = [[childDict valueForKey:@"longitude"] doubleValue];
                CLLocation *neatestLocation = [[CLLocation alloc] initWithLatitude:latti longitude:longi];
                CLLocationDistance meters = [searchLocation distanceFromLocation:neatestLocation];
                if (meters <= nearest) {
                    nearest = meters;
                    requiredDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
                }
            }
        }
    }
    closestDistance = nearest;
    
    nearest = 999999;
    NSDictionary *requiredMRTSDictionary = nil;
    for (NSDictionary *dict in allMRTSStations)
    {
        NSDictionary *childDict = dict;
        if ([[childDict allKeys] containsObject:@"lattitude"] && [[childDict allKeys] containsObject:@"longitude"])
        {
            if ((![[childDict valueForKey:@"lattitude"] isKindOfClass:[NSNull class]]) && (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]]))
            {
                double latti = [[childDict valueForKey:@"lattitude"] doubleValue];
                double longi = [[childDict valueForKey:@"longitude"] doubleValue];
                CLLocation *neatestLocation = [[CLLocation alloc] initWithLatitude:latti longitude:longi];
                CLLocationDistance meters = [searchLocation distanceFromLocation:neatestLocation];
                if (meters <= nearest) {
                    nearest = meters;
                    requiredMRTSDictionary = dict;
                }
            }
        }
    }
    closestMRTS = nearest;
    [requiredDictionary addEntriesFromDictionary:requiredMRTSDictionary];
    return requiredDictionary;
}

-(void)getCurrentDistanceToPlaceCoord:(CLLocationCoordinate2D)coord
{
    currentDistance = @"N/A";
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
    {
        CLLocation *placeLocation = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
        CLLocation *currLocation = [[CLLocation alloc] initWithLatitude:currentCentre.latitude longitude:currentCentre.longitude];
        currentDistance = [NSString stringWithFormat:@"%f",[placeLocation distanceFromLocation:currLocation]/1000];
        
        NSString *distanceURL = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=walking&sensor=false",currentCentre.latitude,currentCentre.longitude,coord.latitude,coord.longitude];
        NSURL *googleAPIurl = [NSURL URLWithString:distanceURL];
        NSData* data = [NSData dataWithContentsOfURL: googleAPIurl];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data
                              
                              options:kNilOptions
                              error:&error];
        
        NSArray *distArray = [json valueForKey:@"rows"];
        if ([distArray count] > 0)
        {
            NSDictionary *distDict = [distArray objectAtIndex:0];
            NSArray *distArray2 = [distDict valueForKey:@"elements"];
            if ([distArray2 count] > 0)
            {
                NSDictionary *distDict2 = [distArray2 objectAtIndex:0];
                currentDistance = [[distDict2 valueForKey:@"distance"] valueForKey:@"text"] ;
            }
        }
    }
}

#pragma mark - CLLocationManager methods.
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
    {
        currentCentre = newLocation.coordinate;
    }
    else
    {
        currentCentre.latitude = 13.052413900000000000;
        currentCentre.longitude = 80.250824599999990000;
    }
}

#pragma mark - MKMapViewDelegate methods.

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    CLLocationCoordinate2D centre = [mv centerCoordinate];
    MKCoordinateRegion region;
    if (firstLaunch) {
        CLLocationCoordinate2D coord;
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
        {
            currentCentre.latitude = 13.052413900000000000;
            currentCentre.longitude = 80.250824599999990000;
            coord = currentCentre;
        }
        else
        {
            coord = locationManager.location.coordinate;
        }
        region = MKCoordinateRegionMakeWithDistance(coord,1000,1000);
        firstLaunch=NO;
    }else {
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
        {
            currentCentre.latitude = 13.052413900000000000;
            currentCentre.longitude = 80.250824599999990000;
            centre = currentCentre;
            currenDist = 30000;
        }
        //Set the center point to the visible region of the map and change the radius to match the search radius passed to the Google query string.
        region = MKCoordinateRegionMakeWithDistance(centre,currenDist,currenDist);
    }
    region.span.latitudeDelta = 0.5;
    region.span.longitudeDelta = 1.0;
    [mv setRegion:region animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    //Define our reuse indentifier.
    static NSString *identifier = @"MapPoint";
    if ([annotation isKindOfClass:[MapPoint class]]) {
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        } else {
            annotationView.annotation = annotation;
        }
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.animatesDrop = YES;
        
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    //Get the east and west points on the map so we calculate the distance (zoom level) of the current map view.
    MKMapRect mRect = self.mapView.visibleMapRect;
    MKMapPoint eastMapPoint = MKMapPointMake(MKMapRectGetMinX(mRect), MKMapRectGetMidY(mRect));
    MKMapPoint westMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), MKMapRectGetMidY(mRect));
    
    //Set our current distance instance variable.
    currenDist = MKMetersBetweenMapPoints(eastMapPoint, westMapPoint);
    
    //Set our current centre point on the map instance variable.
    // currentCentre = self.mapView.centerCoordinate;
}

-(void)searchPlace:(NSString *)place
{
    [searchResult removeAllObjects];
    NSString *trimmedString = [place stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    searchBox.text = trimmedString;
    [loading setHidden:FALSE];
    [searchResultTable setHidden:TRUE];
    [noMatchLabel setHidden:TRUE];
    filter.selectedSegmentIndex = 1;
    [self filterdidChange:nil];
}

#pragma mark-
#pragma mark TextFiled Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == searchBox && textField.text.length > 0)
    {
        [textField resignFirstResponder];
        [searchResult removeAllObjects];
        NSString *trimmedString = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [loading setHidden:FALSE];
        [searchResultTable setHidden:TRUE];
        [noMatchLabel setHidden:TRUE];
        
        NSDictionary *articleParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         trimmedString, @"search_String",
         nil];
        
        [Flurry logEvent:@"Searched_String" withParameters:articleParams];
        
        if (filter.selectedSegmentIndex == 0)
        {
            if (trimmedString.length <= 0)
                searchBox.text = @"POPULAR PLACES";
            
            [self searchRoutes:trimmedString];
            [self searchStages:trimmedString];
            [self queryGooglePlaces:trimmedString withToken:nil];
        }
        else if (filter.selectedSegmentIndex == 1)
        {
            if (trimmedString.length <= 0)
                searchBox.text = @"POPULAR PLACES";
            
            [self queryGooglePlaces:trimmedString withToken:nil];
        }
        else if (filter.selectedSegmentIndex == 2)
        {
            if (trimmedString.length <= 0)
                searchBox.text = @"";
            
            [self searchStages:trimmedString];
            [loading setHidden:TRUE];
            tappedIndexPath = nil;
            [searchResultTable setHidden:FALSE];
            [searchResultTable setContentOffset:CGPointZero animated:NO];
            [searchResultTable reloadData];
            searchBox.userInteractionEnabled = TRUE;
            searchBox.alpha = 1.0;
            filter.userInteractionEnabled = TRUE;
            filter.alpha = 1.0;
        }
        else if (filter.selectedSegmentIndex == 3)
        {
            if (trimmedString.length <= 0)
                searchBox.text = @"";
            
            [self searchRoutes:trimmedString];
            [loading setHidden:TRUE];
            tappedIndexPath = nil;
            [searchResultTable setHidden:FALSE];
            [searchResultTable setContentOffset:CGPointZero animated:NO];
            [searchResultTable reloadData];
            searchBox.userInteractionEnabled = TRUE;
            searchBox.alpha = 1.0;
            filter.userInteractionEnabled = TRUE;
            filter.alpha = 1.0;
        }
        
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@"POPULAR PLACES"])
        textField.text = @"";
}

#pragma mark-
#pragma mark tableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == searchResultTable)
        return 1;
    
    return 0;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*) indexPath
{
    if (!tappedIndexPath)
        return 60;
    
    if ([tappedIndexPath compare: indexPath] == NSOrderedSame)
        return 180;
    
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == searchResultTable)
    {
        CGRect tableHeight = searchResultTable.frame;
        if (IS_IPHONE_5)
        {
            if ([searchResult count] < 5)
            {
                tableHeight.size.height = 60 * [searchResult count];
                if (tappedIndexPath)
                    tableHeight.size.height = 306;
            }
            else
            {
                tableHeight.size.height = 306;
                [searchResultTable flashScrollIndicators];
            }
        }
        else
        {
            if ([searchResult count] < 4)
            {
                tableHeight.size.height = 60 * [searchResult count];
                if (tappedIndexPath)
                    tableHeight.size.height = 225;
            }
            else
            {
                tableHeight.size.height = 225;
                [searchResultTable flashScrollIndicators];
            }
        }
        searchResultTable.frame = tableHeight;
        
        if ([searchResult count] == 0) {
            [loading setHidden:TRUE];
            [searchResultTable setHidden:TRUE];
            [noMatchLabel setHidden:FALSE];
        }
        return [searchResult count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == searchResultTable)
    {
        if (!tappedIndexPath)
        {
            if (indexPath.row >= searchResult.count)
                return nil;
            
            static NSString * CellIdentifier = @"SearchDetailsCell";
            SearchDetailsCell *cell = (SearchDetailsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchDetailsCell"owner:self options:nil];
                cell = (SearchDetailsCell *)[topLevelObjects objectAtIndex:0];
            }
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            
            NSDictionary *cellDict = [searchResult objectAtIndex:indexPath.row];
            NSString *resultType = [cellDict objectForKey:FIELD_SEARCH_TYPE];
            if ([resultType isEqualToString:FIELD_STAGE_SEARCH])
            {
                NSString *stage = [cellDict objectForKey:FIELD_SEARCH_RESULT];
                cell.contentLabel.text = stage;
                [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_stagesType.png"]];
                if ([self isLocationAvailableforStage:stage])
                {
                    NSDictionary *stageDetailDictionay = [NSDictionary dictionaryWithDictionary:[self getDetailsForStage:stage]];
                    
                    NSString *importance, *isTermius;
                    if ([stageDetailDictionay count] > 0)
                    {
                        if (![[stageDetailDictionay valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
                        {
                            BOOL termius = [[stageDetailDictionay valueForKey:@"is_terminus"] boolValue];
                            isTermius = [NSString stringWithFormat:@"%@",termius ? @"YES" : @"NO"];
                        }
                        else
                            isTermius = @"-";
                        
                        if (![[stageDetailDictionay valueForKey:@"importance"] isKindOfClass:[NSNull class]])
                            importance = [[stageDetailDictionay valueForKey:@"importance"] stringValue];
                        else
                            importance = @"-";
                    }
                    else
                    {
                        importance = @"-";
                        isTermius = @"-";
                    }
                    cell.detalilLabel.text = [NSString stringWithFormat:@"Stage Importance: %@\nTermius: %@",importance, isTermius];
                    cell.plotButton.tag = indexPath.row;
                    [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
                    cell.autoButton.tag = indexPath.row;
                    [cell.autoButton addTarget:self action:@selector(onAutoButton:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView setUserInteractionEnabled: NO];
                }
                else
                {
                    [cell.plotButton setHidden:TRUE];
                    [cell.autoButton setHidden:TRUE];
                    cell.detalilLabel.text = @"-";
                }
            }
            else if ([resultType isEqualToString:FIELD_ROUTE_SEARCH])
            {
                NSString *route = [cellDict objectForKey:FIELD_SEARCH_RESULT];
                NSDictionary *routeDetailDictionay = [NSDictionary dictionaryWithDictionary:[self getDetailsforRoute:route]];
                cell.contentLabel.text = route;
                [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_busType.png"]];
                cell.detalilLabel.text = [NSString stringWithFormat:@"Source: %@\nDestincation: %@",[[routeDetailDictionay objectForKey:FIELD_STAGE_SOURCE] capitalizedString],[[routeDetailDictionay objectForKey:FIELD_STAGE_DESTINATION] capitalizedString]];
                cell.plotButton.tag = indexPath.row;
                [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
                [cell.autoButton setHidden:TRUE];
                [cell.contentView setUserInteractionEnabled: NO];
            }
            else if ([resultType isEqualToString:FIELD_GOOGLE_PLACES_SEARCH])
            {
                MapPoint *details = [cellDict objectForKey:FIELD_SEARCH_RESULT];
                cell.contentLabel.text = details.name;
                [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_placesType.png"]];
                cell.detalilLabel.text = details.address;
                cell.plotButton.tag = indexPath.row;
                [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
                cell.autoButton.tag = indexPath.row;
                [cell.autoButton addTarget:self action:@selector(onAutoButton:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView setUserInteractionEnabled: NO];
            }
            return cell;
        }
        else if ([tappedIndexPath compare: indexPath] == NSOrderedSame)
        {
            if (indexPath.row >= searchResult.count)
                return nil;
            
            static NSString * CellIdentifier = @"SearchResultCell";
            SearchResultCell *cell = (SearchResultCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchResultCell"owner:self options:nil];
                cell = (SearchResultCell *)[topLevelObjects objectAtIndex:0];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            
            NSDictionary *cellDict = [searchResult objectAtIndex:indexPath.row];
            MapPoint *details = [cellDict objectForKey:FIELD_SEARCH_RESULT];
            cell.contentLabel.text = details.name;
            [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_placesType.png"]];
            cell.detalilLabel.text = details.address;
            cell.plotButton.tag = indexPath.row;
            [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
            cell.autoButton.tag = indexPath.row;
            [cell.autoButton addTarget:self action:@selector(onAutoButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView setUserInteractionEnabled: NO];
            NSDictionary *resultDict = [placeSearchDetails objectForKey:@"fields"];
            [cell.nearestStage setTitle:[resultDict objectForKey:@"display_name"] forState:UIControlStateNormal];
            if (closestDistance == 999999)
            {
                cell.distanceofStage.text = @"Not Available";
                cell.distanceofStage.textColor = mSingleton.textColor2;
            }
            else
            {
                if (closestDistance > 1000)
                    cell.distanceofStage.text = [NSString stringWithFormat:@"%.2f km",closestDistance/1000];
                else
                    cell.distanceofStage.text = [NSString stringWithFormat:@"%.2f m",closestDistance];
                
                cell.distanceofStage.textColor = mSingleton.textColor1;
            }
            //FOR MRTS
            [cell.nearestStation setTitle:[placeSearchDetails objectForKey:@"station_name"] forState:UIControlStateNormal];
            if (closestMRTS == 999999)
            {
                cell.distanceofStation.text = @"Not Available";
                cell.distanceofStation.textColor = mSingleton.textColor2;
            }
            else
            {
                if (closestMRTS > 1000)
                    cell.distanceofStation.text = [NSString stringWithFormat:@"%.2f km",closestMRTS/1000];
                else
                    cell.distanceofStation.text = [NSString stringWithFormat:@"%.2f m",closestMRTS];
                
                cell.distanceofStation.textColor = mSingleton.textColor1;
            }
            
            currentDistance = @"N/A";
            if ([mSingleton isConnectedToInternet])
                [self getCurrentDistanceToPlaceCoord:details.coordinate];
            if ([currentDistance isEqualToString:@"N/A"])
            {
                cell.distanceFromCurrentLocation.text = @"N/A";
                cell.distanceFromCurrentLocation.textColor = mSingleton.textColor2;
            }
            else
            {
                cell.distanceFromCurrentLocation.text = currentDistance;
                cell.distanceFromCurrentLocation.textColor = mSingleton.textColor1;
            }
            cell.nearestStage.tag = indexPath.row;
            [cell.nearestStage addTarget:self action:@selector(onStageDetailsButton:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.nearestStation.tag = indexPath.row;
            [cell.nearestStation addTarget:self action:@selector(onStationDetailsButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.contentView setUserInteractionEnabled: NO];
            return cell;
        }
        else
        {
            if (indexPath.row >= searchResult.count)
                return nil;
            
            static NSString * CellIdentifier = @"SearchDetailsCell";
            SearchDetailsCell *cell = (SearchDetailsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchDetailsCell"owner:self options:nil];
                cell = (SearchDetailsCell *)[topLevelObjects objectAtIndex:0];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            
            NSDictionary *cellDict = [searchResult objectAtIndex:indexPath.row];
            NSString *resultType = [cellDict objectForKey:FIELD_SEARCH_TYPE];
            if ([resultType isEqualToString:FIELD_STAGE_SEARCH])
            {
                NSString *stage = [cellDict objectForKey:FIELD_SEARCH_RESULT];
                cell.contentLabel.text = stage;
                [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_stagesType.png"]];
                if ([self isLocationAvailableforStage:stage])
                {
                    NSDictionary *stageDetailDictionay = [NSDictionary dictionaryWithDictionary:[self getDetailsForStage:stage]];
                    
                    NSString *importance, *isTermius;
                    if ([stageDetailDictionay count] > 0)
                    {
                        if (![[stageDetailDictionay valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
                        {
                            BOOL termius = [[stageDetailDictionay valueForKey:@"is_terminus"] boolValue];
                            isTermius = [NSString stringWithFormat:@"%@",termius ? @"YES" : @"NO"];
                        }
                        else
                            isTermius = @"-";
                        
                        if (![[stageDetailDictionay valueForKey:@"importance"] isKindOfClass:[NSNull class]])
                            importance = [[stageDetailDictionay valueForKey:@"importance"] stringValue];
                        else
                            importance = @"-";
                    }
                    else
                    {
                        importance = @"-";
                        isTermius = @"-";
                    }
                    cell.detalilLabel.text = [NSString stringWithFormat:@"Stage Importance: %@\nTermius: %@",importance, isTermius];
                    cell.plotButton.tag = indexPath.row;
                    [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
                    cell.autoButton.tag = indexPath.row;
                    [cell.autoButton addTarget:self action:@selector(onAutoButton:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView setUserInteractionEnabled: NO];
                }
                else
                {
                    [cell.plotButton setHidden:TRUE];
                    [cell.autoButton setHidden:TRUE];
                    cell.detalilLabel.text = @"-";
                }
            }
            else if ([resultType isEqualToString:FIELD_ROUTE_SEARCH])
            {
                NSString *route = [cellDict objectForKey:FIELD_SEARCH_RESULT];
                NSDictionary *routeDetailDictionay = [NSDictionary dictionaryWithDictionary:[self getDetailsforRoute:route]];
                cell.contentLabel.text = route;
                [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_busType.png"]];
                cell.detalilLabel.text = [NSString stringWithFormat:@"Source: %@\nDestincation: %@",[[routeDetailDictionay objectForKey:FIELD_STAGE_SOURCE] capitalizedString],[[routeDetailDictionay objectForKey:FIELD_STAGE_DESTINATION] capitalizedString]];
                cell.plotButton.tag = indexPath.row;
                [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
                [cell.autoButton setHidden:TRUE];
                
                [cell.contentView setUserInteractionEnabled: NO];
            }
            else if ([resultType isEqualToString:FIELD_GOOGLE_PLACES_SEARCH])
            {
                MapPoint *details = [cellDict objectForKey:FIELD_SEARCH_RESULT];
                cell.contentLabel.text = details.name;
                [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_placesType.png"]];
                cell.detalilLabel.text = details.address;
                cell.plotButton.tag = indexPath.row;
                [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
                cell.autoButton.tag = indexPath.row;
                [cell.autoButton addTarget:self action:@selector(onAutoButton:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView setUserInteractionEnabled: NO];
            }
            return cell;
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == searchResultTable)
    {
        NSDictionary *cellDict = [searchResult objectAtIndex:indexPath.row];
        NSString *resultType = [cellDict objectForKey:FIELD_SEARCH_TYPE];
        if ([resultType isEqualToString:FIELD_STAGE_SEARCH])
        {
            NSString *stage = [cellDict objectForKey:FIELD_SEARCH_RESULT];
            [self onStage:stage];
        }
        else if ([resultType isEqualToString:FIELD_ROUTE_SEARCH])
        {
            NSString *route = [cellDict objectForKey:FIELD_SEARCH_RESULT];
            [self onRoute:route];
        }
        else if ([resultType isEqualToString:FIELD_GOOGLE_PLACES_SEARCH])
        {
            MapPoint *details = [cellDict objectForKey:FIELD_SEARCH_RESULT];
            placeSearchDetails = [self getNearestStagefromCoord:details.coordinate];
            //[tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            for (id<MKAnnotation> annotation in mapView.annotations)
            {
                if ([annotation isKindOfClass:[MapPoint class]])
                {
                    [mapView deselectAnnotation:annotation animated:NO];
                }
            }
            if (!tappedIndexPath)
            {
                tappedIndexPath = [indexPath copy];
                [mapView selectAnnotation:details animated:YES];
            }
            else if ([tappedIndexPath compare: indexPath] == NSOrderedSame)
            {
                tappedIndexPath = nil;
            }
            else
            {
                tappedIndexPath = [indexPath copy];
                [mapView selectAnnotation:details animated:YES];
            }
            [tableView reloadData];
        }
    }
}

-(void) centerMap
{
    MKCoordinateRegion region;
    CLLocationDegrees maxLat = -90.0;
    CLLocationDegrees maxLon = -180.0;
    CLLocationDegrees minLat = 90.0;
    CLLocationDegrees minLon = 180.0;
    for(int idx = 0; idx < pointArray.count; idx++)
    {
        MapPoint *mp = [pointArray objectAtIndex:idx];
        double lat = mp.coordinate.latitude;
        double lon = mp.coordinate.longitude;
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(lat, lon);
        if(coord.latitude > maxLat)
            maxLat = coord.latitude;
        if(coord.latitude < minLat)
            minLat = coord.latitude;
        if(coord.longitude > maxLon)
            maxLon = coord.longitude;
        if(coord.longitude < minLon)
            minLon = coord.longitude;
    }
    region.center.latitude     = (maxLat + minLat) / 2.0;
    region.center.longitude    = (maxLon + minLon) / 2.0;
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    
    region.span.latitudeDelta  = ((maxLat - minLat)<0.0)?1.0:(maxLat - minLat);
    region.span.longitudeDelta = ((maxLon - minLon)<0.0)?1.0:(maxLon - minLon);
    if (pointArray.count > 0)
        [mapView setRegion:region animated:YES];
}

@end
