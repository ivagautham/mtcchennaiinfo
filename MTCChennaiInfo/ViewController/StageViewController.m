//
//  StageViewController.m
//  MTCChennaiInfo
//
//  Created by Gautham on 20/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "StageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SearchViewController.h"
#import "MapViewController.h"
#import "KeyLegendViewController.h"
#import "Singleton.h"
#import "CustomCellBackground.h"
#import "StageDetailsCell.h"
#import "RouteViewController.h"
#import "Flurry.h"
#import "MRTSViewController.h"
#import "KeyLegendViewController.h"
#import "AutoViewController.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@interface StageViewController ()
{
    BOOL didLoadfromResource;
    NSMutableArray *stageDetailArray;
    NSMutableArray *routesArray;
    NSString *sourceStage, *destinationStage, *junctionStage;
}
-(void)loadStagesFromResource;
@end

@implementation StageViewController
@synthesize allStages;
@synthesize stageDetails;
@synthesize stageBuses;
@synthesize routeInfo;
@synthesize filteredListContent;
@synthesize sourceSearchBox,destinationSearchBox,searchAssistTable,SearchDetailsTable,noMatchLabel,noMatchLabel2,searchPlacesButton;
@synthesize altenateRouteTable,alternateConnectionLabel, alternateRouteView;
@synthesize FinalDest,FinalSrc;
@synthesize sourceCurrentLocation,sourceMagGlass,destinationMagGlass,swapLocations;
@synthesize loadingSpinner;
@synthesize busServicesButton;
@synthesize autoButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        didLoadfromResource = FALSE;
        self.view.layer.cornerRadius = 10;
        UISwipeGestureRecognizer *swipeRightforKeyLegend = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onMRTS:)];
        swipeRightforKeyLegend.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer: swipeRightforKeyLegend];
        
        UISwipeGestureRecognizer *swipeLeftforHome = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onHome:)];
        swipeLeftforHome.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer: swipeLeftforHome];
        
        UISwipeGestureRecognizer *swipeDownforSearch = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSearch:)];
        swipeDownforSearch.direction = UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer: swipeDownforSearch];
        
        UISwipeGestureRecognizer *swipeUpforMap = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onAutoButton:)];
        swipeUpforMap.direction = UISwipeGestureRecognizerDirectionUp;
        [self.view addGestureRecognizer: swipeUpforMap];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!didLoadfromResource)
        [self loadStagesFromResource];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_SHOW_QOS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_HIDE_QOS object:nil];
    
    if (!sourceStage)
        sourceStage = [[NSString alloc] init];
    if (!destinationStage)
        destinationStage = [[NSString alloc] init];
    if (!junctionStage)
        junctionStage = [[NSString alloc] init];
    
    if(!FinalDest)
        FinalDest = [[NSMutableArray alloc] init];
    if(!FinalSrc)
        FinalSrc = [[NSMutableArray alloc] init];
    
    if(!loadingSpinner)
        loadingSpinner = [[UIActivityIndicatorView alloc] init];
    
    sourceSearchBox.layer.cornerRadius = 5;
    sourceSearchBox.layer.borderWidth = 2;
    sourceSearchBox.layer.borderColor = mSingleton.elementColor1.CGColor;
    [sourceSearchBox setDelegate:self];
    CustomCellBackground * backgroundView1 = [[CustomCellBackground alloc] init];
    [sourceSearchBox addSubview: backgroundView1];
    [sourceSearchBox sendSubviewToBack: backgroundView1];
    
    destinationSearchBox.layer.cornerRadius = 5;
    destinationSearchBox.layer.borderWidth = 2;
    destinationSearchBox.layer.borderColor = mSingleton.elementColor1.CGColor;
    [destinationSearchBox setDelegate:self];
    CustomCellBackground * backgroundView2 = [[CustomCellBackground alloc] init];
    [destinationSearchBox addSubview: backgroundView2];
    [destinationSearchBox sendSubviewToBack: backgroundView2];
    
    searchAssistTable.layer.cornerRadius = 5;
    searchAssistTable.layer.borderWidth = 1;
    searchAssistTable.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    //    SearchDetailsTable.layer.cornerRadius = 10;
    //    SearchDetailsTable.layer.borderWidth = 2;
    SearchDetailsTable.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    //    altenateRouteTable.layer.cornerRadius = 10;
    //    altenateRouteTable.layer.borderWidth = 2;
    altenateRouteTable.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    //[self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultNone] waitUntilDone:NO];
    
    [searchAssistTable setHidden:TRUE];
    [SearchDetailsTable setHidden:TRUE];
    [destinationSearchBox setHidden:TRUE];
    [alternateRouteView setHidden:TRUE];
    [destinationMagGlass setHidden:TRUE];
    [swapLocations setHidden:TRUE];
    
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:mSingleton.bgColor];
   
    /*
    sourceCurrentLocation.layer.cornerRadius = 5;
    sourceCurrentLocation.layer.borderWidth = 2;
    sourceCurrentLocation.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    swapLocations.layer.cornerRadius = 5;
    swapLocations.layer.borderWidth = 2;
    swapLocations.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    mapButton.layer.cornerRadius = 5;
    mapButton.layer.borderWidth = 2;
    mapButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    busServicesButton.layer.cornerRadius = 5;
    busServicesButton.layer.borderWidth = 2;
    busServicesButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    [busServicesButton setHidden:FALSE];
    
    autoButton.layer.cornerRadius = 5;
    autoButton.layer.borderWidth = 1;
    autoButton.layer.borderColor = [[UIColor blackColor] CGColor];
    */
    
    UIImage *originalImage1 = [UIImage imageNamed:@"button-bg36.png"];
    UIEdgeInsets insets1 = UIEdgeInsetsMake(5, 5, 5, 5);
    UIImage *stretchableImage1 = [originalImage1 resizableImageWithCapInsets:insets1];
    [autoButton setBackgroundImage:stretchableImage1 forState:UIControlStateNormal];
    
    UIImage *originalImage2 = [UIImage imageNamed:@"button-bg-highlighted36.png"];
    UIEdgeInsets insets2 = UIEdgeInsetsMake(5, 5, 5, 5);
    UIImage *stretchableImage2 = [originalImage2 resizableImageWithCapInsets:insets2];
    [autoButton setBackgroundImage:stretchableImage2 forState:UIControlStateHighlighted];

    
    [autoButton setHidden:TRUE];

}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!didLoadfromResource)
        [self loadStagesFromResource];
    [locationManager startUpdatingLocation];
    [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultHideSpinner] waitUntilDone:YES];
    
    [searchAssistTable setHidden:TRUE];
    [noMatchLabel2 setHidden:TRUE];
    [searchPlacesButton setHidden:TRUE];
    
    if (sourceSearchBox.text.length > 0 && !noMatchLabel.hidden)
    {
        [noMatchLabel2 setHidden:FALSE];
        [searchPlacesButton setHidden:FALSE];
        [searchPlacesButton setTitle:sourceSearchBox.text forState:UIControlStateNormal];
    }
    
    [SearchDetailsTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [altenateRouteTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!didLoadfromResource)
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadStagesFromResource) object:nil];
    
    [sourceSearchBox resignFirstResponder];
    [destinationSearchBox resignFirstResponder];
    
    if (! searchAssistTable.hidden)
        [self setTableHidden:TRUE];
    
    [locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ShowResultView:(NSNumber *)res
{
    ShowResultType result = [res intValue];
    
    if (result == ShowResultPlaces)
    {
        [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultHideSpinner] waitUntilDone:YES];
        
        [sourceSearchBox setFont:[mSingleton getItalicFontOfSize:12]];
        [destinationSearchBox setFont:[mSingleton getItalicFontOfSize:12]];
        [sourceSearchBox setTextColor:[UIColor redColor]];
        [destinationSearchBox setTextColor:[UIColor redColor]];
        
        [searchAssistTable setHidden:TRUE];
        [SearchDetailsTable setHidden:TRUE];
        [alternateRouteView setHidden:TRUE];
        
        [noMatchLabel setHidden:FALSE];
        [noMatchLabel2 setHidden:FALSE];
        [searchPlacesButton setHidden:FALSE];
        [searchPlacesButton setTitle:sourceSearchBox.text forState:UIControlStateNormal];
       
        [autoButton setHidden:TRUE];
    }
    else if (result == ShowResultNone)
    {
        [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultHideSpinner] waitUntilDone:YES];
        
        [sourceSearchBox setFont:[mSingleton getItalicFontOfSize:12]];
        [destinationSearchBox setFont:[mSingleton getItalicFontOfSize:12]];
        [sourceSearchBox setTextColor:[UIColor redColor]];
        [destinationSearchBox setTextColor:[UIColor redColor]];
        
        [searchAssistTable setHidden:TRUE];
        [SearchDetailsTable setHidden:TRUE];
        [alternateRouteView setHidden:TRUE];
        
        [noMatchLabel setHidden:FALSE];
        
        [autoButton setHidden:TRUE];
    }
    else if (result == ShowResultPerfect)
    {
        [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultHideSpinner] waitUntilDone:YES];
        [sourceSearchBox setFont:[mSingleton getFontOfSize:14]];
        [destinationSearchBox setFont:[mSingleton getFontOfSize:14]];
        [sourceSearchBox setTextColor:[UIColor blackColor]];
        [destinationSearchBox setTextColor:[UIColor blackColor]];
        
        [noMatchLabel setHidden:TRUE];
        [noMatchLabel2 setHidden:TRUE];
        [searchPlacesButton setHidden:TRUE];
        
        [alternateRouteView setHidden:TRUE];
        [self.SearchDetailsTable setHidden:FALSE];
        
        BOOL shouldShowAutoBUtton = [self isLocationAvailableforStage:sourceSearchBox.text];
        if (destinationSearchBox.text && ![destinationSearchBox.text isEqualToString:@""])
            shouldShowAutoBUtton = shouldShowAutoBUtton && [self isLocationAvailableforStage:destinationSearchBox.text];
        
        [autoButton setHidden:!shouldShowAutoBUtton];
    }
    else if (result == ShowResultAlternate)
    {
        [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultHideSpinner] waitUntilDone:YES];
        [sourceSearchBox setFont:[mSingleton getFontOfSize:14]];
        [destinationSearchBox setFont:[mSingleton getFontOfSize:14]];
        [sourceSearchBox setTextColor:[UIColor blackColor]];
        [destinationSearchBox setTextColor:[UIColor blackColor]];
        
        [noMatchLabel setHidden:TRUE];
        [noMatchLabel2 setHidden:TRUE];
        [searchPlacesButton setHidden:TRUE];
        [self.SearchDetailsTable setHidden:TRUE];
        [alternateRouteView setHidden:FALSE];
        
        BOOL shouldShowAutoBUtton = [self isLocationAvailableforStage:sourceSearchBox.text];
        if (destinationSearchBox.text && ![destinationSearchBox.text isEqualToString:@""])
            shouldShowAutoBUtton = shouldShowAutoBUtton && [self isLocationAvailableforStage:destinationSearchBox.text];
        
        [autoButton setHidden:!shouldShowAutoBUtton];
    }
    else if (result == ShowResultShowDestination)
    {
        destinationSearchBox.hidden = FALSE;
        destinationSearchBox.alpha = 1.0;
        [destinationMagGlass setHidden:FALSE];
        [swapLocations setHidden:FALSE];
    }
    else if (result == ShowResultDimDestination)
    {
        destinationSearchBox.hidden = FALSE;
        destinationSearchBox.text = @"";
        destinationSearchBox.alpha = 0.6;
        [destinationMagGlass setHidden:FALSE];
        [swapLocations setHidden:FALSE];
    }
    else if (result == ShowResultHideDestination)
    {
        destinationSearchBox.text = @"";
        [destinationMagGlass setHidden:TRUE];
        [swapLocations setHidden:TRUE];
        destinationSearchBox.hidden = TRUE;
    }
    else if (result == ShowResultHideResponder)
    {
        [sourceSearchBox resignFirstResponder];
        [destinationSearchBox resignFirstResponder];
    }
    else if (result == ShowResultShowSpinner)
    {
        [searchAssistTable setHidden:TRUE];
        [SearchDetailsTable setHidden:TRUE];
        [alternateRouteView setHidden:TRUE];
        [loadingSpinner setHidden:FALSE];
        [loadingSpinner startAnimating];
        [noMatchLabel setHidden:TRUE];
        [noMatchLabel2 setHidden:TRUE];
        [searchPlacesButton setHidden:TRUE];
    }
    else if (result == ShowResultHideSpinner)
    {
        [loadingSpinner setHidden:TRUE];
        [loadingSpinner stopAnimating];
    }
    else if (result == ShowResultDisableResponder)
    {
        [sourceSearchBox resignFirstResponder];
        [destinationSearchBox resignFirstResponder];
        [sourceSearchBox setUserInteractionEnabled:FALSE];
        [destinationSearchBox setUserInteractionEnabled:FALSE];
    }
    else if (result == ShowResultEnableResponder)
    {
        [sourceSearchBox setUserInteractionEnabled:TRUE];
        [destinationSearchBox setUserInteractionEnabled:TRUE];
        
        [noMatchLabel2 setHidden:TRUE];
        [searchPlacesButton setHidden:TRUE];
        if (sourceSearchBox.text.length > 0 && !noMatchLabel.hidden)
        {
            [noMatchLabel2 setHidden:FALSE];
            [searchPlacesButton setHidden:FALSE];
            [searchPlacesButton setTitle:sourceSearchBox.text forState:UIControlStateNormal];
        }
    }
}

-(BOOL)isLocationAvailableforStage:(NSString *)stage
{
    for (NSDictionary *dict in stageDetails) {
        NSString *dictValue = [[dict valueForKey:@"fields"] valueForKey:@"display_name"];
        id latitude = [[dict valueForKey:@"fields"] valueForKey:@"latitude"];
        id longitude = [[dict valueForKey:@"fields"] valueForKey:@"longitude"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]] && ![latitude isMemberOfClass:[NSNull class]] && ![longitude isMemberOfClass:[NSNull class]]) {
            return YES;
        }
    }
    return NO;
}

-(void)loadStagesFromResource
{
    didLoadfromResource = FALSE;
    [allStages removeAllObjects];
    allStages = [mSingleton.allStages mutableCopy];
    
    [stageBuses removeAllObjects];
    stageBuses = [mSingleton.stageBuses mutableCopy];
    
    [stageDetails removeAllObjects];
    stageDetails = [mSingleton.stageDetails mutableCopy];
    
    [routeInfo removeAllObjects];
    routeInfo = [mSingleton.routeInfo mutableCopy];
    
    if (!filteredListContent)
        filteredListContent = [[NSMutableArray alloc] initWithArray:allStages copyItems:YES];
    
    if(!routesArray)
        routesArray = [[NSMutableArray alloc] init];
    
    didLoadfromResource = TRUE;
}

-(IBAction)onHome:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = @"cube";
    transition.subtype = kCATransitionFromRight;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(IBAction)onSearch:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromBottom;
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[SearchViewController class]]) {
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    
    SearchViewController *searchViewController = [[mSingleton getViewControllerManagerInstance] getSearchViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchViewController animated:NO];
}

-(IBAction)onMap:(id)sender
{
    NSMutableDictionary *sourceStageMarker = [self getLocationDetailsForStage:sourceSearchBox.text];
    NSMutableDictionary *destinationStageMarker = [self getLocationDetailsForStage:destinationSearchBox.text];
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    [mapViewController plotSorce:sourceStageMarker andDestination:destinationStageMarker];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
}

-(IBAction)onMRTS:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromLeft;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[MRTSViewController class]]) {
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    MRTSViewController *MRTSViewController = [[mSingleton getViewControllerManagerInstance] getMRTSViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:MRTSViewController animated:NO];
}

-(IBAction)onAutoButton:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromTop;
    MapPoint *stageMapPoint = nil;
    MapPoint *destinationMapPoint = nil;
    stageMapPoint = [self getLocationMarkerForStage:sourceSearchBox.text];
    destinationMapPoint = [self getLocationMarkerForStage:destinationSearchBox.text];

    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[AutoViewController class]]) {
            AutoViewController *autoViewController = (AutoViewController *)vc;
            if (stageMapPoint && destinationMapPoint)
            {
                autoViewController.sourcePoint = stageMapPoint;
                autoViewController.destinationPoint = destinationMapPoint;
            }
            else if(stageMapPoint)
            {
                if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
                {
                    autoViewController.destinationPoint = autoViewController.sourcePoint;
                    autoViewController.sourcePoint = stageMapPoint;
                }
                else
                {
                    autoViewController.sourcePoint = stageMapPoint;
                }
 
            }
            else if(destinationMapPoint)
            {
                if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
                {
                    autoViewController.destinationPoint = autoViewController.sourcePoint;
                    autoViewController.sourcePoint = destinationMapPoint;
                }
                else
                {
                    autoViewController.sourcePoint = destinationMapPoint;
                }
            }
            [autoViewController updateLocationDetails];
            
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:autoViewController animated:NO];
            return;
        }
    }
    AutoViewController *autoViewController = [[mSingleton getViewControllerManagerInstance] getAutoViewController];
    if (stageMapPoint && destinationMapPoint)
    {
        autoViewController.sourcePoint = stageMapPoint;
        autoViewController.destinationPoint = destinationMapPoint;
    }
    else if(stageMapPoint)
    {
        if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
        {
            autoViewController.destinationPoint = autoViewController.sourcePoint;
            autoViewController.sourcePoint = stageMapPoint;
        }
        else
        {
            autoViewController.sourcePoint = stageMapPoint;
        }
        
    }
    else if(destinationMapPoint)
    {
        if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
        {
            autoViewController.destinationPoint = autoViewController.sourcePoint;
            autoViewController.sourcePoint = destinationMapPoint;
        }
        else
        {
            autoViewController.sourcePoint = destinationMapPoint;
        }
    }
    [autoViewController updateLocationDetails];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:autoViewController animated:NO];
}

-(IBAction)onSourceCurrentLocation:(id)sender
{
    NSDictionary *requiredDictionary = nil;
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
    {
        CLLocationDistance nearest = 999999;
        CLLocation *searchLocation = [[CLLocation alloc] initWithLatitude:currentCentre.latitude longitude:currentCentre.longitude];
        for (NSDictionary *dict in stageDetails)
        {
            NSDictionary *childDict = [dict valueForKey:@"fields"];
            if ((![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]]) && (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]]))
            {
                if ([allStages containsObject:[[childDict valueForKey:@"mtc_name"] uppercaseString]])
                {
                    double latti = [[childDict valueForKey:@"latitude"] doubleValue];
                    double longi = [[childDict valueForKey:@"longitude"] doubleValue];
                    CLLocation *neatestLocation = [[CLLocation alloc] initWithLatitude:latti longitude:longi];
                    CLLocationDistance meters = [searchLocation distanceFromLocation:neatestLocation];
                    if (meters <= nearest) {
                        nearest = meters;
                        requiredDictionary = dict;
                    }
                }
            }
        }
    }
    if(requiredDictionary)
    {
        sourceSearchBox.text = [[[requiredDictionary valueForKey:@"fields"] valueForKey:@"mtc_name"] uppercaseString];
        NSDictionary *eventLocation = [NSDictionary dictionaryWithObjectsAndKeys:sourceSearchBox.text,@"source",destinationSearchBox.text,@"destination", nil];
        [self performSelectorInBackground:@selector(doStageDetailsUpdate:) withObject:eventLocation];        //[self updateDetailsTableForSource:sourceSearchBox.text andDestination:destinationSearchBox.text];
    }
}

-(IBAction)onSwapLocations:(id)sender
{
    [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultShowDestination] waitUntilDone:NO];
    
    NSString *swapVar = sourceSearchBox.text;
    sourceSearchBox.text = destinationSearchBox.text;
    destinationSearchBox.text = swapVar;
    
    if (sourceSearchBox.text.length == 0)
        [self onSourceCurrentLocation:nil];
    
    
    if (! searchAssistTable.hidden)
        [self setTableHidden:TRUE];
    
    if ([sourceSearchBox.text isEqualToString:destinationSearchBox.text])
    {
        destinationSearchBox.text = @"";
        NSDictionary *eventLocation = [NSDictionary dictionaryWithObjectsAndKeys:sourceSearchBox.text,@"source",destinationSearchBox.text,@"destination", nil];
        [self performSelectorInBackground:@selector(doStageDetailsUpdate:) withObject:eventLocation];
    }
    else
    {
        NSDictionary *eventLocation = [NSDictionary dictionaryWithObjectsAndKeys:sourceSearchBox.text,@"source",destinationSearchBox.text,@"destination", nil];
        [self performSelectorInBackground:@selector(doStageDetailsUpdate:) withObject:eventLocation];
        
    }
}

-(IBAction)searchPlace:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromBottom;
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[SearchViewController class]]) {
            SearchViewController *currentVC = (SearchViewController *)vc;
            NSString *toSearch = [searchPlacesButton titleForState:UIControlStateNormal];
            [currentVC searchPlace:toSearch];
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    
    SearchViewController *searchViewController = [[mSingleton getViewControllerManagerInstance] getSearchViewController];
    NSString *toSearch = [searchPlacesButton titleForState:UIControlStateNormal];
    [searchViewController searchPlace:toSearch];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchViewController animated:NO];
}

-(IBAction)onBusServices:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    KeyLegendViewController *keyLegendViewController = [[mSingleton getViewControllerManagerInstance] getKeyLegendViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:keyLegendViewController animated:NO];
}

-(NSMutableDictionary *)getLocationDetailsForStage:(NSString *)stage
{
    NSMutableDictionary *Marker;
    for (NSDictionary *dict in stageDetails) {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[NSMutableDictionary alloc] init];
            [Marker setValue:stage forKey:@"stage"];
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                id latitude = [childDict valueForKey:@"latitude"];
                [Marker setValue:latitude forKey:@"latitude"];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                id longitude = [childDict valueForKey:@"longitude"];
                [Marker setValue:longitude forKey:@"longitude"];
            }
            if (![[childDict valueForKey:@"importance"] isKindOfClass:[NSNull class]])
            {
                id importance = [childDict valueForKey:@"importance"];
                [Marker setValue:importance forKey:@"importance"];
            }
            if (![[childDict valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
            {
                id is_terminus = [childDict valueForKey:@"is_terminus"];
                [Marker setValue:is_terminus forKey:@"is_terminus"];
            }
        }
        
    }
    return Marker;
}

-(MapPoint *)getLocationMarkerForStage:(NSString *)stage
{
    MapPoint *Marker;
    
    CLLocationDegrees latitude;
	CLLocationDegrees longitude;
    for (NSDictionary *dict in stageDetails)
    {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[MapPoint alloc] init];
            Marker.name = stage;
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                latitude = [[childDict valueForKey:@"latitude"] doubleValue];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                longitude = [[childDict valueForKey:@"longitude"] doubleValue];
            }
        }
    }
    Marker.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    return Marker;
}

-(void)setTableHidden:(BOOL)flag
{
    [searchAssistTable setHidden:FALSE];
    NSTimeInterval animationDuration = 0.5;/* determine length of animation */;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    if (flag)
        searchAssistTable.frame = CGRectMake(searchAssistTable.frame.origin.x, searchAssistTable.frame.origin.y, searchAssistTable.frame.size.width, 0);
    else
        searchAssistTable.frame = CGRectMake(searchAssistTable.frame.origin.x, searchAssistTable.frame.origin.y, searchAssistTable.frame.size.width, searchAssistTable.frame.size.height);
    
    [UIView commitAnimations];
    [noMatchLabel setHidden:!flag];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
    for (NSString *section in self.allStages) {
        if ([[section uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound)
        {
            [self.filteredListContent addObject:section];
        }
    }
    
    if([self.filteredListContent count] == 0)
    {
        [searchAssistTable setHidden:TRUE];
        [noMatchLabel setHidden:FALSE];
    }
    else
        if (searchAssistTable.hidden)
            [self setTableHidden:FALSE];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSString *string in self.filteredListContent)
    {
        if ([string hasPrefix:searchText])
            [tempArray addObject:string];
    }
    
    [self.filteredListContent removeObjectsInArray:tempArray];
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [tempArray count])];
    [self.filteredListContent insertObjects:tempArray atIndexes:indexSet];
    
    [self.searchAssistTable reloadData];
}

-(void)doStageDetailsUpdate:(NSDictionary *)place
{
    NSString *sourceString = [place valueForKey:@"source"];
    NSString *destionationString = [place valueForKey:@"destination"];
    
    if ([sourceString isEqualToString:destionationString])
    {
        destionationString = @"";
        [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultDimDestination] waitUntilDone:NO];
    }
    
    NSDictionary *articleParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     sourceString, @"source",
     destionationString, @"destination",
     nil];
    
    [Flurry logEvent:@"Searched_Stage" withParameters:articleParams];
    
    if (destionationString.length == 0)
        destionationString = nil;
    
    [self updateDetailsTableForSource:sourceString andDestination:destionationString];
}

-(void)updateDetailsTableForSource:(NSString *)source andDestination:(NSString *)destincation
{
    [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultDisableResponder] waitUntilDone:NO];
    [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultShowSpinner] waitUntilDone:NO];
    [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultHideResponder] waitUntilDone:NO];
    sourceStage = [source capitalizedString];
    destinationStage = [destincation capitalizedString];
    
    if (!didLoadfromResource)
        [self loadStagesFromResource];
    
    [sourceSearchBox performSelectorOnMainThread:@selector(setText:) withObject:source waitUntilDone:YES];
    [destinationSearchBox performSelectorOnMainThread:@selector(setText:) withObject:destincation waitUntilDone:YES];
    
    if(!stageDetailArray)
        stageDetailArray = [[NSMutableArray alloc] init];
    [stageDetailArray removeAllObjects];
    [stageDetailArray setArray:[self getDetailsforStageSource:source]];
    if (!destincation || destincation.length == 0)
    {
        destinationSearchBox.text = @"";
        destinationSearchBox.alpha = 0.6;
        
        if(stageDetailArray && [stageDetailArray count] > 0)
        {
            [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultDimDestination] waitUntilDone:NO];
            [routesArray removeAllObjects];
            NSDictionary *result = [stageDetailArray objectAtIndex:0];
            NSArray *routes = [result valueForKey:FIELD_LIST_OF_ROUTES];
            [routesArray addObjectsFromArray:routes];
            
            [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultPerfect] waitUntilDone:NO];
            
            [self.SearchDetailsTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }
        else
        {
            [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultPlaces] waitUntilDone:NO];
        }
    }
    else
    {
        [stageDetailArray addObjectsFromArray:[self getDetailsforStageSource:destincation]];
        [routesArray removeAllObjects];
        if ([stageDetailArray count] > 0)
        {
            NSMutableArray *routes = [[NSMutableArray alloc] init];
            for (NSMutableDictionary *resultDict in stageDetailArray )
                [routes addObjectsFromArray:[resultDict valueForKey:FIELD_LIST_OF_ROUTES]];
            
            for (NSDictionary *dict in routes)
            {
                BOOL result = [self checkForDestination:destincation andSource:source inRote:[dict valueForKey:@"field_stage_number"]];
                if (result)
                    [routesArray addObject:dict];
            }
        }
        if(routesArray && [routesArray count] > 0)
        {
            [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultPerfect] waitUntilDone:NO];
            
            [self.SearchDetailsTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }
        else
        {
            //Check for multiple routes
            NSString * junction = [self checkNearestJunctionForDestination:destincation andSource:source];
            junctionStage = [junction capitalizedString];
            if(!junction)
            {
                [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultNone] waitUntilDone:NO];
            }
            else
            {
                NSMutableArray *arraySrc = [[NSMutableArray alloc] init];
                [FinalSrc removeAllObjects];
                [arraySrc setArray:[self getDetailsforStageSource:source]];
                [arraySrc setArray:[self getDetailsforStageSource:[junction uppercaseString]]];
                if ([arraySrc count] >0)
                {
                    NSDictionary *resultSrc = [arraySrc objectAtIndex:0];
                    NSArray *srcRoutes = [resultSrc valueForKey:FIELD_LIST_OF_ROUTES];
                    for (NSDictionary *dict in srcRoutes)
                    {
                        BOOL result = [self checkForDestination:source andSource:[junction uppercaseString] inRote:[dict valueForKey:@"field_stage_number"]];
                        if (result)
                            [FinalSrc addObject:dict];
                    }
                }
                NSMutableArray *arrayDest = [[NSMutableArray alloc] init];
                [FinalDest removeAllObjects];
                [arrayDest setArray:[self getDetailsforStageSource:[junction uppercaseString]]];
                [arrayDest setArray:[self getDetailsforStageSource:destincation]];
                if ([arrayDest count] >0)
                {
                    NSDictionary *resultDest = [arrayDest objectAtIndex:0];
                    NSArray *destRoutes = [resultDest valueForKey:FIELD_LIST_OF_ROUTES];
                    for (NSDictionary *dict in destRoutes)
                    {
                        BOOL result = [self checkForDestination:[junction uppercaseString] andSource:destincation inRote:[dict valueForKey:@"field_stage_number"]];
                        if (result)
                            [FinalDest addObject:dict];
                    }
                }
                if ([FinalDest count] > 0 && [FinalSrc count] > 0)
                {
                    NSString *alterJunction = [NSString stringWithFormat:@"  No direct buses.\n  Nearest junction:%@",[junctionStage uppercaseString]];
                    [alternateConnectionLabel performSelectorOnMainThread:@selector(setText:) withObject:alterJunction waitUntilDone:YES];
                    [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultAlternate] waitUntilDone:NO];
                    [self.altenateRouteTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
                }
                else
                {
                    [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultNone] waitUntilDone:NO];
                }
            }
        }
    }
    [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultEnableResponder] waitUntilDone:NO];
}

-(NSString *)checkNearestJunctionForDestination:(NSString *)destination andSource:(NSString *)source
{
    NSArray *sourceArray = [self getDetailsforStageSource:source];
    NSArray *destinationArray = [self getDetailsforStageSource:destination];
    
    NSMutableArray *sourceRouteArray = [[NSMutableArray alloc] init];
    NSMutableArray *destinationRouteArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *sourceStageArray = [[NSMutableArray alloc] init];
    NSMutableArray *destinationStageArray = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary *resultDict in sourceArray )
        [sourceRouteArray addObjectsFromArray:[resultDict valueForKey:FIELD_LIST_OF_ROUTES]];
    
    for (NSMutableDictionary *resultDict in destinationArray )
        [destinationRouteArray addObjectsFromArray:[resultDict valueForKey:FIELD_LIST_OF_ROUTES]];
    
    for (NSDictionary *stages in sourceRouteArray)
    {
        NSDictionary *routeDict = [self getDetailsforRoute:[stages valueForKey:@"field_stage_number"]];
        [sourceStageArray addObjectsFromArray:[routeDict valueForKey:@"field_stage_route"]];
    }
    
    for (NSDictionary *stages in destinationRouteArray)
    {
        NSDictionary *routeDict = [self getDetailsforRoute:[stages valueForKey:@"field_stage_number"]];
        [destinationStageArray addObjectsFromArray:[routeDict valueForKey:@"field_stage_route"]];
    }
    
    NSMutableSet *intersection = [NSMutableSet setWithArray:sourceStageArray];
    [intersection intersectSet:[NSSet setWithArray:destinationStageArray]];
    
    NSArray *resultArray = [intersection allObjects];
    NSDictionary *sourceDetails = [self getDetailsForStage:source];
    double sourceLatti = [[sourceDetails valueForKey:@"latitude"] doubleValue];
    double sourceLongi = [[sourceDetails valueForKey:@"longitude"] doubleValue];
    CLLocation *sourceLocation = [[CLLocation alloc] initWithLatitude:sourceLatti longitude:sourceLongi];
    
    NSString *importanctJunction = nil;
    CLLocationDistance nearest = 999999;
    for (NSString *stage in resultArray)
    {
        NSDictionary *dict = [self getDetailsForStage:stage];
        double latti = [[dict valueForKey:@"latitude"] doubleValue];
        double longi = [[dict valueForKey:@"longitude"] doubleValue];
        CLLocation *neatestLocation = [[CLLocation alloc] initWithLatitude:latti longitude:longi];
        CLLocationDistance meters = [neatestLocation distanceFromLocation:sourceLocation];
        if (meters <= nearest)
        {
            nearest = meters;
            importanctJunction = stage;
        }
    }
    return importanctJunction;
}

-(BOOL)checkForDestination:(NSString *)destination andSource:(NSString *)source inRote:(NSString *)route
{
    BOOL src = FALSE;
    BOOL dest = FALSE;
    NSDictionary *routeDict = [self getDetailsforRoute:route];
    NSArray *routeStages = [routeDict valueForKey:@"field_stage_route"];
    for (NSString *stages in routeStages)
    {
        if ([[destination uppercaseString] isEqualToString:[stages uppercaseString]])
        {
            dest = TRUE;
        }
        else if ([[source uppercaseString] isEqualToString:[stages uppercaseString]])
        {
            src = TRUE;
        }
    }
    return (dest && src);
}

-(NSDictionary *)getDetailsforRoute:(NSString *)route
{
    for (NSDictionary *dict in routeInfo) {
        if ([[[dict objectForKey:FIELD_STAGE_NUMBER] uppercaseString] isEqual:[route uppercaseString]]) {
            return dict;
        }
    }
    return nil;
}

-(NSMutableArray *)getDetailsforStageSource:(NSString *)source
{
    NSMutableArray* newArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in stageBuses) {
        if ([[[dict objectForKey:FIELD_SOURCE_STAGE] uppercaseString] isEqual:[source uppercaseString]]) {
            [newArray addObject:dict];
            break;
        }
    }
    return newArray;
}

-(NSString *)getServiceTypeforRoute:(NSString *)route
{
    for (NSDictionary *dict in routeInfo) {
        if ([[[dict objectForKey:@"field_stage_number"] uppercaseString] isEqual:[route uppercaseString]])
            return [[dict objectForKey:@"field_stage_type"] capitalizedString];
    }
    return @"-";
}

-(NSMutableDictionary *)getDetailsForStage:(NSString *)stage
{
    NSMutableDictionary *Marker;
    for (NSDictionary *dict in stageDetails) {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[NSMutableDictionary alloc] init];
            [Marker setValue:stage forKey:@"stage"];
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                id latitude = [childDict valueForKey:@"latitude"];
                [Marker setValue:latitude forKey:@"latitude"];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                id longitude = [childDict valueForKey:@"longitude"];
                [Marker setValue:longitude forKey:@"longitude"];
            }
            if (![[childDict valueForKey:@"importance"] isKindOfClass:[NSNull class]])
            {
                id importance = [childDict valueForKey:@"importance"];
                [Marker setValue:importance forKey:@"importance"];
            }
            if (![[childDict valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
            {
                id is_terminus = [childDict valueForKey:@"is_terminus"];
                [Marker setValue:is_terminus forKey:@"is_terminus"];
            }
        }
    }
    return Marker;
}

#pragma mark - CLLocationManager methods.
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
    {
        currentCentre = newLocation.coordinate;
    }
    else
    {
        currentCentre.latitude = 13.052413900000000000;
        currentCentre.longitude = 80.250824599999990000;
    }
}
#pragma mark-
#pragma mark tableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == searchAssistTable)
    {
        CGRect tableHeight = searchAssistTable.frame;
        if (IS_IPHONE_5)
        {
            if ([filteredListContent count] < 4)
            {
                tableHeight.size.height = 44 * [filteredListContent count];
            }
            else
            {
                tableHeight.size.height = 176;
                [searchAssistTable flashScrollIndicators];
            }
        }
        else
        {
            if ([filteredListContent count] < 3)
            {
                tableHeight.size.height = 44 * [filteredListContent count];
            }
            else
            {
                tableHeight.size.height = 132;
                [searchAssistTable flashScrollIndicators];
            }
        }
        searchAssistTable.frame = tableHeight;
        return [filteredListContent count];
    }
    else if (tableView == SearchDetailsTable && [stageDetailArray count] > 0 && [routesArray count] > 0)
    {
        //Strip duplicate in routesArray
        NSArray *copy = [routesArray copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([routesArray indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [routesArray removeObjectAtIndex:index];
            }
            index--;
        }
        
        CGRect tableHeight = SearchDetailsTable.frame;
        if (IS_IPHONE_5)
        {
            if ([routesArray count] < 5)
                tableHeight.size.height =10 + (44 * ([routesArray count] +1));
            else
                tableHeight.size.height = 286;
        }
        else
        {
            if ([routesArray count] < 3)
                tableHeight.size.height = 10 + (44 * ([routesArray count] +1));
            else
                tableHeight.size.height = 220;
            
        }
        SearchDetailsTable.frame = tableHeight;
        return [routesArray count];
    }
    else if (tableView == altenateRouteTable)
    {
        //Strip duplicate in routesArray
        NSArray *copy = [FinalSrc copy];
        NSInteger index = [copy count] - 1;
        for (id object in [copy reverseObjectEnumerator]) {
            if ([FinalSrc indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                [FinalSrc removeObjectAtIndex:index];
            }
            index--;
        }
        
        NSArray *copy2 = [FinalDest copy];
        NSInteger index2 = [copy2 count] - 1;
        for (id object in [copy2 reverseObjectEnumerator]) {
            if ([FinalDest indexOfObject:object inRange:NSMakeRange(0, index2)] != NSNotFound) {
                [FinalDest removeObjectAtIndex:index2];
            }
            index2--;
        }
        
        CGRect tableHeight = altenateRouteTable.frame;
        if (IS_IPHONE_5)
        {
            if ([FinalDest count] + [FinalSrc count] < 4)
                tableHeight.size.height = 10 + (44 * (([FinalDest count] + [FinalSrc count]) +2));
            else
                tableHeight.size.height = 286;
        }
        else
        {
            if ([FinalDest count] + [FinalSrc count] < 3)
                tableHeight.size.height = 10 + (44 * (([FinalDest count] + [FinalSrc count]) +2));
            else
                tableHeight.size.height = 220;
        }
        altenateRouteTable.frame = tableHeight;
        
        if (section == 1)
            return [FinalSrc count];
        else if(section == 2)
            return [FinalDest count];
        else
            return 0;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == searchAssistTable || tableView == SearchDetailsTable)
        return 1;
    else if (tableView == altenateRouteTable)
        return 3;
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == searchAssistTable)
    {
        if (indexPath.row >= filteredListContent.count)
            return nil;
        
        static NSString * CellIdentifier = @"searchAssistTableCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        cell.textLabel.text = [filteredListContent objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        
        return cell;
    }
    if (tableView == SearchDetailsTable)
    {
        if (indexPath.row >= routesArray.count)
            return nil;
        
        static NSString * CellIdentifier = @"SearchDetailsTableCell";
        StageDetailsCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StageDetailsCell" owner:self options:nil];
            cell = (StageDetailsCell *)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        NSDictionary *routeDetail = [routesArray objectAtIndex:(indexPath.row)];
        
        if ([[routeDetail valueForKey:FIELD_STAGE_NUMBER] isEqualToString:@"Error"])
        {
            cell.route.text = [routeDetail valueForKey:@"field_stage_destination"];
            cell.source.text = [[routeDetail valueForKey:@"field_stage_type"] capitalizedString];
            cell.destination.text = [[routeDetail valueForKey:@"field_stage_source"] capitalizedString];
        }
        else
        {
            cell.route.text = [routeDetail valueForKey:FIELD_STAGE_NUMBER];
            cell.source.text = [[routeDetail valueForKey:FIELD_STAGE_SOURCE] capitalizedString];
            cell.destination.text = [[routeDetail valueForKey:FIELD_STAGE_DESTINATION] capitalizedString];
        }
        cell.service.text = [self getServiceTypeforRoute:cell.route.text];
        
        cell.route.backgroundColor = [UIColor clearColor];
        cell.source.backgroundColor = [UIColor clearColor];
        cell.destination.backgroundColor = [UIColor clearColor];
        cell.service.backgroundColor = [UIColor clearColor];
        
        cell.route.textColor = mSingleton.textColor1;
        cell.source.textColor = mSingleton.textColor1;
        cell.destination.textColor = mSingleton.textColor1;
        cell.service.textColor = mSingleton.textColor1;
        
        cell.route.font = [mSingleton getBoldFontOfSize:15];
        cell.source.font = [mSingleton getBoldFontOfSize:11];
        cell.destination.font = [mSingleton getBoldFontOfSize:11];
        cell.service.font = [mSingleton getBoldFontOfSize:11];
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        return cell;
    }
    if (tableView == altenateRouteTable)
    {
        static NSString * CellIdentifier = @"SearchDetailsTableCell";
        StageDetailsCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StageDetailsCell" owner:self options:nil];
            cell = (StageDetailsCell *)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.route.backgroundColor = [UIColor clearColor];
        cell.source.backgroundColor = [UIColor clearColor];
        cell.destination.backgroundColor = [UIColor clearColor];
        cell.service.backgroundColor = [UIColor clearColor];
        
        cell.route.textColor = mSingleton.textColor1;
        cell.source.textColor = mSingleton.textColor1;
        cell.destination.textColor = mSingleton.textColor1;
        cell.service.textColor = mSingleton.textColor1;
        
        cell.route.font = [mSingleton getBoldFontOfSize:15];
        cell.source.font = [mSingleton getBoldFontOfSize:11];
        cell.destination.font = [mSingleton getBoldFontOfSize:11];
        cell.service.font = [mSingleton getBoldFontOfSize:11];
        
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        if (indexPath.section == 1) {
            ((CustomCellBackground *) cell.backgroundView).lastCell = indexPath.row == self.filteredListContent.count - 1;
            //((CustomCellBackground *)cell.selectedBackgroundView).lastCell = indexPath.row == self.filteredListContent.count - 1;
        }
        if (indexPath.section == 1)
        {
            if (indexPath.row >= FinalSrc.count)
                return nil;
            
            NSDictionary *routeDetail = [FinalSrc objectAtIndex:(indexPath.row)];
            if ([[routeDetail valueForKey:FIELD_STAGE_NUMBER] isEqualToString:@"Error"])
            {
                cell.route.text = [routeDetail valueForKey:@"field_stage_destination"];
                cell.source.text = [[routeDetail valueForKey:@"field_stage_type"] capitalizedString];
                cell.destination.text = [[routeDetail valueForKey:@"field_stage_source"] capitalizedString];
            }
            else
            {
                cell.route.text = [routeDetail valueForKey:FIELD_STAGE_NUMBER];
                cell.source.text = [[routeDetail valueForKey:FIELD_STAGE_SOURCE] capitalizedString];
                cell.destination.text = [[routeDetail valueForKey:FIELD_STAGE_DESTINATION] capitalizedString];
            }
            cell.service.text = [self getServiceTypeforRoute:cell.route.text];
        }
        else if (indexPath.section == 2)
        {
            if (indexPath.row >= FinalDest.count)
                return nil;
            
            NSDictionary *routeDetail = [FinalDest objectAtIndex:(indexPath.row)];
            if ([[routeDetail valueForKey:FIELD_STAGE_NUMBER] isEqualToString:@"Error"])
            {
                cell.route.text = [routeDetail valueForKey:@"field_stage_destination"];
                cell.source.text = [[routeDetail valueForKey:@"field_stage_type"] capitalizedString];
                cell.destination.text = [[routeDetail valueForKey:@"field_stage_source"] capitalizedString];
            }
            else
            {
                cell.route.text = [routeDetail valueForKey:FIELD_STAGE_NUMBER];
                cell.source.text = [[routeDetail valueForKey:FIELD_STAGE_SOURCE] capitalizedString];
                cell.destination.text = [[routeDetail valueForKey:FIELD_STAGE_DESTINATION] capitalizedString];
            }
            cell.service.text = [self getServiceTypeforRoute:cell.route.text];
        }
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == SearchDetailsTable)
        return 44.0;
    
    if (tableView == altenateRouteTable)
    {
        if (section == 0)
            return 44.0;
        
        return 22.0;
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == altenateRouteTable)
    {
        if (section == 1)
            return [NSString stringWithFormat:@"%@ to %@", sourceStage, junctionStage];
        else if (section == 2)
            return [NSString stringWithFormat:@"%@ to %@", junctionStage, destinationStage];
    }
    return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView;
    if (tableView == SearchDetailsTable)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StageDetailsCell" owner:self options:nil];
        sectionView = (StageDetailsCell *)[nib objectAtIndex:0];
    }
    if (tableView == altenateRouteTable)
    {
        if (section == 0)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StageDetailsCell" owner:self options:nil];
            sectionView = (StageDetailsCell *)[nib objectAtIndex:0];
        }
    }
    return sectionView;
}

#pragma mark-
#pragma mark tableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == searchAssistTable)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (! searchAssistTable.hidden)
            [self setTableHidden:TRUE];
        
        if ([sourceSearchBox isFirstResponder])
            sourceSearchBox.text = cell.textLabel.text;
        
        else if ([destinationSearchBox isFirstResponder])
            destinationSearchBox.text = cell.textLabel.text;
        NSDictionary *eventLocation = [NSDictionary dictionaryWithObjectsAndKeys:sourceSearchBox.text,@"source",destinationSearchBox.text,@"destination", nil];
        [self performSelectorInBackground:@selector(doStageDetailsUpdate:) withObject:eventLocation];
        //[self updateDetailsTableForSource:sourceSearchBox.text andDestination:destinationSearchBox.text];
    }
    if (tableView == SearchDetailsTable || tableView == altenateRouteTable)
    {
        StageDetailsCell * cell = (StageDetailsCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        CATransition *transition = [CATransition animation];
        transition.delegate = self;
        transition.duration = 0.6;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.subtype = kCATransitionFromTop;
        
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isMemberOfClass:[RouteViewController class]]) {
                transition.type = @"cameraIris";
                RouteViewController *routeViewController = (RouteViewController *)vc;
                [routeViewController updateDetailsTableForRoute:[cell.route.text uppercaseString]];
                [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
                [self.navigationController popToViewController:vc animated:NO];
                return;
            }
        }
        RouteViewController *routeViewController = [[mSingleton getViewControllerManagerInstance] getRouteViewController];
        transition.type = @"cameraIris";
        [routeViewController updateDetailsTableForRoute:[cell.route.text uppercaseString]];
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:routeViewController animated:NO];
        
    }
}

#pragma mark-
#pragma mark TextFiled Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [alternateRouteView setHidden:TRUE];
    if (textField == sourceSearchBox)
    {
        [sourceSearchBox setFont:[mSingleton getFontOfSize:14]];
        [sourceSearchBox setTextColor:[UIColor blackColor]];
        
        [noMatchLabel setHidden:TRUE];
        [noMatchLabel2 setHidden:TRUE];
        [searchPlacesButton setHidden:TRUE];
        [self.SearchDetailsTable setHidden:TRUE];
        [self.searchAssistTable setFrame:CGRectMake(10, 92, 300, 220)];
        [self filterContentForSearchText:sourceSearchBox.text scope:nil];
    }
    else if (textField == destinationSearchBox)
    {
        if(! destinationSearchBox.hidden)
        {
            [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultShowDestination] waitUntilDone:NO];
            destinationSearchBox.alpha = 1.0;
        }
        
        if (destinationSearchBox.text.length == 0)
            destinationSearchBox.text = @"";
        
        [destinationSearchBox setFont:[mSingleton getFontOfSize:14]];
        [destinationSearchBox setTextColor:[UIColor blackColor]];
        [self.searchAssistTable setFrame:CGRectMake(10, 172, 300, 220)];
        
        //        [noMatchLabel setHidden:TRUE];
        //        [noMatchLabel2 setHidden:TRUE];
        //        [searchPlacesButton setHidden:TRUE];
        //        [self.SearchDetailsTable setHidden:TRUE];
        [self filterContentForSearchText:destinationSearchBox.text scope:nil];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == destinationSearchBox)
    {
        if (destinationSearchBox.text.length == 0)
        {
            destinationSearchBox.text = @"";
            destinationSearchBox.alpha = 0.6;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == sourceSearchBox || textField == destinationSearchBox)
        [self filterContentForSearchText:[textField.text stringByReplacingCharactersInRange:range withString:string] scope:nil];
    
    if (textField == sourceSearchBox && [[textField.text stringByReplacingCharactersInRange:range withString:string] length] == 0)
    {
        [self performSelectorOnMainThread:@selector(ShowResultView:) withObject:[NSNumber numberWithInt:ShowResultHideDestination] waitUntilDone:NO];
        destinationSearchBox.text = @"";
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (! searchAssistTable.hidden)
        [self setTableHidden:TRUE];
    
    if (([self.filteredListContent count] > 0) && (![self.filteredListContent containsObject:[textField.text uppercaseString]]))
    {
        BOOL didgetPrefix = FALSE;
        for (NSString *string in self.filteredListContent)
        {
            if ([string hasPrefix:textField.text])
            {
                textField.text = string;
                didgetPrefix = TRUE;
                break;
            }
        }
        if (!didgetPrefix)
            textField.text = [self.filteredListContent objectAtIndex:0];
    }
    
    if (textField == sourceSearchBox)
    {
        NSDictionary *eventLocation = [NSDictionary dictionaryWithObjectsAndKeys:sourceSearchBox.text,@"source",destinationSearchBox.text,@"destination", nil];
        [self performSelectorInBackground:@selector(doStageDetailsUpdate:) withObject:eventLocation];
    }
    else if (textField == destinationSearchBox)
    {
        NSDictionary *eventLocation = [NSDictionary dictionaryWithObjectsAndKeys:sourceSearchBox.text,@"source",destinationSearchBox.text,@"destination", nil];
        [self performSelectorInBackground:@selector(doStageDetailsUpdate:) withObject:eventLocation];
    }
    return YES;
}


@end
