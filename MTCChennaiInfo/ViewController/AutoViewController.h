//
//  AutoViewController.h
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 30-3-14.
//  Copyright (c) 2014 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapPoint.h"

@interface AutoViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentCentre;
    BOOL firstLaunch;
    int currenDist;

    IBOutlet UITextField *sourceField;
    IBOutlet UITextField *destinationField;

    IBOutlet UIButton *currentLocationBtn;
    IBOutlet UIButton *swapLocationsBtn;
    IBOutlet UIButton *searchBtn;
    IBOutlet UIButton *fareBtn;

    IBOutlet UISwitch *nightServiceSwth;
    
    IBOutlet UITableView *searchAssistTable;
    
    IBOutlet UIView *resultView;
    IBOutlet UILabel *fareLabel;
    IBOutlet UILabel *distanceLabel;

    IBOutlet UIButton *infoButtonBtn;
    IBOutlet UIView *infoView1;
    IBOutlet UIView *infoView2;

    
    NSMutableArray *stageDetails;
    NSMutableArray *filteredListContent;
    
    MapPoint *sourcePoint;
    MapPoint *destinationPoint;

    NSArray* routes;
    
    IBOutlet UIActivityIndicatorView *loadingView;
    IBOutlet UILabel *loadingText;

}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property(nonatomic) BOOL firstLaunch;
@property(nonatomic) int currenDist;

@property(nonatomic) UITextField *sourceField;
@property(nonatomic) UITextField *destinationField;

@property(nonatomic) UIButton *currentLocationBtn;
@property(nonatomic) UIButton *swapLocationsBtn;
@property(nonatomic) UIButton *searchBtn;
@property(nonatomic) UIButton *fareBtn;

@property(nonatomic) UISwitch *nightServiceSwth;

@property(nonatomic) UITableView *searchAssistTable;

@property(nonatomic) UIView *resultView;
@property(nonatomic) UILabel *fareLabel;
@property(nonatomic) UILabel *distanceLabel;

@property(nonatomic) UIButton *infoButtonBtn;
@property(nonatomic) UIView *infoView1;
@property(nonatomic) UIView *infoView2;

@property(nonatomic) NSMutableArray *stageDetails;
@property(nonatomic) NSMutableArray *filteredListContent;

@property(nonatomic) MapPoint *sourcePoint;
@property(nonatomic) MapPoint *destinationPoint;

@property(nonatomic) UIActivityIndicatorView *loadingView;
@property(nonatomic) UILabel *loadingText;

-(IBAction)onBack:(id)sender;

-(IBAction)onCurrentLocation:(id)sender;
-(IBAction)onSwapLocations:(id)sender;
-(IBAction)onSearch:(id)sender;
-(IBAction)onCalculateFare:(id)sender;
-(IBAction)onInfo:(id)sender;
-(IBAction)onNightSwitch:(id)sender;

-(void)plotStage:(MapPoint *)sourceMapPoint;
-(void)plotSorce:(MapPoint *)sourceMapPoint andDestination:(MapPoint *)destinationMapPoint;
-(void)updateLocationDetails;
@end
