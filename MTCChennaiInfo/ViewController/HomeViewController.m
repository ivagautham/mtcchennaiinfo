//
//  HomeViewController.m
//  MTCChennaiInfo
//
//  Created by Gautham on 20/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "HomeViewController.h"
#import "StageViewController.h"
#import "RouteViewController.h"
#import "SearchViewController.h"
#import "MapViewController.h"
#import "AboutViewController.h"
#import "Singleton.h"
#import "HelpView.h"
#import "Flurry.h"
#import "ExpandableNavigation.h"
#import "MRTSViewController.h"
#import "AutoViewController.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])
@interface HomeViewController ()
//FOR HELPVIEW
@property (nonatomic, strong) NSArray *pageGestureImages;
@property (nonatomic, strong) NSArray *pageActionImages;
@property (nonatomic, strong) NSArray *pageTitle;
@property (nonatomic, strong) NSArray *pageDescription;
@property (nonatomic, strong) NSMutableArray *pageViews;

- (void)loadVisiblePages;
- (void)loadPage:(NSInteger)page;
- (void)purgePage:(NSInteger)page;
@end

@implementation HomeViewController
@synthesize search, route, service, map, stage;
@synthesize searchlabel, routelabel, servicelabel, maplabel, stagelabel;
@synthesize ratelabel,feedbacklabel, sharelabel, aboutlabel,helplabel;
@synthesize heavyRefresh;
@synthesize restoreDefaults;
@synthesize feedback;
@synthesize help;
@synthesize about;
@synthesize rate;
@synthesize share;
@synthesize main;
@synthesize autoButton;
@synthesize navigation;
@synthesize scrollView = _scrollView;
@synthesize pageControl = _pageControl;
@synthesize helpView = _helpView;

@synthesize pageViews = _pageViews;
@synthesize pageGestureImages = _pageGestureImages;
@synthesize pageActionImages = _pageActionImages;
@synthesize pageTitle = _pageTitle;
@synthesize pageDescription = _pageDescription;
@synthesize closeHelp = _closeHelp;

@synthesize loadingView;
@synthesize loadingView2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.layer.cornerRadius = 10;
        UISwipeGestureRecognizer *swipeLeftforRoute = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onRoute:)];
        swipeLeftforRoute.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer: swipeLeftforRoute];
        
        UISwipeGestureRecognizer *swipeRightforStage = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onStage:)];
        swipeRightforStage.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer: swipeRightforStage];
        
        UISwipeGestureRecognizer *swipeDownforSearch = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSearch:)];
        swipeDownforSearch.direction = UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer: swipeDownforSearch];
        
        UISwipeGestureRecognizer *swipeUpforMap = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onAuto:)];
        swipeUpforMap.direction = UISwipeGestureRecognizerDirectionUp;
        [self.view addGestureRecognizer: swipeUpforMap];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        [self.scrollView addGestureRecognizer:singleTap];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setUserInteractionEnabled:FALSE];
   
    [self.view addSubview:loadingView2];
    [loadingView2 setHidden:FALSE];
    [loadingView2 setAlpha:1.0];
    [loadingView2 setFrame:self.view.frame];
    [self.view bringSubviewToFront:loadingView2];
    
    [self.view addSubview:loadingView];
    [loadingView setHidden:FALSE];
    [loadingView setAlpha:1.0];
    [loadingView setFrame:self.view.frame];
    [self.view bringSubviewToFront:loadingView];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_ROUTE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_ROUTE_INFO object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_ROUTE_DUMP object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_STAGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_STAGE_INFO object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_STAGE_DUMP object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_STATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_STATION_DETAILS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SUCCESS_FETCH_STATION_SCHEDULE object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SHOW_LOADING_VIEW_1 object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_SHOW_LOADING_VIEW_2 object:nil];

    [self setupExpandableItems];
    [mSingleton setupColors];
    //[self.view setBackgroundColor:mSingleton.bgColor];

    [mSingleton loadMTCDataFromResource];
    [mSingleton performSelectorInBackground:@selector(loadMRTSDataFromResource) withObject:nil];
    
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_SHOW_QOS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_HIDE_QOS object:nil];
    // Do any additional setup after loading the view from its nib.
    */
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if( self.navigation.expanded ) {
        [self.navigation collapse];
        [self setlabelHidden:FALSE];
    }
    
//    [self.main.layer removeAllAnimations];
//    
//    
//    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
//    theAnimation.duration = 2.0;
//    theAnimation.repeatCount = HUGE_VALF;
//    theAnimation.autoreverses = YES;
//    theAnimation.fromValue = [NSNumber numberWithFloat:1.0];
//    theAnimation.toValue = [NSNumber numberWithFloat:0.0];
//    [self.main.layer addAnimation:theAnimation forKey:@"animateOpacity"];
    
}

-(void)setlabelHidden:(BOOL)flag
{
    [searchlabel setHidden:flag];
    [routelabel setHidden:flag];
    [stagelabel setHidden:flag];
    [maplabel setHidden:flag];
    [servicelabel setHidden:flag];
    
    if (flag)
        [self.main setImage:[UIImage imageNamed:@"ico_settings_2.png"] forState:UIControlStateNormal];
    else
        [self.main setImage:[UIImage imageNamed:@"ico_settings.png"] forState:UIControlStateNormal];
    
    [ratelabel setHidden:!flag];
    [helplabel setHidden:!flag];
    [aboutlabel setHidden:!flag];
    [sharelabel setHidden:!flag];
    [feedbacklabel setHidden:!flag];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) localNotificationhandler:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_ROUTE])
    {
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_ROUTE_INFO])
    {
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_ROUTE_DUMP])
    {
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_STAGE])
    {
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_STAGE_INFO])
    {
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_STAGE_DUMP])
    {
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_STATION])
    {
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_STATION_DETAILS])
    {
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SUCCESS_FETCH_STATION_SCHEDULE])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SHOW_LOADING_VIEW_1 object:nil];
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SHOW_LOADING_VIEW_1])
    {
        [UIView animateWithDuration:2.0 animations:^{
            [loadingView setAlpha:0.0];
            [loadingView2 setAlpha:1.0];
        }
                         completion:^(BOOL finished){
                             [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SHOW_LOADING_VIEW_2 object:nil];
                         }];
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_SHOW_LOADING_VIEW_2])
    {
        [UIView animateWithDuration:2.0 animations:^{
            [loadingView2 setAlpha:0.0];
        }
                         completion:^(BOOL finished){
                             [loadingView setHidden:TRUE];
                             [loadingView removeFromSuperview];
                             [loadingView2 setHidden:TRUE];
                             [loadingView2 removeFromSuperview];
                             [self performSelectorOnMainThread:@selector(doStartUpActivity) withObject:nil waitUntilDone:NO];
                         }];
    }

}

-(void)doStartUpActivity
{
    [self.view setUserInteractionEnabled:TRUE];
    self.helpView.alpha = 0.0;
    BOOL helpVal = [[[NSUserDefaults standardUserDefaults] objectForKey:@"didShowHelp"] boolValue];
    if (!helpVal)
    {
        NSDictionary *articleParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         [[UIDevice currentDevice] name], @"Device_Name",
         [[UIDevice currentDevice] systemVersion], @"Device_OS",
         [[[UIDevice currentDevice] identifierForVendor] UUIDString], @"Device_ID",
         nil];
        
        [Flurry logEvent:@"App_Installed" withParameters:articleParams];
        //[self performSelector:@selector(showHelp) withObject:nil afterDelay:0.2];
        [self showHelp];
    }
    else
    {
        NSDictionary *articleParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         [[UIDevice currentDevice] name], @"Device_Name",
         [[UIDevice currentDevice] systemVersion], @"Device_OS",
         [[[UIDevice currentDevice] identifierForVendor] UUIDString], @"Device_ID",
         nil];
        
        [Flurry logEvent:@"App_Launched" withParameters:articleParams];
    }
    [[mSingleton getViewControllerManagerInstance] getMapViewController];
}

-(IBAction)onCloseHelp:(id)sender
{
    [UIView animateWithDuration:1.0 animations:^{
        self.helpView.alpha = 0.0;
    }
                     completion:^(BOOL finished){
                         [self.helpView setHidden:TRUE];
                         [self.helpView removeFromSuperview];
                     }];
}

-(void)showHelp
{
    [self.view addSubview:self.helpView];
    
    [self.helpView setHidden:FALSE];
    self.helpView.alpha = 0.0;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.helpView.alpha = 1.0;
    }
                     completion:nil];
    
    // Set up the image we want to scroll & zoom and add it to the scroll view
    self.pageGestureImages = [NSArray arrayWithObjects:
                              [UIImage imageNamed:@"Swipe_Left.png"],
                              [UIImage imageNamed:@"Swipe_Right.png"],
                              [UIImage imageNamed:@"Swipe_Up.png"],
                              [UIImage imageNamed:@"Swipe_Down.png"],
                              [UIImage imageNamed:@"Pinch.png"],
                              nil];
    
    self.pageActionImages = [NSArray arrayWithObjects:
                             [UIImage imageNamed:@"ico_route"],
                             [UIImage imageNamed:@"ico_stage"],
                             [UIImage imageNamed:@"ico_search"],
                             [UIImage imageNamed:@"ico_auto.png"],
                             [UIImage imageNamed:@"ico_train_service"],
                             nil];
    
    self.pageTitle = [NSArray arrayWithObjects:@"Bus No",@"Bus Stop",@"Search",@"Auto",@"Train",nil];
    
    self.pageDescription = [NSArray arrayWithObjects:@"SWIPE LEFT from home screen or tap on the 'Bus No' button to search for various buses, its services and its route path",@"SWIPE RIGHT from home screen or tap on the 'Bus Stop' button to explore all the bus stages and get bus details from different stages",@"SWIPE UP from any screen or tap on the SEARCH button to get Namma Chennai City search option where any place, bus and stage of chennai can be found",@"SWIPE DOWN from any screen or tap on the AUTO button to get auto rickshaw fares and directions",@"The MRTS view can be accessed by tapping the 'Train' button where you can get all MRTS/local train details grouped by its type",nil];
    
    NSInteger pageCount = self.pageGestureImages.count + 1;
    
    // Set up the page control
    self.pageControl.currentPage = 2;
    self.pageControl.numberOfPages = pageCount;
    
    // Set up the array to hold the views for each page
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * (self.pageGestureImages.count + 1), pagesScrollViewSize.height);
    
    self.scrollView.layer.cornerRadius = 5;
    self.scrollView.layer.borderWidth = 0.1;
    
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
    [[NSUserDefaults standardUserDefaults] setObject:@"TRUE" forKey:@"didShowHelp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages we want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    for (NSInteger i=lastPage+1; i<self.pageGestureImages.count + 1; i++) {
        [self purgePage:i];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageGestureImages.count + 1) {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    HelpView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageGestureImages.count + 1) {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Load an individual page, first seeing if we've already loaded it
    HelpView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        pageView = [HelpView helpview];
        if (page != 0)
        {
            [pageView.gestureImage setImage:[self.pageGestureImages objectAtIndex:page - 1]];
            [pageView.actionImage setImage:[self.pageActionImages objectAtIndex:page - 1]];
            [pageView.titleLabel setText:[self.pageTitle objectAtIndex:page - 1]];
            [pageView.detailView setText:[self.pageDescription objectAtIndex:page - 1]];
        }
        else
        {
            [pageView.gestureImage setHidden:TRUE];
            [pageView.actionImage setHidden:TRUE];
            [pageView.detailView setHidden:TRUE];
            
            [pageView.titleLabel setText:@"Welcome To Namma Chennai"];
            UILabel *detaillabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 280, 200, 64)] ;
            [detaillabel setTextColor:[UIColor whiteColor]];
            [detaillabel setNumberOfLines:20];
            [detaillabel setFont:[mSingleton getBoldFontOfSize:10]];
            [detaillabel setTextAlignment:NSTextAlignmentCenter];
            [detaillabel setText:@"This app provide an easily navigable/searchable options for using MTC Buses, MRTS & Auto rickshaws in Chennai"];
            [pageView addSubview:detaillabel];
            
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_Help1"]];
            [imgView setFrame:CGRectMake(13, 60, 215, 185.5)];
            [pageView addSubview:imgView];
        }
        
        pageView.frame = frame;
        [self.scrollView addSubview:pageView];
        
        
        [self.pageViews replaceObjectAtIndex:page withObject:pageView];
    }
}

- (void)singleTapGestureCaptured:(id)gesture
{
    CGPoint point = self.scrollView.contentOffset;
    
    if (point.x + 240 < self.scrollView.contentSize.width)
        point.x = point.x + 240;
    
    [self.scrollView setContentOffset:point animated:YES];
    [self loadVisiblePages];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Load the pages which are now on screen
    [self loadVisiblePages];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onStage:(id)sender
{
    if (self.helpView.alpha == 1.0)
        return;
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = @"cube";
    transition.subtype = kCATransitionFromLeft;
    StageViewController *stageViewController = [[mSingleton getViewControllerManagerInstance] getStageViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:stageViewController animated:NO];
}

-(IBAction)onRoute:(id)sender
{
    if (self.helpView.alpha == 1.0)
        return;
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromRight;
    RouteViewController *routeViewController = [[mSingleton getViewControllerManagerInstance] getRouteViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:routeViewController animated:NO];
}

-(IBAction)onSearch:(id)sender
{
    if (self.helpView.alpha == 1.0)
        return;
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromBottom;
    SearchViewController *searchViewController = [[mSingleton getViewControllerManagerInstance] getSearchViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchViewController animated:NO];
}

-(IBAction)onMap:(id)sender
{
    if (self.helpView.alpha == 1.0)
        return;
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    //[mapViewController performSelectorInBackground:@selector(plotAllStages) withObject:nil];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
}

-(IBAction)onMRTS:(id)sender
{
    if (self.helpView.alpha == 1.0)
        return;
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cameraIris";
    transition.subtype = kCATransitionFromTop;
    MRTSViewController *MRTSViewController = [[mSingleton getViewControllerManagerInstance] getMRTSViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:MRTSViewController animated:NO];
}

-(IBAction)onAuto:(id)sender
{
    if (self.helpView.alpha == 1.0)
        return;
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromTop;
    
    AutoViewController *autoViewController = [[mSingleton getViewControllerManagerInstance] getAutoViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:autoViewController animated:NO];
}

-(void)setupExpandableItems
{
    NSArray* buttons = [NSArray arrayWithObjects:feedback, about, help, rate, share, nil];
    NSArray* basicbuttons = [NSArray arrayWithObjects:search, stage, map, service, route, nil];

    self.navigation = [[ExpandableNavigation alloc] initWithMenuItems:buttons basicItems:basicbuttons mainButton:self.main radius:110];
}

- (IBAction) touchMenuItem:(id)sender {
    
    // if the menu is expanded, then collapse it when an menu item is touched.
    if( self.navigation.expanded ) {
        [self.navigation collapse];
    }
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    if (sender == about) {
        AboutViewController *aboutViewController = [[mSingleton getViewControllerManagerInstance] getAboutViewController];
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:aboutViewController animated:NO];
    }
    else if(sender == share) {
        NSArray *activityItems = [NSArray arrayWithObjects:@"Check out the latest navigation app for iOS devices \"Namma CHENNAI\" from the app store https://itunes.apple.com/us/app/namma-mtc/id718101880?ls=1&mt=8. App helps in easy navigation between the city", nil];
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        [self presentViewController:activityController animated:YES completion:nil];
    }
    else if(sender == feedback) {
        if([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
            
            mailComposeViewController.mailComposeDelegate = self;
            [mailComposeViewController setToRecipients:[NSArray arrayWithObjects:@"nammamtc@gmail.com",nil]];
            [mailComposeViewController setSubject:@"Namma CHENNAI feedback"];
            NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
            [mailComposeViewController setMessageBody:[NSString stringWithFormat:@"Feedback for Namma CHENNAI version %@",versionString] isHTML:NO];
            
            [self presentViewController:mailComposeViewController animated:YES completion:nil];
        }
        else
        {
            UIAlertView *configureMailAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please configure Mail account in Settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [configureMailAlert show];
        }
        
    }
    else if(sender == rate) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/namma-mtc/id718101880?ls=1&mt=8"]];
    }
    else if(sender == help) {
        [self showHelp];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
        default:
        {
            UIAlertView *mailsendFailed = [[UIAlertView alloc] initWithTitle:@"" message:@"Unable to send mail.\nTry again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [mailsendFailed show];
            break;
        }
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
}
@end
