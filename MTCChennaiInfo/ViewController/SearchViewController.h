//
//  SearchViewController.h
//  MTCChennaiInfo
//
//  Created by Gautham on 20/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapPoint.h"

#define kGOOGLE_API_KEY @"AIzaSyB55nQx8JbQeULuGEzuIgabd44hEEt9N6c"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

@interface SearchViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate,NSXMLParserDelegate>
{
    IBOutlet UITextField *searchBox;
    IBOutlet UITableView *searchResultTable;
    IBOutlet UILabel *noMatchLabel;
    IBOutlet UISegmentedControl *filter;
    IBOutlet UIActivityIndicatorView *loading;
    
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentCentre;
    int currenDist;
    BOOL firstLaunch;
    
    NSMutableArray *allStages;
    NSMutableArray *allRoutes;
    NSMutableArray *routeInfo;
    NSMutableArray *stageDetails;
    
    NSMutableArray *allMRTSStations;

    IBOutlet UIButton *backButton;
}

@property(nonatomic, strong) NSMutableArray *allStages;
@property(nonatomic, strong) NSMutableArray *allRoutes;
@property(nonatomic, strong) NSMutableArray *routeInfo;
@property(nonatomic, strong) NSMutableArray *stageDetails;
@property(nonatomic, strong) NSMutableArray *allMRTSStations;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) UITextField *searchBox;
@property (strong, nonatomic) UITableView *searchResultTable;
@property (strong, nonatomic) UILabel *noMatchLabel;
@property (strong, nonatomic) UISegmentedControl *filter;
@property (strong, nonatomic) UIActivityIndicatorView *loading;

@property (strong, nonatomic) NSMutableArray *searchResult;

@property(nonatomic, strong) UIButton *backButton;

-(IBAction)onHome:(id)sender;

-(IBAction)filterdidChange:(id)sender;

-(void)searchPlace:(NSString *)place;

@end
