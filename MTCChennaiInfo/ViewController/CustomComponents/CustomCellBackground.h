//
//  CustomCellBackground.h
//  CoolTable
//
//  Created by Brian Moakley on 2/17/13.
//  Copyright (c) 2013 Razeware. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	ColorTypeLightGray = 0,
	ColorTypeGray,
	ColorTypeDarkGray
} ColorType;

@interface CustomCellBackground : UIView

@property (nonatomic, assign) ColorType colortype;
@property (nonatomic, assign) BOOL lastCell;
@property (nonatomic, assign) BOOL selected;

@end
