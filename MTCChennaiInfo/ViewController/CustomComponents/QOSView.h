//
//  QOSView.h
//  MTCChennaiInfo
//
//  Created by Gautham on 29/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface QOSView : UIView<CLLocationManagerDelegate>
{
    IBOutlet UIView *noIntenetView;
    IBOutlet UIView *noLocationServiceView;
    IBOutlet UIActivityIndicatorView *loadingIndicator;
}

@property(nonatomic, strong) UIView *noIntenetView;
@property(nonatomic, strong) UIView *noLocationServiceView;
@property(nonatomic, strong) UIActivityIndicatorView *loadingIndicator;

+ (id)qosView;

@end
