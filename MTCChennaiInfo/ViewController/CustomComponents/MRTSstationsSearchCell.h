//
//  MRTSstationsSearchCell.h
//  MTCChennaiInfo
//
//  Created by Gautham on 11/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRTSstationsSearchCell : UITableViewCell
{
    IBOutlet UILabel *stationName;
    IBOutlet UILabel *stationCode;
    NSNumber *stationID;
}

@property(nonatomic, strong) UILabel *stationName;
@property(nonatomic, strong) UILabel *stationCode;
@property(nonatomic, strong) NSNumber *stationID;

@end
