//
//  RoutePathCell.h
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 27-8-13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoutePathCell : UITableViewCell
{
    IBOutlet UILabel *pathLabel;
    IBOutlet UIButton *mapitButton;
    IBOutlet UIButton *autoButton;

}
@property(nonatomic, strong) UILabel *pathLabel;
@property(nonatomic, strong) UIButton *mapitButton;
@property(nonatomic, strong) UIButton *autoButton;

@end
