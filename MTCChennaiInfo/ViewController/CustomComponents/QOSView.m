//
//  QOSView.m
//  MTCChennaiInfo
//
//  Created by Gautham on 29/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "QOSView.h"
#import <QuartzCore/QuartzCore.h>
#import "Singleton.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@implementation QOSView
@synthesize loadingIndicator;
@synthesize noIntenetView;
@synthesize noLocationServiceView;

BOOL internet, location;
CGRect frame1, frame2;
CLLocationManager *forNotification;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

+ (id)qosView
{
    QOSView *customView = [[[NSBundle mainBundle] loadNibNamed:@"QOSView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[QOSView class]])
        return customView;
    else
        return nil;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    internet = [mSingleton isConnectedToInternet];
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
        location = FALSE;
    else
        location = TRUE;
    
    
    frame1 = noLocationServiceView.frame;
    frame2 = noIntenetView.frame;
    
    //noIntenetView.layer.cornerRadius = 5;
    noIntenetView.layer.borderWidth = 0.1;
    noIntenetView.layer.masksToBounds = NO;
    noIntenetView.layer.shadowOffset = CGSizeMake(-5, 2);
    noIntenetView.layer.shadowRadius = 5;
    noIntenetView.layer.shadowOpacity = 0.5;
    
    //noLocationServiceView.layer.cornerRadius = 5;
    noLocationServiceView.layer.borderWidth = 0.1;
    noLocationServiceView.layer.borderWidth = 0.1;
    noLocationServiceView.layer.masksToBounds = NO;
    noLocationServiceView.layer.shadowOffset = CGSizeMake(-5, 2);
    noLocationServiceView.layer.shadowRadius = 5;
    noLocationServiceView.layer.shadowOpacity = 0.5;
    
    [loadingIndicator stopAnimating];
    forNotification = [[CLLocationManager alloc] init];
    forNotification.delegate = self;
    
    [self doShowHideOperations];
}

-(void)doShowHideOperations
{
    if(!location && !internet)
    {
        noIntenetView.hidden = FALSE;
        noLocationServiceView.hidden = FALSE;
       // noLocationServiceView .frame = frame1;
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_SHOW_QOS object: nil];
    }
    else if(!location)
    {
        noLocationServiceView.hidden = FALSE;
        //noLocationServiceView .frame = frame2;
        noIntenetView.hidden = TRUE;
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_SHOW_QOS object: nil];
    }
    else if(!internet)
    {
        noIntenetView.hidden = FALSE;
       // noIntenetView .frame = frame2;
        noLocationServiceView.hidden = TRUE;
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_SHOW_QOS object: nil];
    }
    else if(location && internet)
    {
        noIntenetView.hidden = TRUE;
        noLocationServiceView.hidden = TRUE;
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_HIDE_QOS object: nil];
    }
}

- (void)reachabilityChanged:(NSNotification * )notification
{
    if ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == NotReachable)
    {
        internet = NO;
    }
    if (([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == ReachableViaWiFi) || ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == ReachableViaWWAN))
    {
        internet = YES;
    }
    if (([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableViaWiFi) || ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableViaWWAN))
    {
        internet = YES;
    }

    [self doShowHideOperations];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
        location = FALSE;
    else
        location = TRUE;
    [self doShowHideOperations];
}

- (void)handleTouchAtLocation:(CGPoint)touchLocation
{
    if(CGRectContainsPoint(noIntenetView.frame,touchLocation) && !internet)
    {
        UIAlertView *noInternetAlert = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Application cannot load few data since your iPhone is not connected to the Internet. \nYou can turn Internet on or off at Settings > Cellular" delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [noInternetAlert show];
        [self reachabilityChanged:nil];
    }
    else if(CGRectContainsPoint(noLocationServiceView.frame,touchLocation) && !location)
    {
        UIAlertView *noLocationAlert = [[UIAlertView alloc] initWithTitle:@"No Location Service" message:@"Application cannot determine your approximate location. \nYou can turn Location Services on or off at Settings > Privacy > Location Services" delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [noLocationAlert show];
    }
    [self doShowHideOperations];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

@end
