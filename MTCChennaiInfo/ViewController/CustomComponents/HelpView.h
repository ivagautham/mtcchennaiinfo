//
//  HelpView.h
//  MTCChennaiInfo
//
//  Created by Gautham on 24/09/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpView : UIView
{
    IBOutlet UILabel *titleLabel;
    IBOutlet UIImageView *gestureImage;
    IBOutlet UIImageView *actionImage;
    IBOutlet UILabel *detailView;
}

+ (id)helpview;

@property(nonatomic, strong)  UILabel *titleLabel;
@property(nonatomic, strong) UIImageView *gestureImage;
@property(nonatomic, strong) UIImageView *actionImage;
@property(nonatomic, strong) UILabel *detailView;

@end
