//
//  MRTSstationsSearchCell.h
//  MTCChennaiInfo
//
//  Created by Gautham on 11/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRTSroutesCell : UITableViewCell
{
    IBOutlet UILabel *source;
    IBOutlet UILabel *destination;
    NSNumber *direction;
}

@property(nonatomic, strong) UILabel *source;
@property(nonatomic, strong) UILabel *destination;
@property(nonatomic, strong) NSNumber *direction;

@end
