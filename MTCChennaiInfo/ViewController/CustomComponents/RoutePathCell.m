//
//  RoutePathCell.m
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 27-8-13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "RoutePathCell.h"

@implementation RoutePathCell
@synthesize pathLabel;
@synthesize mapitButton;
@synthesize autoButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
