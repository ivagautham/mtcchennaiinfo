//
//  TrainDetailsSectionView.m
//  MTCChennaiInfo
//
//  Created by Gautham on 04/11/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "TrainDetailsSectionView.h"

@implementation TrainDetailsSectionView
@synthesize backButton,carousel,swapButton,index;
@synthesize destination,source;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (id)trainDetailsSectionView
{
    TrainDetailsSectionView *customView = [[[NSBundle mainBundle] loadNibNamed:@"TrainDetailsSectionView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[TrainDetailsSectionView class]])
        return customView;
    else
        return nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
