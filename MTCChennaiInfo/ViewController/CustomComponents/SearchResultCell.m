//
//  SearchResultCell.m
//  MTCChennaiInfo
//
//  Created by Gautham on 29/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "SearchResultCell.h"

@implementation SearchResultCell
@synthesize searchResultTypeImage;
@synthesize contentLabel;
@synthesize detalilLabel;
@synthesize plotButton;
@synthesize distanceFromCurrentLocation;
@synthesize nearestStage;
@synthesize distanceofStage;
@synthesize distanceofStation;
@synthesize nearestStation;
@synthesize autoButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
