//
//  MapPoint.h
//  GooglePlaces
//
//  Created by van Lint Jason on 6/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

typedef enum {
	MapPointTypePlace = 0,
	MapPointTypeStage,
	MapPointTypeRoute
} MapPointType;

@interface MapPoint : NSObject <MKAnnotation>
{
    NSString *_name;
    NSString *_address;
    CLLocationCoordinate2D _coordinate;
    MapPointType type;
}

@property (copy) NSString *name;
@property (copy) NSString *address;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) MapPointType type;

- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate andtype:(MapPointType)mapPointType;

@end
