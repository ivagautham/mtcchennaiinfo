//
//  RouteDetailsCell.m
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 23-8-13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "RouteDetailsCell.h"

@implementation RouteDetailsCell
@synthesize source;
@synthesize destination;
@synthesize duration;
@synthesize service;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
