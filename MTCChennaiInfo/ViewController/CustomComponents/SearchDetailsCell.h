//
//  SearchDetailsCell.h
//  MTCChennaiInfo
//
//  Created by Gautham on 27/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchDetailsCell : UITableViewCell
{
    IBOutlet UIImageView *searchResultTypeImage;
    IBOutlet UILabel *contentLabel;
    IBOutlet UILabel *detalilLabel;
    IBOutlet UIButton *plotButton;
    IBOutlet UIButton *autoButton;
}

@property(nonatomic, strong) UIImageView *searchResultTypeImage;
@property(nonatomic, strong) UILabel *contentLabel;
@property(nonatomic, strong) UILabel *detalilLabel;
@property(nonatomic, strong) UIButton *plotButton;
@property(nonatomic, strong) UIButton *autoButton;

@end
