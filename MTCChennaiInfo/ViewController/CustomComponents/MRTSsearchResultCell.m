//
//  MRTSstationsSearchCell.m
//  MTCChennaiInfo
//
//  Created by Gautham on 11/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "MRTSsearchResultCell.h"

@implementation MRTSsearchResultCell
@synthesize source, routeID, direction, destination;
@synthesize stop,carousel;
@synthesize servicesLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
