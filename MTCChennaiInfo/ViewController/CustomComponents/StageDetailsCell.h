//
//  StageDetailsCell.h
//  MTCChennaiInfo
//
//  Created by Gautham on 02/09/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StageDetailsCell : UITableViewCell
{
    IBOutlet UILabel *route;
    IBOutlet UILabel *source;
    IBOutlet UILabel *destination;
    IBOutlet UILabel *service;
}

@property(nonatomic, strong) UILabel *route;
@property(nonatomic, strong) UILabel *source;
@property(nonatomic, strong) UILabel *destination;
@property(nonatomic, strong) UILabel *service;

@end
