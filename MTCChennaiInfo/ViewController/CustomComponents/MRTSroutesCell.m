//
//  MRTSstationsSearchCell.m
//  MTCChennaiInfo
//
//  Created by Gautham on 11/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "MRTSroutesCell.h"

@implementation MRTSroutesCell
@synthesize source, direction, destination;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
