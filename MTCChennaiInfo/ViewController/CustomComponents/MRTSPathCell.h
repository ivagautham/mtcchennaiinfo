//
//  RoutePathCell.h
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 27-8-13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface MRTSPathCell : UITableViewCell
{
    IBOutlet UILabel *pathLabel;
    IBOutlet UIButton *mapitButton;
    IBOutlet iCarousel *carousel;
    IBOutlet UILabel *stopLabel;
    IBOutlet UILabel *servicesLabel;

}

@property(nonatomic, strong) UILabel *pathLabel;
@property(nonatomic, strong) UIButton *mapitButton;
@property(nonatomic, strong) iCarousel *carousel;
@property(nonatomic, strong) UILabel *stopLabel;
@property(nonatomic, strong) UILabel *servicesLabel;

@end
