//
//  SearchResultCell.h
//  MTCChennaiInfo
//
//  Created by Gautham on 29/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultCell : UITableViewCell
{
    IBOutlet UIImageView *searchResultTypeImage;
    IBOutlet UILabel *contentLabel;
    IBOutlet UILabel *detalilLabel;
    IBOutlet UIButton *plotButton;
    
    IBOutlet UILabel *distanceFromCurrentLocation;
    IBOutlet UILabel *distanceofStage;
    IBOutlet UILabel *distanceofStation;
    IBOutlet UIButton *nearestStage;
    IBOutlet UIButton *nearestStation;

    IBOutlet UIButton *autoButton;
}

@property(nonatomic, strong) UIImageView *searchResultTypeImage;
@property(nonatomic, strong) UILabel *contentLabel;
@property(nonatomic, strong) UILabel *detalilLabel;
@property(nonatomic, strong) UIButton *plotButton;

@property(nonatomic, strong) UILabel *distanceFromCurrentLocation;
@property(nonatomic, strong) UIButton *nearestStage;
@property(nonatomic, strong) UILabel *distanceofStage;
@property(nonatomic, strong) UILabel *distanceofStation;
@property(nonatomic, strong) UIButton *nearestStation;
@property(nonatomic, strong) UIButton *autoButton;

@end
