//
//  TrainDetailsSectionView.h
//  MTCChennaiInfo
//
//  Created by Gautham on 04/11/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface TrainDetailsSectionView : UIView
{
    IBOutlet UIButton *backButton;
    IBOutlet iCarousel *carousel;
    IBOutlet UIButton *swapButton;
    int index;
    IBOutlet UILabel *source;
    IBOutlet UILabel *destination;

}

@property(nonatomic, strong) UIButton *backButton;
@property(nonatomic, strong) iCarousel *carousel;
@property(nonatomic, strong) UIButton *swapButton;
@property(nonatomic) int index;
@property(nonatomic, strong) UILabel *source;
@property(nonatomic, strong) UILabel *destination;

+ (id)trainDetailsSectionView;

@end
