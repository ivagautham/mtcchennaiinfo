//
//  MRTSstationsSearchCell.h
//  MTCChennaiInfo
//
//  Created by Gautham on 11/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface MRTSsearchResultCell : UITableViewCell
{
    IBOutlet UILabel *source;
    IBOutlet UILabel *destination;
    IBOutlet UILabel *stop;
    IBOutlet iCarousel *carousel;
    IBOutlet UILabel *servicesLabel;

    NSNumber *routeID;
    NSNumber *direction;

}

@property(nonatomic, strong) UILabel *source;
@property(nonatomic, strong) UILabel *destination;
@property(nonatomic, strong) NSNumber *routeID;
@property(nonatomic, strong) NSNumber *direction;
@property(nonatomic, strong) UILabel *stop;
@property(nonatomic, strong) iCarousel *carousel;
@property(nonatomic, strong) UILabel *servicesLabel;

@end
