//
//  HelpView.m
//  MTCChennaiInfo
//
//  Created by Gautham on 24/09/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "HelpView.h"

@implementation HelpView
@synthesize titleLabel;
@synthesize gestureImage;
@synthesize actionImage;
@synthesize detailView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.layer.cornerRadius = 5;
        self.layer.borderWidth = 0.1;
        
        if (!titleLabel)
            titleLabel = [[UILabel alloc] init];
        if (!gestureImage)
            gestureImage = [[UIImageView alloc] init];
        if (!actionImage)
            actionImage = [[UIImageView alloc] init];
        if (!detailView)
            detailView = [[UILabel alloc] init];
    }
    return self;
}

+ (id)helpview
{
    HelpView *customView = [[[NSBundle mainBundle] loadNibNamed:@"HelpView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[HelpView class]])
        return customView;
    else
        return nil;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
