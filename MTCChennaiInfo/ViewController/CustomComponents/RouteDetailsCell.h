//
//  RouteDetailsCell.h
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 23-8-13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteDetailsCell : UITableViewCell
{
    IBOutlet UILabel *source;
    IBOutlet UILabel *destination;
    IBOutlet UILabel *duration;
    IBOutlet UILabel *service;
}

@property(nonatomic, strong) UILabel *source;
@property(nonatomic, strong) UILabel *destination;
@property(nonatomic, strong) UILabel *duration;
@property(nonatomic, strong) UILabel *service;
@end
