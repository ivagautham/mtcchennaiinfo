//
//  MRTSViewController.h
//  MTCChennaiInfo
//
//  Created by Gautham on 09/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "iCarousel.h"

@interface MRTSViewController : UIViewController<CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate, iCarouselDataSource,iCarouselDelegate, UIGestureRecognizerDelegate>
{
    IBOutlet UISegmentedControl *MRTSsegment;
    
    IBOutlet UIView *MRTSroutesView;
    IBOutlet UITableView *MRTSallRoutesTable;
    IBOutlet UITableView *MRTSselectedRouteTable;
    IBOutlet UITableView *MRTStimeDetailTable;
    
    NSMutableArray *allMRTSRoutes;
    
    IBOutlet UIView *MRTSsearchView;
    IBOutlet UITextField *stationSearchbox;
    IBOutlet UITableView *searchAssistTable;
    IBOutlet UIActivityIndicatorView *spinnerSearch;
    IBOutlet UITableView *searchResultTable;
    IBOutlet UILabel *noResult;
    
    NSMutableArray *filteredListContent;
    NSMutableArray *allMRTSStations;
    NSMutableArray *allMRTSStationDetails;
    NSMutableDictionary *MRTSStationDetailsDictionary;
    NSMutableArray *searchResultArray;
    NSMutableDictionary *searchResultDictionary;
        
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentCentre;
    
    IBOutlet UIButton *sourceCurrentLocation;
    IBOutlet UIButton *eye;

}

@property(nonatomic, strong) UISegmentedControl *MRTSsegment;

@property(nonatomic, strong) UIView *MRTSroutesView;
@property(nonatomic, strong) UITableView *MRTSallRoutesTable;
@property(nonatomic, strong) UITableView *MRTSselectedRouteTable;
@property(nonatomic, strong) UITableView *MRTStimeDetailTable;

@property(nonatomic, strong) NSMutableArray *allMRTSRoutes;

@property(nonatomic, strong) UIView *MRTSsearchView;
@property(nonatomic, strong) UITextField *stationSearchbox;
@property(nonatomic, strong) UITableView *searchAssistTable;
@property(nonatomic, strong) UIActivityIndicatorView *spinnerSearch;
@property(nonatomic, strong) UITableView *searchResultTable;
@property(nonatomic, strong) UILabel *noResult;

@property(nonatomic, strong) NSMutableArray *filteredListContent;
@property(nonatomic, strong) NSMutableArray *allMRTSStations;
@property(nonatomic, strong) NSMutableArray *allMRTSStationDetails;
@property(nonatomic, strong) NSMutableDictionary *MRTSStationDetailsDictionary;
@property(nonatomic, strong) NSMutableArray *searchResultArray;
@property(nonatomic, strong) NSMutableDictionary *searchResultDictionary;

@property(nonatomic, strong) UIButton *eye;

-(IBAction)onHome:(id)sender;
-(IBAction)onStage:(id)sender;
-(IBAction)onRoute:(id)sender;
-(IBAction)onSearch:(id)sender;
-(IBAction)onMap:(id)sender;
-(IBAction)onEye:(id)sender;

-(IBAction)onMRTSsegment:(id)sender;
-(IBAction)onNearestStation:(id)sender;

-(void)updateDetailsTableForSource:(NSString *)source;

@end
