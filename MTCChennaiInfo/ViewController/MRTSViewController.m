//
//  MRTSViewController.m
//  MTCChennaiInfo
//
//  Created by Gautham on 09/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "MRTSViewController.h"
#import "Singleton.h"
#import "SearchViewController.h"
#import "StageViewController.h"
#import "RouteViewController.h"
#import "MapViewController.h"
#import "MRTSstationsSearchCell.h"
#import "MRTSMapViewController.h"
#import "MRTSroutesCell.h"
#import "MRTSsearchResultCell.h"
#import "TrainDetailsSectionView.h"
#import "MRTSPathCell.h"
#import "Flurry.h"
#import "AutoViewController.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@interface MRTSViewController ()
{
    BOOL didLoadfromResource;
    NSInteger selectedIndex;
}
-(void)loadStagesFromResource;

@end

@implementation MRTSViewController
@synthesize MRTSsegment;
@synthesize MRTSsearchView,searchAssistTable,stationSearchbox,spinnerSearch,searchResultTable;
@synthesize filteredListContent,allMRTSStations,allMRTSStationDetails, MRTSStationDetailsDictionary,searchResultArray,searchResultDictionary;
@synthesize MRTSroutesView,MRTSallRoutesTable,MRTSselectedRouteTable,MRTStimeDetailTable;
@synthesize allMRTSRoutes;
@synthesize eye,noResult;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        didLoadfromResource = FALSE;
        self.view.layer.cornerRadius = 10;
        UISwipeGestureRecognizer *swipeRightforRoute = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onRoute:)];
        swipeRightforRoute.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer: swipeRightforRoute];
        [swipeRightforRoute setCancelsTouchesInView:NO];
        swipeRightforRoute.delegate = self;
        
        UISwipeGestureRecognizer *swipeLeftforStage = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onStage:)];
        swipeLeftforStage.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer: swipeLeftforStage];
        [swipeLeftforStage setCancelsTouchesInView:NO];
        swipeLeftforStage.delegate = self;

        UISwipeGestureRecognizer *swipeDownforSearch = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSearch:)];
        swipeDownforSearch.direction = UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer: swipeDownforSearch];
        [swipeDownforSearch setCancelsTouchesInView:NO];
        swipeDownforSearch.delegate = self;

        UISwipeGestureRecognizer *swipeUpforMap = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onAutoButton:)];
        swipeUpforMap.direction = UISwipeGestureRecognizerDirectionUp;
        [self.view addGestureRecognizer: swipeUpforMap];
        [swipeUpforMap setCancelsTouchesInView:NO];
        swipeUpforMap.delegate = self;

    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:mSingleton.bgColor];
    if (!didLoadfromResource)
        [self loadStagesFromResource];
    
    [stationSearchbox setDelegate:self];
    [searchAssistTable setDelegate:self];
    [searchAssistTable setDataSource:self];
    
    stationSearchbox.layer.cornerRadius = 5;
    stationSearchbox.layer.borderWidth = 2;
    stationSearchbox.layer.borderColor = mSingleton.elementColor1.CGColor;
    
    searchAssistTable.layer.cornerRadius = 5;
    searchAssistTable.layer.borderWidth = 1;
    searchAssistTable.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [searchAssistTable setHidden:TRUE];
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    [spinnerSearch setHidden:TRUE];
    [spinnerSearch stopAnimating];
    
    
    if(!searchResultDictionary)
        searchResultDictionary = [[NSMutableDictionary alloc] init];
    
    if (!MRTSStationDetailsDictionary)
        MRTSStationDetailsDictionary = [[NSMutableDictionary alloc] init];
    
//    sourceCurrentLocation.layer.cornerRadius = 5;
//    sourceCurrentLocation.layer.borderWidth = 2;
//    sourceCurrentLocation.layer.borderColor = [[UIColor whiteColor] CGColor];
//    
//    eye.layer.cornerRadius = 5;
//    eye.layer.borderWidth = 2;
//    eye.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    MRTSsegment.selectedSegmentIndex = 0;
    
    [MRTSsearchView setHidden:TRUE];
    [MRTSsearchView removeFromSuperview];
    
    [MRTSroutesView setHidden:FALSE];
    [self.view addSubview:MRTSroutesView];
    if (IS_IPHONE_5)
        [MRTSroutesView setFrame:CGRectMake(0, 136, 320, 382)];
    else
        [MRTSroutesView setFrame:CGRectMake(0, 136, 320, 294)];

    [MRTSroutesView setBackgroundColor:[UIColor clearColor]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!didLoadfromResource)
        [self loadStagesFromResource];
    [locationManager startUpdatingLocation];
    
    [searchAssistTable setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!didLoadfromResource)
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadStagesFromResource) object:nil];
    [locationManager stopUpdatingLocation];
}

-(void)loadStagesFromResource
{
    didLoadfromResource = FALSE;
    [searchResultArray removeAllObjects];
    
    [allMRTSStations removeAllObjects];
    allMRTSStations = [mSingleton.allMRTSStations mutableCopy];
    
    [allMRTSStationDetails removeAllObjects];
    allMRTSStationDetails = [mSingleton.allMRTSStationsDetails mutableCopy];
    
    if (!filteredListContent)
        filteredListContent = [[NSMutableArray alloc] initWithArray:allMRTSStations copyItems:YES];
    
    
    [allMRTSRoutes removeAllObjects];
    allMRTSRoutes = [mSingleton.allMRTSRoutes mutableCopy];
    
    didLoadfromResource = TRUE;
}

-(IBAction)onHome:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = @"cameraIris";
    transition.subtype = kCATransitionFromLeft;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(IBAction)onStage:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = @"cube";
    transition.subtype = kCATransitionFromRight;
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[StageViewController class]]) {
            StageViewController *stageViewController = (StageViewController *)vc;
            
            if ([sender isKindOfClass:[NSString class]])
                [stageViewController updateDetailsTableForSource:sender andDestination:nil];
            
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    StageViewController *stageViewController = [[mSingleton getViewControllerManagerInstance] getStageViewController];
    if ([sender isKindOfClass:[NSString class]])
        [stageViewController updateDetailsTableForSource:sender andDestination:nil];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:stageViewController animated:NO];
}

-(IBAction)onRoute:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromLeft;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[RouteViewController class]])
        {
            RouteViewController *routeViewController = (RouteViewController *)vc;
            if ([sender isKindOfClass:[NSString class]])
                [routeViewController updateDetailsTableForRoute:sender];
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    RouteViewController *routeViewController = [[mSingleton getViewControllerManagerInstance] getRouteViewController];
    if ([sender isKindOfClass:[NSString class]])
        [routeViewController updateDetailsTableForRoute:sender];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:routeViewController animated:NO];
}

-(IBAction)onSearch:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromBottom;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[SearchViewController class]]) {
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    SearchViewController *searchViewController = [[mSingleton getViewControllerManagerInstance] getSearchViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchViewController animated:NO];
}

-(IBAction)onAutoButton:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromTop;
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[AutoViewController class]]) {
            AutoViewController *autoViewController = (AutoViewController *)vc;
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:autoViewController animated:NO];
            return;
        }
    }
    AutoViewController *autoViewController = [[mSingleton getViewControllerManagerInstance] getAutoViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    //[self.navigationController pushViewController:autoViewController animated:NO];
    [self.navigationController pushViewController:autoViewController animated:NO];
}

-(IBAction)onMap:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[MapViewController class]]) {
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
}

-(IBAction)onMapButton:(UIButton *)sender
{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    MRTSPathCell *cell = (MRTSPathCell*)[MRTSselectedRouteTable cellForRowAtIndexPath:myIP];
    
    NSMutableDictionary *stageMarker = [self getLocationDetailsForStation:cell.pathLabel.text];
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    
    [mapViewController plotStage:stageMarker];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
    
}

-(NSMutableDictionary *)getLocationDetailsForStation:(NSString *)stage
{
    NSMutableDictionary *Marker = [[NSMutableDictionary alloc] init];
    for (NSDictionary *dict in allMRTSStations)
    {
        if ([[dict allKeys] containsObject:@"lattitude"] && [[dict allKeys] containsObject:@"longitude"])
        {
            NSString *dictValue = [dict valueForKey:@"station_name"];
            id latitude = [dict valueForKey:@"lattitude"];
            id longitude = [dict valueForKey:@"longitude"];
            
            if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]] && ![latitude isMemberOfClass:[NSNull class]] && ![longitude isMemberOfClass:[NSNull class]])
            {
                [Marker setValue:stage forKey:@"stage"];
                if (![dict isKindOfClass:[NSNull class]])
                {
                    id latitude = [dict valueForKey:@"lattitude"];
                    [Marker setValue:latitude forKey:@"latitude"];
                }
                if (![[dict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
                {
                    id longitude = [dict valueForKey:@"longitude"];
                    [Marker setValue:longitude forKey:@"longitude"];
                }
                return Marker;
            }
        }
    }
    return nil;
}


-(IBAction)onEye:(id)sender
{
    [Flurry logEvent:@"onEye"];

    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MRTSMapViewController *MRTSMapViewController = [[mSingleton getViewControllerManagerInstance] getMRTSMapViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:MRTSMapViewController animated:NO];
}

-(IBAction)onMRTSsegment:(id)sender
{
    if (MRTSsegment.selectedSegmentIndex == 0)
    {
        [MRTSsearchView setHidden:TRUE];
        [MRTSsearchView removeFromSuperview];
        
        [MRTSroutesView setHidden:FALSE];
        [self.view addSubview:MRTSroutesView];
        if (IS_IPHONE_5)
            [MRTSroutesView setFrame:CGRectMake(0, 136, 320, 382)];
        else
            [MRTSroutesView setFrame:CGRectMake(0, 136, 320, 294)];

        [MRTSroutesView setBackgroundColor:[UIColor clearColor]];
        
    }
    else if(MRTSsegment.selectedSegmentIndex == 1)
    {
        [MRTSroutesView setHidden:TRUE];
        [MRTSroutesView removeFromSuperview];
        
        [MRTSsearchView setHidden:FALSE];
        [self.view addSubview:MRTSsearchView];
        if (IS_IPHONE_5)
            [MRTSsearchView setFrame:CGRectMake(0, 136, 320, 382)];
        else
            [MRTSsearchView setFrame:CGRectMake(0, 136, 320, 294)];

        [MRTSsearchView setBackgroundColor:[UIColor clearColor]];
        if (! searchAssistTable.hidden)
            [self setTableHidden:TRUE];
    }
}

-(IBAction)onNearestStation:(id)sender
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
    {
        CLLocationDistance nearest = 999999;
        CLLocation *searchLocation = [[CLLocation alloc] initWithLatitude:currentCentre.latitude longitude:currentCentre.longitude];
        NSDictionary *requiredDictionary = nil;
        for (NSDictionary *dict in allMRTSStations)
        {
            if ((![[dict valueForKey:@"lattitude"] isKindOfClass:[NSNull class]]) && (![[dict valueForKey:@"longitude"] isKindOfClass:[NSNull class]]))
            {
                NSNumber *latti = [dict valueForKey:@"lattitude"];
                NSNumber *longi = [dict valueForKey:@"longitude"];
                CLLocation *neatestLocation = [[CLLocation alloc] initWithLatitude:[latti doubleValue] longitude:[longi doubleValue]];
                CLLocationDistance meters = [searchLocation distanceFromLocation:neatestLocation];
                if ((meters <= nearest) && (latti && longi)) {
                    nearest = meters;
                    requiredDictionary = dict;
                }
            }
        }
        
        [stationSearchbox resignFirstResponder];
        stationSearchbox.text = [requiredDictionary valueForKey:@"station_name"];
        NSNumber *stnCode = [requiredDictionary valueForKey:@"_id"];
        
        if (! searchAssistTable.hidden)
            [self setTableHidden:TRUE];
        [self performSelectorInBackground:@selector(BGActivityForSearch:) withObject:stnCode];
    }
}

-(void)BGActivityForSearch:(NSNumber *)stnCode
{
    [Flurry logEvent:@"MRTS search"];
    [searchResultArray removeAllObjects];
    searchResultArray = [NSMutableArray arrayWithArray:[self getDetailsforStation:stnCode]];
    [searchResultTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    //[self fetchTabledataforStnDetails:searchResultArray];
}

-(void)updateDetailsTableForSource:(NSString *)source
{
    MRTSsegment.selectedSegmentIndex = 1;
    [self onMRTSsegment:nil];
    stationSearchbox.text = source;
    
    [stationSearchbox resignFirstResponder];
    if (! searchAssistTable.hidden)
        [self setTableHidden:TRUE];
    
    for (NSDictionary *section in self.allMRTSStations)
    {
        NSString *stnSection = [section valueForKey:@"station_name"];
        if ([[stnSection uppercaseString] isEqualToString:[source uppercaseString]])
        {
            NSString *stnCodeSection = [section valueForKey:@"_id"];
            [self performSelectorInBackground:@selector(BGActivityForSearch:) withObject:stnCodeSection];
            return;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
    for (NSDictionary *section in self.allMRTSStations) {
        NSString *stnSection = [section valueForKey:@"station_name"];
        if ([[stnSection uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound)
        {
            [self.filteredListContent addObject:section];
        }
        NSString *stnCodeSection = [section valueForKey:@"station_code"];
        if ([[stnCodeSection uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound)
        {
            if([self.filteredListContent indexOfObject:section] == NSNotFound)
                [self.filteredListContent addObject:section];
        }
    }
    
    if([self.filteredListContent count] == 0)
        [searchAssistTable setHidden:TRUE];
    else if (searchAssistTable.hidden)
        [self setTableHidden:FALSE];
    
    [self.searchAssistTable reloadData];
}

-(void)setTableHidden:(BOOL)flag
{
    [searchAssistTable setHidden:flag];
    NSTimeInterval animationDuration = 0.5;/* determine length of animation */;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    if (flag)
        searchAssistTable.frame = CGRectMake(searchAssistTable.frame.origin.x, searchAssistTable.frame.origin.y, searchAssistTable.frame.size.width, 0);
    else
        searchAssistTable.frame = CGRectMake(searchAssistTable.frame.origin.x, searchAssistTable.frame.origin.y, searchAssistTable.frame.size.width, searchAssistTable.frame.size.height);
    
    [UIView commitAnimations];
}

-(NSArray *)getDetailsforStation:(NSNumber *)stationID
{
    [spinnerSearch performSelectorOnMainThread:@selector(setHidden:) withObject:[NSNumber numberWithBool:FALSE] waitUntilDone:TRUE];
    [spinnerSearch performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:YES];
    [stationSearchbox performSelectorOnMainThread:@selector(setEnabled:) withObject:[NSNumber numberWithBool:FALSE] waitUntilDone:TRUE];
    
    
    NSMutableArray *stationDetails = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in allMRTSStationDetails)
    {
        NSNumber *stn = [dict valueForKey:@"station_id"];
        if ([stationID isEqualToNumber:stn])
        {
            if ([self checkValidStop:dict])
                [stationDetails addObject:dict];
        }
    }
    return stationDetails;
}

-(NSDictionary *)fetchTabledataforStnDetails:(NSArray *)stationDetails
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
       for (NSDictionary *detailDict in stationDetails)
    {        
        NSMutableDictionary *requiredDict = [[NSMutableDictionary alloc] init];
        NSNumber *routeNo = [detailDict valueForKey:@"route_no"];
        
        NSDictionary *srcDestArray = [self getSourceforRoute:routeNo] ;
        NSString *source = [srcDestArray valueForKey:@"source"];
        NSString *destination = [srcDestArray valueForKey:@"dest"];
        
        NSNumber *stopNo = [detailDict valueForKey:@"stop_no"];
        NSNumber *tripNo = [detailDict valueForKey:@"trip_no"];
        NSNumber *direction = [detailDict valueForKey:@"direction"];
        NSString *time = [detailDict valueForKey:@"time"];
        NSString *service = [detailDict valueForKey:@"serviceable"];
        
        [requiredDict setValue:routeNo forKey:@"route_no"];
        [requiredDict setValue:source forKey:@"source"];
        [requiredDict setValue:destination forKey:@"destination"];
        [requiredDict setValue:stopNo forKey:@"stop_no"];
        [requiredDict setValue:tripNo forKey:@"trip_no"];
        [requiredDict setValue:direction forKey:@"direction"];
        [requiredDict setValue:time forKey:@"time"];
        [requiredDict setValue:service forKey:@"service"];
        NSMutableArray *routeList = [dict objectForKey:@"MRTS"];
        if (!routeList)
        {
            routeList = [NSMutableArray array];
            [dict setObject:routeList forKey:@"MRTS"];
        }
        
        [routeList addObject:requiredDict];
    }
    [spinnerSearch performSelectorOnMainThread:@selector(setHidden:) withObject:[NSNumber numberWithBool:TRUE] waitUntilDone:TRUE];
    [spinnerSearch performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
    [stationSearchbox performSelectorOnMainThread:@selector(setEnabled:) withObject:[NSNumber numberWithBool:TRUE] waitUntilDone:TRUE];
    
    return dict;
}

-(NSDictionary *)getSourceforRoute:(NSNumber *)route
{
    NSString *source = nil;
    NSString *dest = nil;
    for (NSDictionary *dict in allMRTSStationDetails)
    {
        NSNumber *routeNo = [dict valueForKey:@"route_no"];
        NSNumber *stopNo = [dict valueForKey:@"stop_no"];
        NSNumber *srcNo = [dict valueForKey:@"station_id"];
        NSNumber *destnNo = [dict valueForKey:@"dest_station_id"];
        if ([route isEqualToNumber:routeNo] && [stopNo isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            source = [self getStationNameforID:srcNo];
            dest = [self getStationNameforID:destnNo];
            NSDictionary *resultDict = [NSDictionary dictionaryWithObjectsAndKeys:source, @"source",
                                        dest, @"dest",
                                        nil];
            return resultDict;
            
        }
    }
    return nil;
}

-(NSString *)getStationNameforID:(NSNumber *)stnID
{
    for (NSDictionary *dict in allMRTSStations)
    {
        if ([stnID isEqualToNumber:[dict valueForKey:@"_id"]])
        {
            return [dict valueForKey:@"station_name"];
        }
    }
    return nil;
}

-(BOOL)checkValidStop:(NSDictionary *)dict
{
    if ([[dict objectForKey:@"stop_no"] isEqualToNumber:[NSNumber numberWithInt:0]])
        return FALSE;
    else if ([[dict objectForKey:@"time"] length] == 0 )
        return FALSE;
    
    return TRUE;
}


#pragma mark - CLLocationManager methods.
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)
    {
        currentCentre = newLocation.coordinate;
    }
    else
    {
        currentCentre.latitude = 13.052413900000000000;
        currentCentre.longitude = 80.250824599999990000;
    }
}

#pragma mark-
#pragma mark tableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //For Routes
    if (tableView == MRTSallRoutesTable)
    {
        return 1;
    }
    //For Search
    else if (tableView == searchAssistTable)
    {
        return 1;
    }
    else if(tableView == searchResultTable)
    {
        return 1;
    }
    else if(tableView == MRTSselectedRouteTable)
    {
        return 1;
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == MRTSallRoutesTable)
    {
        return @"All Trains";
    }
    else if (tableView == searchResultTable)
    {
        if ([searchResultDictionary count] > 0)
            return [NSString stringWithFormat:@"at %@",stationSearchbox.text];
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //For Routes
    if (tableView == MRTSallRoutesTable)
    {
        return [allMRTSRoutes count];
    }
    else if(tableView == MRTSselectedRouteTable)
    {
        return [MRTSStationDetailsDictionary count];
    }
    //For Search
    else if (tableView == searchAssistTable)
    {
        CGRect tableHeight = searchAssistTable.frame;
        if (IS_IPHONE_5)
        {
            if ([filteredListContent count] < 5)
            {
                tableHeight.size.height = 44 * [filteredListContent count];
            }
            else
            {
                tableHeight.size.height = 176;
                [searchAssistTable flashScrollIndicators];
            }
        }
        else
        {
            if ([filteredListContent count] < 3)
            {
                tableHeight.size.height = 44 * [filteredListContent count];
            }
            else
            {
                tableHeight.size.height = 132;
                [searchAssistTable flashScrollIndicators];
            }
        }
        self.searchAssistTable.frame = tableHeight;
        return [self.filteredListContent count];
    }
    else if(tableView == searchResultTable)
    {
        [self.searchResultDictionary removeAllObjects];
        //NSNumber *count = [NSNumber numberWithInt:0];
        for (NSDictionary *dict in searchResultArray)
        {
            NSNumber *routeNO = [dict objectForKey:@"route_no"];
            NSNumber *Direction = [dict objectForKey:@"direction"];
            
            NSMutableArray *routeList = [searchResultDictionary objectForKey:[NSString stringWithFormat:@"%@/%@", routeNO,Direction]];
            if (!routeList)
            {
                routeList = [NSMutableArray array];
                [searchResultDictionary setObject:routeList forKey:[NSString stringWithFormat:@"%@/%@", routeNO,Direction]];
            }
            [routeList addObject:dict];
        }
        
        if ([searchResultDictionary count] <= 0)
        {
            [searchResultTable setHidden:TRUE];
            [noResult setHidden:FALSE];
        }
        else
        {
            [searchResultTable setHidden:FALSE];
            [noResult setHidden:TRUE];
        }
        return [searchResultDictionary count];
    }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == MRTSselectedRouteTable)
    {
        TrainDetailsSectionView *sectionView = [TrainDetailsSectionView trainDetailsSectionView];
        [sectionView.backButton addTarget:self action:@selector(closeView:) forControlEvents:UIControlEventTouchUpInside];
        [sectionView.swapButton addTarget:self action:@selector(swapLocations:) forControlEvents:UIControlEventTouchUpInside];
        sectionView.index = 0;
        NSDictionary *routeDict;
        if ((selectedIndex < 0) && ((selectedIndex * -1) < allMRTSRoutes.count))
        {
            routeDict = [allMRTSRoutes objectAtIndex:(selectedIndex * -1)];
            NSArray *items = [[routeDict valueForKey:@"route_desc"] componentsSeparatedByString:@"-"];
            
            sectionView.destination.text = [[items objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            sectionView.source.text = [[items objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }
        else if ((selectedIndex > 0) && (selectedIndex < allMRTSRoutes.count))

        {
            routeDict = [allMRTSRoutes objectAtIndex:selectedIndex];
            NSArray *items = [[routeDict valueForKey:@"route_desc"] componentsSeparatedByString:@"-"];
            
            sectionView.source.text = [[items objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            sectionView.destination.text = [[items objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }
        
        return sectionView;
    }
    return nil;
}

- (void)closeView:(id)sender
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         MRTSallRoutesTable.alpha = 1;
                         MRTSselectedRouteTable.alpha = 0;
                         [MRTSallRoutesTable reloadData];
                     }];
    
}

- (void)swapLocations:(id)sender
{
    NSDictionary *routeDict;
    if ([allMRTSRoutes count] > 0)
    {
        if (selectedIndex < 0)
        {
            routeDict = [allMRTSRoutes objectAtIndex:(selectedIndex * -1)];
            [self getTrainDetailsForRoute:[[routeDict valueForKey:@"_id"] intValue] andDirection:1];
        }
        else
        {
            routeDict = [allMRTSRoutes objectAtIndex:selectedIndex];
            [self getTrainDetailsForRoute:[[routeDict valueForKey:@"_id"] intValue] andDirection:0];
        }
        selectedIndex = selectedIndex * -1;
        
        [MRTSselectedRouteTable reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //For Routes
    if (tableView == MRTSallRoutesTable)
    {
        static NSString * CellIdentifier = @"MRTSroutesCell";
        MRTSroutesCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MRTSroutesCell" owner:self options:nil];
            cell = (MRTSroutesCell *)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        NSDictionary *routeDict = [allMRTSRoutes objectAtIndex:indexPath.row];
        NSArray *items = [[routeDict valueForKey:@"route_desc"] componentsSeparatedByString:@"-"];
        
        cell.source.text = [[items objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        cell.destination.text = [[items objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        
        return cell;
    }
    if (tableView == MRTSselectedRouteTable)
    {
        static NSString * CellIdentifier = @"MRTSPathCell";
        MRTSPathCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MRTSPathCell" owner:self options:nil];
            cell = (MRTSPathCell *)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        
        NSString *key = [[MRTSStationDetailsDictionary allKeys] objectAtIndex:indexPath.row];
        NSArray *Value = [MRTSStationDetailsDictionary valueForKey:key];
        
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"trip_no"  ascending:YES];
        Value = [Value sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        
        NSString *Path = [self getStationNameforID:[NSNumber numberWithInt:[key intValue]]];
        cell.pathLabel.text = Path;
        NSString *stopNO = @"";
        if ([Value count] > 0) {
            NSDictionary *dict = [Value objectAtIndex:0];
            if ([[dict valueForKey:@"stop_no"] intValue] > 0)
                stopNO = [NSString stringWithFormat:@"Stop No : %d",[[dict valueForKey:@"stop_no"] intValue]];
        }
        cell.stopLabel.text = stopNO;
        
        NSString *servicesCount = [NSString stringWithFormat:@"No of Services : %d",[Value count]];
        cell.servicesLabel.text = servicesCount;
        
        cell.carousel.forTable = tableView;
        cell.carousel.delegate = self;
        cell.carousel.dataSource = self;
        cell.carousel.tag = indexPath.row;
        cell.carousel.type = iCarouselTypeLinear;
        
        [cell.carousel setFrame:CGRectMake(47, 52, 268, 36)];
        [cell.carousel reloadData];
        
        cell.mapitButton.tag = indexPath.row;
        if ([self isLocationAvailableforStation:cell.pathLabel.text])
        {
            [cell.mapitButton removeFromSuperview];
            [cell.contentView addSubview:cell.mapitButton];
            [cell bringSubviewToFront:cell.mapitButton];
            [cell.mapitButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.mapitButton setHidden:FALSE];
        }
        else
        {
            [cell.mapitButton removeTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.mapitButton setHidden:TRUE];
        }
        return cell;
    }
    //For Search
    else if (tableView == searchAssistTable)
    {
        static NSString * CellIdentifier = @"MRTSstationsSearchCell";
        MRTSstationsSearchCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MRTSstationsSearchCell" owner:self options:nil];
            cell = (MRTSstationsSearchCell *)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        
        cell.stationName.text = [[filteredListContent objectAtIndex:indexPath.row] valueForKey:@"station_name"];
        cell.stationCode.text = [[filteredListContent objectAtIndex:indexPath.row] valueForKey:@"station_code"];
        cell.stationID = [[filteredListContent objectAtIndex:indexPath.row] valueForKey:@"_id"];
        
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        
        return cell;
    }
    else if(tableView == searchResultTable)
    {
        NSString *key = [[searchResultDictionary allKeys] objectAtIndex:indexPath.row];
        NSArray *Value = [searchResultDictionary valueForKey:key];
        static NSString * CellIdentifier = @"MRTSsearchResultCell";
        MRTSsearchResultCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MRTSsearchResultCell" owner:self options:nil];
            cell = (MRTSsearchResultCell *)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        NSArray *items = [key componentsSeparatedByString:@"/"];
        NSString *Path = [self getPathforRoute:[[items objectAtIndex:0] intValue]];
        
        cell.stop.text =[NSString stringWithFormat:@"Stop No: %@",[[Value objectAtIndex:0] valueForKey:@"stop_no"]];
        
        NSString *servicesCount = [NSString stringWithFormat:@"No of Services : %d",[Value count]];
        cell.servicesLabel.text = servicesCount;
        
        cell.carousel.forTable = tableView;
        cell.carousel.delegate = self;
        cell.carousel.dataSource = self;
        cell.carousel.tag = indexPath.row;
        cell.carousel.type = iCarouselTypeLinear;
        cell.routeID = [[Value objectAtIndex:0] valueForKey:@"route_no"];
        cell.direction = [[Value objectAtIndex:0] valueForKey:@"direction"];

        [cell.carousel setFrame:CGRectMake(52, 52, 268, 36)];
        
        [cell.carousel reloadData];
        cell.carousel.vertical = FALSE;
        if([[items objectAtIndex:1] intValue] == 0)
        {
            items = [Path componentsSeparatedByString:@"-"];
            cell.source.text = [[items objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            cell.destination.text = [[items objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }
        else
        {
            items = [Path componentsSeparatedByString:@"-"];
            cell.destination.text = [[items objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            cell.source.text = [[items objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }
        return cell;
    }
    
    return nil;
}

-(NSString *)getPathforRoute:(int)route
{
    for (NSDictionary *dict in allMRTSRoutes)
    {
        int routeNo = [[dict objectForKey:@"_id"] intValue];
        if (route == routeNo)
        {
            NSString *path = [dict objectForKey:@"route_desc"];
            return path;
        }
    }
    return nil;
}

-(BOOL)isLocationAvailableforStation:(NSString *)stage
{
    for (NSDictionary *dict in allMRTSStations)
    {
        if ([[dict allKeys] containsObject:@"lattitude"] && [[dict allKeys] containsObject:@"longitude"])
        {
            NSString *dictValue = [dict valueForKey:@"station_name"];
            id latitude = [dict valueForKey:@"lattitude"];
            id longitude = [dict valueForKey:@"longitude"];
            
            if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]] && ![latitude isMemberOfClass:[NSNull class]] && ![longitude isMemberOfClass:[NSNull class]]) {
                return YES;
            }
        }
    }
    return NO;
}
#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if(carousel.forTable == searchResultTable)
    {
        NSString *key = [[searchResultDictionary allKeys] objectAtIndex:carousel.tag];
        NSArray *Value = [searchResultDictionary valueForKey:key];
        
        return [Value count] + 1;
    }
    else if(carousel.forTable == MRTSselectedRouteTable)
    {
        NSString *key = [[MRTSStationDetailsDictionary allKeys] objectAtIndex:carousel.tag];
        NSArray *Value = [MRTSStationDetailsDictionary valueForKey:key];

        return [Value count] + 1;
    }
    return 0;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    if (index == 0)
    {
        if (!view)
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 41, 36)];
        
        for (UIView *temp in view.subviews)
            [temp removeFromSuperview];
        
        UIImageView *engineImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 41, 36)];
        [engineImage setImage:[UIImage imageNamed:@"ico_engine"]];
        view.layer.borderWidth = 0;
        [view addSubview:engineImage];
        view.alpha = 0.8;
        
        return view;
    }
    
    if(carousel.forTable == searchResultTable)
    {
        UILabel *trip = nil;
        UILabel *time = nil;
        
        //create new view if no view is available for recycling
        if (view == nil)
        {
            int width = 40;
            
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 36)];
            view.layer.borderWidth = 0;
            
            [view setBackgroundColor:[UIColor clearColor]];
            view.contentMode = UIViewContentModeCenter;
            
            time = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 36)];
            time.backgroundColor = mSingleton.elementColor1;
            time.textAlignment = NSTextAlignmentCenter;
            time.font = [time.font fontWithSize:14];
            time.textColor = mSingleton.bgColor;
            
            trip.tag = 100;
            time.tag = 99;
            
            view.layer.cornerRadius = 5;
            view.layer.borderWidth = 0.5;
            view.layer.borderColor = [[UIColor whiteColor] CGColor];
            
            [view addSubview:trip];
            [view addSubview:time];
        }
        else
        {
            if ([view.subviews count] == 1)
            {
                for (UIView *temp in view.subviews)
                    [temp removeFromSuperview];
                
                int width = 40;
                
                view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 36)];
                view.layer.borderWidth = 0;
                
                [view setBackgroundColor:[UIColor clearColor]];
                view.contentMode = UIViewContentModeCenter;
                
                time = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 36)];
                time.backgroundColor = mSingleton.elementColor1;
                time.textAlignment = NSTextAlignmentCenter;
                time.font = [time.font fontWithSize:14];
                time.textColor = mSingleton.bgColor;
                
                trip.tag = 100;
                time.tag = 99;
                
                view.layer.cornerRadius = 5;
                view.layer.borderWidth = 0.5;
                view.layer.borderColor = [[UIColor whiteColor] CGColor];
                
                [view addSubview:trip];
                [view addSubview:time];
            }
            else
            {
                //get a reference to the label in the recycled view
                time = (UILabel *)[view viewWithTag:99];
                trip = (UILabel *)[view viewWithTag:100];
            }
            
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
        NSString *key = [[searchResultDictionary allKeys] objectAtIndex:carousel.tag];
        NSArray *Value = [searchResultDictionary valueForKey:key];
        NSDictionary *dict = [Value objectAtIndex:index - 1];
        
        if (![dict valueForKey:@"time"] || [[dict valueForKey:@"time"] isEqualToString:@""])
            time.text = @"-";
        else
            time.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"time"]];
        
        trip.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"trip_no"]];
        
        return view;
    }
    else if(carousel.forTable == MRTSselectedRouteTable)
    {
        UILabel *trip = nil;
        UILabel *time = nil;
        
        //create new view if no view is available for recycling
        if (view == nil)
        {
            int width = 40;
            
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 36)];
            view.layer.borderWidth = 0;
            
            [view setBackgroundColor:[UIColor clearColor]];
            view.contentMode = UIViewContentModeCenter;
            
            trip = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 12)];
            trip.backgroundColor = mSingleton.elementColor1;
            trip.textAlignment = NSTextAlignmentCenter;
            trip.font = [trip.font fontWithSize:12];
            trip.textColor = mSingleton.bgColor;
            
            time = [[UILabel alloc] initWithFrame:CGRectMake(0, 12, width, 24)];
            time.backgroundColor = mSingleton.elementColor1;
            time.textAlignment = NSTextAlignmentCenter;
            time.font = [time.font fontWithSize:14];
            time.textColor = mSingleton.bgColor;
            
            trip.tag = 100;
            time.tag = 99;
            
            view.layer.cornerRadius = 5;
            view.layer.borderWidth = 0.5;
            view.layer.borderColor = [[UIColor whiteColor] CGColor];
            
            [view addSubview:trip];
            [view addSubview:time];
        }
        else
        {
            if ([view.subviews count] == 1)
            {
                for (UIView *temp in view.subviews)
                    [temp removeFromSuperview];
                
                int width = 40;
                
                view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 36)];
                view.layer.borderWidth = 0;
                
                [view setBackgroundColor:[UIColor clearColor]];
                view.contentMode = UIViewContentModeCenter;
                
                trip = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 12)];
                trip.backgroundColor = mSingleton.elementColor1;
                trip.textAlignment = NSTextAlignmentCenter;
                trip.font = [trip.font fontWithSize:12];
                trip.textColor = mSingleton.bgColor;
                
                time = [[UILabel alloc] initWithFrame:CGRectMake(0, 12, width, 24)];
                time.backgroundColor = mSingleton.elementColor1;
                time.textAlignment = NSTextAlignmentCenter;
                time.font = [time.font fontWithSize:14];
                time.textColor = mSingleton.bgColor;
                
                trip.tag = 100;
                time.tag = 99;
                
                view.layer.cornerRadius = 5;
                view.layer.borderWidth = 0.5;
                view.layer.borderColor = [[UIColor whiteColor] CGColor];

                [view addSubview:trip];
                [view addSubview:time];
            }
            else
            {
                //get a reference to the label in the recycled view
                time = (UILabel *)[view viewWithTag:99];
                trip = (UILabel *)[view viewWithTag:100];
            }
            
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
        NSString *key = [[MRTSStationDetailsDictionary allKeys] objectAtIndex:carousel.tag];
        NSArray *Value = [MRTSStationDetailsDictionary valueForKey:key];
        NSDictionary *dict = [Value objectAtIndex:index - 1];
        
        if (![dict valueForKey:@"time"] || [[dict valueForKey:@"time"] isEqualToString:@""])
            time.text = @"-";
        else
            time.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"time"]];
        
        trip.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"trip_no"]];
        
        return view;
    }
    return nil;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    return 41;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    if (index == 0)
    {
        [carousel scrollToItemAtIndex:1 animated:YES];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == searchAssistTable)
    {
        MRTSstationsSearchCell *cell = (MRTSstationsSearchCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (! searchAssistTable.hidden)
            [self setTableHidden:TRUE];
        
        if ([stationSearchbox isFirstResponder])
            [stationSearchbox resignFirstResponder];
        
        stationSearchbox.text = cell.stationName.text;
        [self performSelectorInBackground:@selector(BGActivityForSearch:) withObject:cell.stationID];
    }
    else if(tableView == MRTSallRoutesTable)
    {
        NSDictionary *routeDict = [allMRTSRoutes objectAtIndex:indexPath.row];
        [self getTrainDetailsForRoute:[[routeDict valueForKey:@"_id"] intValue] andDirection:0];
        
        selectedIndex = indexPath.row;
        MRTSallRoutesTable.alpha = 0;
        MRTSselectedRouteTable.alpha = 1;
        [MRTSselectedRouteTable reloadData];
    }
    else if(tableView == searchResultTable)
    {
        MRTSsearchResultCell *cell = (MRTSsearchResultCell *)[tableView cellForRowAtIndexPath:indexPath];
        MRTSsegment.selectedSegmentIndex = 0;
        [self onMRTSsegment:nil];

        [self getTrainDetailsForRoute:[cell.routeID intValue] andDirection:[cell.direction intValue]];
        if ([cell.direction intValue] == 0)
            selectedIndex = [cell.routeID intValue];
        else
            selectedIndex = [cell.routeID intValue] * -1;
        
        [tableView deselectRowAtIndexPath:indexPath animated:TRUE];

        [UIView animateWithDuration:0.5
                         animations:^{
                             MRTSallRoutesTable.alpha = 0;
                             MRTSselectedRouteTable.alpha = 1;
                             [MRTSselectedRouteTable reloadData];
                         }];

    }
    else if(tableView == MRTSselectedRouteTable)
    {
        MRTSPathCell *cell = (MRTSPathCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (! searchAssistTable.hidden)
            [self setTableHidden:TRUE];
        
        if ([stationSearchbox isFirstResponder])
            [stationSearchbox resignFirstResponder];
        
        stationSearchbox.text = cell.pathLabel.text;
        [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
        [self performSelectorInBackground:@selector(updateDetailsTableForSource:) withObject:cell.pathLabel.text];
    }
}


-(NSMutableDictionary *)getTrainDetailsForRoute:(int)route andDirection:(int)direction
{
    [self.MRTSStationDetailsDictionary removeAllObjects];
    for (NSDictionary *dict in allMRTSStationDetails)
    {
        NSNumber *routeNO = [dict objectForKey:@"route_no"];
        NSNumber *Direction = [dict objectForKey:@"direction"];
        NSNumber *stationID = [dict objectForKey:@"station_id"];
        
        if (([routeNO intValue] == route) && ([Direction intValue] == direction))
        {
            NSMutableArray *routeList = [MRTSStationDetailsDictionary objectForKey:[NSString stringWithFormat:@"%@",stationID]];
            if (!routeList)
            {
                routeList = [NSMutableArray array];
                [MRTSStationDetailsDictionary setObject:routeList forKey:[NSString stringWithFormat:@"%@",stationID]];
            }
            [routeList addObject:dict];
        }
    }
    return MRTSStationDetailsDictionary;
}

#pragma mark-
#pragma mark TextFiled Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == stationSearchbox)
    {
        [stationSearchbox setFont:[mSingleton getFontOfSize:14]];
        [stationSearchbox setTextColor:[UIColor blackColor]];
        
        [self filterContentForSearchText:stationSearchbox.text scope:nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == stationSearchbox)
        [self filterContentForSearchText:[textField.text stringByReplacingCharactersInRange:range withString:string] scope:nil];
    
    if (textField == stationSearchbox && [[textField.text stringByReplacingCharactersInRange:range withString:string] length] == 0)
    {
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == stationSearchbox)
    {
        if ([filteredListContent count] > 0)
        {
            NSNumber *stnCode = nil;
            if([self.filteredListContent indexOfObject:textField.text] == NSNotFound)
            {
                textField.text = [[self.filteredListContent objectAtIndex:0] valueForKey:@"station_name"];
                stnCode = [[filteredListContent objectAtIndex:0] valueForKey:@"_id"];
            }
            if (! searchAssistTable.hidden)
                [self setTableHidden:TRUE];
            
            [self performSelectorInBackground:@selector(BGActivityForSearch:) withObject:stnCode];
        }
    }
    return YES;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view class] == [UITableViewCell class])
        return NO;
    
    return YES;
    
}
@end
