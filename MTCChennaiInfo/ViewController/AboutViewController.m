//
//  AboutViewController.m
//  MTCChennaiInfo
//
//  Created by Gautham on 21/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "AboutViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Singleton.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.layer.cornerRadius = 10;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!versionlabel)
        versionlabel = [[UILabel alloc] init];
    
    NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
    [versionlabel setText:[NSString stringWithFormat:@"v%@",versionString]];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:mSingleton.bgColor];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onBack:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
    
}

@end
