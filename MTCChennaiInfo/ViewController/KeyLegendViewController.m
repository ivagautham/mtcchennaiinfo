//
//  KeyLegendViewController.m
//  MTCChennaiInfo
//
//  Created by Gautham on 21/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "KeyLegendViewController.h"
#import "Singleton.h"
#import "SearchViewController.h"
#import "StageViewController.h"
#import "RouteViewController.h"
#import "MapViewController.h"
#import "SearchDetailsCell.h"
#import "AutoViewController.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])
@interface KeyLegendViewController ()
{
    NSMutableArray *carouselObjects;
    BOOL isLoading;
    NSString *currentValue;
    NSInteger selectedIndex;
}

@end

@implementation KeyLegendViewController
@synthesize carousel;
@synthesize allStages;
@synthesize allRoutes;
@synthesize routeInfo;
@synthesize stageDetails;
@synthesize keyTable;
@synthesize loading;
@synthesize keyResult;
@synthesize loadingLabel;
@synthesize tempArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.layer.cornerRadius = 10;
        }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_SHOW_QOS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_HIDE_QOS object:nil];
    [allStages removeAllObjects];
    allStages = [mSingleton.allStages mutableCopy];
    
    [allRoutes removeAllObjects];
    allRoutes = [mSingleton.allRoutes mutableCopy];
    
    [routeInfo removeAllObjects];
    routeInfo = [mSingleton.routeInfo mutableCopy];
    
    [stageDetails removeAllObjects];
    stageDetails = [mSingleton.stageDetails mutableCopy];
    
    if(!keyResult)
        keyResult =[[NSMutableArray alloc] init];
    [keyResult removeAllObjects];
    
    if(!tempArray)
        tempArray =[[NSMutableArray alloc] init];
    [tempArray removeAllObjects];
    
    if(!currentValue)
        currentValue = [[NSString alloc] init];
    
    //    keyTable.layer.cornerRadius = 10;
    //    keyTable.layer.borderWidth = 2;
    keyTable.layer.borderColor = [mSingleton.bgColor CGColor];
    isLoading = FALSE;
    [loadingLabel setHidden:TRUE];
    [self setupCarousel];
    [self.view setBackgroundColor:mSingleton.bgColor];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setupCarousel
{
    carouselObjects = [[NSMutableArray alloc] init];
    [carouselObjects removeAllObjects];
    carouselObjects = [mSingleton.routeTypes mutableCopy];
    [carouselObjects addObjectsFromArray:[NSArray arrayWithObjects:@"All", @"Termius", @"Stages", @"Routes", nil]];
    //Removing small bus, no details yet
    [carouselObjects removeObject:@"Small Bus"];
    carousel.type = iCarouselTypeCoverFlow;
    carousel.bounces = FALSE;
    [carousel reloadData];
    selectedIndex = [carouselObjects count]/2;
    [carousel scrollToItemAtIndex:(selectedIndex) animated:NO];
    NSString *val = [carouselObjects objectAtIndex:(selectedIndex)];
    [self performSelector:@selector(finishSetUP) withObject:nil afterDelay:0.2];
    [self performSelectorInBackground:@selector(loadTableForValue:) withObject:val];
}

-(void)finishSetUP
{
    UIView *currView = [self.carousel itemViewAtIndex:(selectedIndex)];
    [currView setBackgroundColor:mSingleton.elementColor1];
    currView.layer.borderColor = [mSingleton.bgColor CGColor];
    [tempArray removeAllObjects];
    [keyTable setHidden:FALSE];
    [loading setHidden:FALSE];
    [loading startAnimating];
    isLoading = TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onStage:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[StageViewController class]]) {
            StageViewController *stageViewController = (StageViewController *)vc;
            
            if ([sender isKindOfClass:[NSString class]])
                [stageViewController updateDetailsTableForSource:sender andDestination:nil];
            
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    StageViewController *stageViewController = [[mSingleton getViewControllerManagerInstance] getStageViewController];
    if ([sender isKindOfClass:[NSString class]])
        [stageViewController updateDetailsTableForSource:sender andDestination:nil];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:stageViewController animated:NO];
}

-(IBAction)onRoute:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[RouteViewController class]])
        {
            RouteViewController *routeViewController = (RouteViewController *)vc;
            if ([sender isKindOfClass:[NSString class]])
                [routeViewController updateDetailsTableForRoute:sender];
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    RouteViewController *routeViewController = [[mSingleton getViewControllerManagerInstance] getRouteViewController];
    if ([sender isKindOfClass:[NSString class]])
        [routeViewController updateDetailsTableForRoute:sender];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:routeViewController animated:NO];
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [carouselObjects count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 100)];
        view.layer.cornerRadius = 5;
        view.layer.borderWidth = 2;
        view.layer.borderColor = [mSingleton.elementColor1 CGColor];
        
        [view setBackgroundColor:[UIColor whiteColor]];
        view.contentMode = UIViewContentModeCenter;
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [mSingleton getFontOfSize:15];
        label.textColor = mSingleton.textColor1;
        view.tag = index;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = [carouselObjects objectAtIndex:index];
    
    return view;
}

- (BOOL)carousel:(iCarousel *)carousel shouldSelectItemAtIndex:(NSInteger)index
{
    if (isLoading)
    {
        if (selectedIndex != index)
        {
            UIView *currView = [self.carousel itemViewAtIndex:index];
            currView.layer.borderColor = [mSingleton.elementColor1 CGColor];
            
            currView.backgroundColor = [UIColor colorWithRed:(200.0/255.0) green:(100.0/255.0) blue:(50.0/255.0) alpha:1.0];
            [currView performSelector:@selector(setBackgroundColor:) withObject:[UIColor whiteColor] afterDelay:0.5];
        }
    }
    else
    {
        selectedIndex = index;
    }
    return !isLoading;
}


- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    for (int i = 0; i < [carouselObjects count]; i++)
    {
        UIView *view = [self.carousel itemViewAtIndex:i];
        [view setBackgroundColor:[UIColor whiteColor]];
        view.layer.borderColor = [mSingleton.elementColor1 CGColor];
    }
    
    UIView *currView = [self.carousel currentItemView];
    [currView setBackgroundColor:mSingleton.elementColor1];
    currView.layer.borderColor = [mSingleton.bgColor CGColor];
    NSString *val = [carouselObjects objectAtIndex:currView.tag];
    [tempArray removeAllObjects];
    [keyTable setHidden:FALSE];
    [loading setHidden:FALSE];
    [loading startAnimating];
    isLoading = TRUE;
    
    [self performSelectorInBackground:@selector(loadTableForValue:) withObject:val];
}

-(void)loadTableForValue:(NSString *)value
{
    currentValue = value;
    if ([value isEqualToString:@"All"])
    {
        for (NSString *section in self.allStages)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:section forKey:FIELD_SEARCH_RESULT];
            [dict setValue:FIELD_STAGE_SEARCH forKey:FIELD_SEARCH_TYPE];
            [tempArray addObject:dict];
        }
        for (NSString *section in self.allRoutes)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:section forKey:FIELD_SEARCH_RESULT];
            [dict setValue:FIELD_ROUTE_SEARCH forKey:FIELD_SEARCH_TYPE];
            [tempArray addObject:dict];
        }
    }
    else if ([value isEqualToString:@"Stages"])
    {
        for (NSString *section in self.allStages)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:section forKey:FIELD_SEARCH_RESULT];
            [dict setValue:FIELD_STAGE_SEARCH forKey:FIELD_SEARCH_TYPE];
            [tempArray addObject:dict];
        }
    }
    else if ([value isEqualToString:@"Routes"])
    {
        for (NSString *section in self.allRoutes)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:section forKey:FIELD_SEARCH_RESULT];
            [dict setValue:FIELD_ROUTE_SEARCH forKey:FIELD_SEARCH_TYPE];
            [tempArray addObject:dict];
        }
    }
    else if ([value isEqualToString:@"Ordinary"] || [value isEqualToString:@"General Shift"] || [value isEqualToString:@"Night Service"] || [value isEqualToString:@"Express"] || [value isEqualToString:@"Deluxe"] || [value isEqualToString:@"Air Condition"])
    {
        for (NSString *section in self.allRoutes)
        {
            for (NSDictionary *dictionary in routeInfo)
            {
                if (([[[dictionary objectForKey:FIELD_STAGE_TYPE] uppercaseString] isEqual:[value uppercaseString]]) && ([[[dictionary objectForKey:FIELD_STAGE_NUMBER] uppercaseString] isEqual:[section uppercaseString]]))
                {
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                    [dict setValue:section forKey:FIELD_SEARCH_RESULT];
                    [dict setValue:FIELD_ROUTE_SEARCH forKey:FIELD_SEARCH_TYPE];
                    [tempArray addObject:dict];
                    break;
                }
            }
        }
    }
    else if ([value isEqualToString:@"Termius"])
    {
        for (NSString *section in self.allStages)
        {
            if ([[[self getDetailsForStage:section] valueForKey:@"is_terminus"] boolValue])
            {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:section forKey:FIELD_SEARCH_RESULT];
                [dict setValue:FIELD_STAGE_SEARCH forKey:FIELD_SEARCH_TYPE];
                [tempArray addObject:dict];
            }
        }
    }
    
    [loadingLabel performSelectorOnMainThread:@selector(setHidden:) withObject:[NSNumber numberWithBool:FALSE] waitUntilDone:NO];
    [loading performSelectorOnMainThread:@selector(setHidden:) withObject:[NSNumber numberWithBool:TRUE] waitUntilDone:NO];
    [loading performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
    [keyTable performSelectorOnMainThread:@selector(setHidden:) withObject:[NSNumber numberWithBool:FALSE] waitUntilDone:NO];
    [keyTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];

    isLoading = FALSE;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.1f;
    }
    
    if (option == iCarouselOptionWrap)
    {
        return NO;
    }
    
    return value;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == keyTable)
        return 1;
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = nil;
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [aView setBackgroundColor:mSingleton.elementColor1];
    aView.contentMode = UIViewContentModeCenter;
    label = [[UILabel alloc] initWithFrame:aView.bounds];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [mSingleton getFontOfSize:15];
    [label setTextColor:[UIColor whiteColor]];
    label.text = currentValue;
    [aView addSubview:label];
    
    return aView;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*) indexPath
{
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == keyTable)
    {
        CGRect tableHeight = keyTable.frame;
        [keyResult removeAllObjects];
        [keyResult addObjectsFromArray:tempArray];
        if ([keyResult count] < 5)
        {
            tableHeight.size.height = 60 * [keyResult count];
        }
        else
        {
            tableHeight.size.height = 358;
            [keyTable flashScrollIndicators];
        }
        keyTable.frame = tableHeight;
        
        if ([keyResult count] == 0)
        {
            [loading setHidden:TRUE];
            [loading stopAnimating];
            [keyTable setHidden:TRUE];
        }
        else
        {
            [keyTable setHidden:FALSE];
        }
        return [keyResult count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == keyTable)
    {
        NSString * CellIdentifier = currentValue;
        SearchDetailsCell *cell = (SearchDetailsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchDetailsCell"owner:self options:nil];
            cell = (SearchDetailsCell *)[topLevelObjects objectAtIndex:0];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        NSDictionary *cellDict = [keyResult objectAtIndex:indexPath.row];
        NSString *resultType = [cellDict objectForKey:FIELD_SEARCH_TYPE];
        if ([resultType isEqualToString:FIELD_STAGE_SEARCH])
        {
            NSString *stage = [cellDict objectForKey:FIELD_SEARCH_RESULT];
            cell.contentLabel.text = stage;
            [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_stagesType.png"]];
            if ([self isLocationAvailableforStage:stage])
            {
                NSDictionary *stageDetailDictionay = [NSDictionary dictionaryWithDictionary:[self getDetailsForStage:stage]];
                
                NSString *importance, *isTermius;
                if ([stageDetailDictionay count] > 0)
                {
                    if (![[stageDetailDictionay valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
                    {
                        BOOL termius = [[stageDetailDictionay valueForKey:@"is_terminus"] boolValue];
                        isTermius = [NSString stringWithFormat:@"%@",termius ? @"YES" : @"NO"];
                    }
                    else
                        isTermius = @"-";
                    
                    if (![[stageDetailDictionay valueForKey:@"importance"] isKindOfClass:[NSNull class]])
                        importance = [[stageDetailDictionay valueForKey:@"importance"] stringValue];
                    else
                        importance = @"-";
                }
                else
                {
                    importance = @"-";
                    isTermius = @"-";
                }
                cell.detalilLabel.text = [NSString stringWithFormat:@"Stage Importance: %@\nTermius: %@",importance, isTermius];
                cell.plotButton.tag = indexPath.row;
                [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.autoButton.tag = indexPath.row;
                [cell.autoButton addTarget:self action:@selector(onAutoButton:) forControlEvents:UIControlEventTouchUpInside];

                [cell.contentView setUserInteractionEnabled: NO];
            }
            else
            {
                [cell.plotButton setHidden:TRUE];
                [cell.autoButton setHidden:TRUE];

                cell.detalilLabel.text = @"-";
            }
        }
        else if ([resultType isEqualToString:FIELD_ROUTE_SEARCH])
        {
            NSString *route = [cellDict objectForKey:FIELD_SEARCH_RESULT];
            NSDictionary *routeDetailDictionay = [NSDictionary dictionaryWithDictionary:[self getDetailsforRoute:route]];
            cell.contentLabel.text = route;
            [cell.searchResultTypeImage setImage:[UIImage imageNamed:@"ico_busType.png"]];
            cell.detalilLabel.text = [NSString stringWithFormat:@"Source: %@\nDestincation: %@",[[routeDetailDictionay objectForKey:FIELD_STAGE_SOURCE] capitalizedString],[[routeDetailDictionay objectForKey:FIELD_STAGE_DESTINATION] capitalizedString]];
            cell.plotButton.tag = indexPath.row;
            [cell.plotButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.autoButton setHidden:TRUE];
            [cell.contentView setUserInteractionEnabled: NO];
        }
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == keyTable)
    {
        NSDictionary *cellDict = [keyResult objectAtIndex:indexPath.row];
        NSString *resultType = [cellDict objectForKey:FIELD_SEARCH_TYPE];
        if ([resultType isEqualToString:FIELD_STAGE_SEARCH])
        {
            NSString *stage = [cellDict objectForKey:FIELD_SEARCH_RESULT];
            [self onStage:stage];
        }
        else if ([resultType isEqualToString:FIELD_ROUTE_SEARCH])
        {
            NSString *route = [cellDict objectForKey:FIELD_SEARCH_RESULT];
            [self onRoute:route];
        }
    }
}

-(BOOL)isLocationAvailableforStage:(NSString *)stage
{
    for (NSDictionary *dict in stageDetails) {
        NSString *dictValue = [[dict valueForKey:@"fields"] valueForKey:@"display_name"];
        id latitude = [[dict valueForKey:@"fields"] valueForKey:@"latitude"];
        id longitude = [[dict valueForKey:@"fields"] valueForKey:@"longitude"];
        
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]] && ![latitude isMemberOfClass:[NSNull class]] && ![longitude isMemberOfClass:[NSNull class]]) {
            return YES;
        }
    }
    return NO;
}

-(NSMutableDictionary *)getDetailsForStage:(NSString *)stage
{
    NSMutableDictionary *Marker;
    for (NSDictionary *dict in stageDetails) {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[NSMutableDictionary alloc] init];
            [Marker setValue:stage forKey:@"stage"];
            if (![[childDict valueForKey:@"importance"] isKindOfClass:[NSNull class]])
            {
                id importance = [childDict valueForKey:@"importance"];
                [Marker setValue:importance forKey:@"importance"];
            }
            if (![[childDict valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
            {
                id is_terminus = [childDict valueForKey:@"is_terminus"];
                [Marker setValue:is_terminus forKey:@"is_terminus"];
            }
        }
    }
    return Marker;
}

-(NSDictionary *)getDetailsforRoute:(NSString *)route
{
    for (NSDictionary *dict in routeInfo) {
        if ([[[dict objectForKey:FIELD_STAGE_NUMBER] uppercaseString] isEqual:[route uppercaseString]]) {
            return dict;
        }
    }
    return nil;
}

-(IBAction)onBack:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
    
}

-(IBAction)onAutoButton:(UIButton *)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SearchDetailsCell *cell = (SearchDetailsCell*)[keyTable cellForRowAtIndexPath:myIP];

    MapPoint *stageMapPoint = [self getLocationMarkerForStage:cell.contentLabel.text];

    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[AutoViewController class]]) {
            AutoViewController *autoViewController = (AutoViewController *)vc;
            if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
            {
                autoViewController.destinationPoint = autoViewController.sourcePoint;
                autoViewController.sourcePoint = stageMapPoint;
            }
            else
            {
                autoViewController.sourcePoint = stageMapPoint;
            }
            
            [autoViewController updateLocationDetails];
            
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:autoViewController animated:NO];
            return;
        }
    }
    AutoViewController *autoViewController = [[mSingleton getViewControllerManagerInstance] getAutoViewController];
    if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
    {
        autoViewController.destinationPoint = autoViewController.sourcePoint;
        autoViewController.sourcePoint = stageMapPoint;
    }
    else
    {
        autoViewController.sourcePoint = stageMapPoint;
    }
    
    [autoViewController updateLocationDetails];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:autoViewController animated:NO];
}

-(IBAction)onMapButton:(UIButton *)sender
{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SearchDetailsCell *cell = (SearchDetailsCell*)[keyTable cellForRowAtIndexPath:myIP];
    NSDictionary *requiredDict = nil;
    for (NSDictionary *dict in keyResult)
    {
        NSString *resultType = [dict objectForKey:FIELD_SEARCH_TYPE];
        if ([resultType isEqualToString:FIELD_ROUTE_SEARCH] || [resultType isEqualToString:FIELD_STAGE_SEARCH] )
        {
            NSString *result = [dict objectForKey:FIELD_SEARCH_RESULT];
            if ([result isEqualToString:cell.contentLabel.text])
            {
                requiredDict = dict;
                break;
            }
        }
        else
        {
            MapPoint *details = [dict objectForKey:FIELD_SEARCH_RESULT];
            if ([details.name isEqualToString:cell.contentLabel.text])
            {
                requiredDict = dict;
                break;
            }
        }
    }
    
    if (requiredDict)
    {
        if ([[requiredDict objectForKey:FIELD_SEARCH_TYPE] isEqualToString:FIELD_ROUTE_SEARCH])
        {
            NSString *route = [requiredDict objectForKey:FIELD_SEARCH_RESULT];
            NSDictionary *routeDetailDictionay = [NSDictionary dictionaryWithDictionary:[self getDetailsforRoute:route]];
            [self onMapforRoute:routeDetailDictionay];
        }
        else if([[requiredDict objectForKey:FIELD_SEARCH_TYPE] isEqualToString:FIELD_STAGE_SEARCH])
        {
            NSString *stage = [requiredDict objectForKey:FIELD_SEARCH_RESULT];
            [self onMapforStage:stage];
        }
        else
        {
            MapPoint *details = [requiredDict objectForKey:FIELD_SEARCH_RESULT];
            [self onMapforPlace:details];
        }
    }
}

-(void)onMapforPlace:(MapPoint *)point
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    [mapViewController plotPlace:point];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
}

-(void)onMapforRoute:(NSDictionary *)routedetail
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    NSMutableArray *pathArray = [[NSMutableArray alloc] init];
    for (NSString *path in [routedetail objectForKey:FIELD_STAGE_ROUTE]) {
        if ([self isLocationAvailableforStage:path]) {
            [pathArray addObject:[self getLocationDetailsForStage:path]];
        }
    }
    [mapViewController plotRoute:pathArray];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
}

-(void)onMapforStage:(NSString *)stage
{
    NSMutableDictionary *stageMarker = [self getLocationDetailsForStage:stage];
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    
    [mapViewController plotStage:stageMarker];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
    
}

-(NSMutableDictionary *)getLocationDetailsForStage:(NSString *)stage
{
    NSMutableDictionary *Marker;
    for (NSDictionary *dict in stageDetails) {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[NSMutableDictionary alloc] init];
            [Marker setValue:stage forKey:@"stage"];
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                id latitude = [childDict valueForKey:@"latitude"];
                [Marker setValue:latitude forKey:@"latitude"];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                id longitude = [childDict valueForKey:@"longitude"];
                [Marker setValue:longitude forKey:@"longitude"];
            }
            if (![[childDict valueForKey:@"importance"] isKindOfClass:[NSNull class]])
            {
                id importance = [childDict valueForKey:@"importance"];
                [Marker setValue:importance forKey:@"importance"];
            }
            if (![[childDict valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
            {
                id is_terminus = [childDict valueForKey:@"is_terminus"];
                [Marker setValue:is_terminus forKey:@"is_terminus"];
            }
        }
        
    }
    return Marker;
}

-(MapPoint *)getLocationMarkerForStage:(NSString *)stage
{
    MapPoint *Marker;
    
    CLLocationDegrees latitude;
	CLLocationDegrees longitude;
    for (NSDictionary *dict in stageDetails)
    {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[MapPoint alloc] init];
            Marker.name = stage;
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                latitude = [[childDict valueForKey:@"latitude"] doubleValue];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                longitude = [[childDict valueForKey:@"longitude"] doubleValue];
            }
        }
    }
    Marker.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    return Marker;
}

@end
