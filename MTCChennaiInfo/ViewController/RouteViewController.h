//
//  RouteViewController.h
//  MTCChennaiInfo
//
//  Created by Gautham on 20/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KeyLegendViewController;
@class MRTSViewController;

@interface RouteViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    NSMutableArray *allRoutes;
    NSMutableArray *routeInfo;
    NSMutableArray *filteredListContent;
    NSMutableArray *stageDetails;
    
    IBOutlet UITextField *routeSearchBox;
    IBOutlet UITableView *routeSearchBoxAssistTable;
    IBOutlet UITableView *routeSearchDetailsTable;
    IBOutlet UITableView *routeSearchPathTable;
    IBOutlet UILabel *noMatchLabel;
       
    IBOutlet UIButton *mapButton;
    IBOutlet UIButton *busServicesButton;

}

@property(nonatomic, strong) NSMutableArray *allRoutes;
@property(nonatomic, strong) NSMutableArray *routeInfo;
@property(nonatomic, strong) NSMutableArray *filteredListContent;
@property(nonatomic, strong) NSMutableArray *stageDetails;

@property(nonatomic, strong) UITextField *routeSearchBox;
@property(nonatomic, strong) UITableView *routeSearchBoxAssistTable;
@property(nonatomic, strong) UITableView *routeSearchDetailsTable;
@property(nonatomic, strong) UITableView *routeSearchPathTable;
@property(nonatomic, strong) UILabel *noMatchLabel;

@property(nonatomic, strong) UIButton *busServicesButton;

-(IBAction)onHome:(id)sender;
-(IBAction)onSearch:(id)sender;
-(IBAction)onMap:(id)sender;
-(IBAction)onMRTS:(id)sender;

-(void)updateDetailsTableForRoute:(NSString *)route;

-(IBAction)onBusServices:(id)sender;

@end
