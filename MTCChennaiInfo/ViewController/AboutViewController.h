//
//  AboutViewController.h
//  MTCChennaiInfo
//
//  Created by Gautham on 21/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
{
    IBOutlet UILabel *versionlabel;
}

-(IBAction)onBack:(id)sender;

@end
