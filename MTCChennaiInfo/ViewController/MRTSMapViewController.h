//
//  MRTSMapViewController.h
//  MTCChennaiInfo
//
//  Created by Gautham on 29/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRTSMapViewController : UIViewController
{
    IBOutlet UIScrollView *baseScrollView;
    UIImageView *imageMapView;
    IBOutlet UIButton *backButton;
}

@property(nonatomic, strong) UIScrollView *baseScrollView;
@property(nonatomic, strong) UIImageView *imageMapView;
@property(nonatomic, strong) UIButton *backButton;

-(IBAction)onBackButton:(id)sender;

@end
