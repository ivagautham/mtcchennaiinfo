//
//  StageViewController.h
//  MTCChennaiInfo
//
//  Created by Gautham on 20/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class KeyLegendViewController;
@class MRTSViewController;

typedef enum {
	ShowResultNone = 0,
	ShowResultPerfect,
	ShowResultAlternate,
    ShowResultShowDestination,
    ShowResultDimDestination,
    ShowResultHideDestination,
    ShowResultHideResponder,
    ShowResultShowSpinner,
    ShowResultHideSpinner,
    ShowResultDisableResponder,
    ShowResultEnableResponder,
    ShowResultPlaces
} ShowResultType;

@interface StageViewController : UIViewController<CLLocationManagerDelegate,UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    NSMutableArray *allStages;
    NSMutableArray *stageDetails;
    NSMutableArray *stageBuses;
    NSMutableArray *routeInfo;
    NSMutableArray *filteredListContent;
    
    IBOutlet UITextField *sourceSearchBox;
    IBOutlet UITextField *destinationSearchBox;
    IBOutlet UITableView *searchAssistTable;
    IBOutlet UITableView *SearchDetailsTable;
    
    IBOutlet UIImageView *sourceMagGlass;
    IBOutlet UIImageView *destinationMagGlass;
    
    IBOutlet UIButton *sourceCurrentLocation;
    IBOutlet UIButton *swapLocations;
    
    IBOutlet UILabel *noMatchLabel;
    IBOutlet UILabel *noMatchLabel2;
    IBOutlet UIButton *searchPlacesButton;

    IBOutlet UIView *alternateRouteView;
    IBOutlet UILabel *alternateConnectionLabel;
    IBOutlet UITableView *altenateRouteTable;
    
    IBOutlet UIActivityIndicatorView *loadingSpinner;
    
    NSMutableArray *FinalSrc;
    NSMutableArray *FinalDest;
        
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentCentre;
    
    IBOutlet UIButton *mapButton;
    IBOutlet UIButton *busServicesButton;
    
    IBOutlet UIButton *autoButton;
}

@property(nonatomic, strong) NSMutableArray *allStages;
@property(nonatomic, strong) NSMutableArray *stageDetails;
@property(nonatomic, strong) NSMutableArray *stageBuses;
@property(nonatomic, strong) NSMutableArray *routeInfo;
@property(nonatomic, strong) NSMutableArray *filteredListContent;

@property(nonatomic, strong)  UITextField *sourceSearchBox;
@property(nonatomic, strong)  UITextField *destinationSearchBox;
@property(nonatomic, strong)  UITableView *searchAssistTable;
@property(nonatomic, strong)  UITableView *SearchDetailsTable;
@property(nonatomic, strong)  UILabel *noMatchLabel;
@property(nonatomic, strong)  UILabel *noMatchLabel2;
@property(nonatomic, strong)  UIButton *searchPlacesButton;

@property(nonatomic, strong) UIImageView *sourceMagGlass;
@property(nonatomic, strong) UIImageView *destinationMagGlass;

@property(nonatomic, strong) UIButton *sourceCurrentLocation;
@property(nonatomic, strong) UIButton *swapLocations;

@property(nonatomic, strong) UIView *alternateRouteView;
@property(nonatomic, strong) UILabel *alternateConnectionLabel;
@property(nonatomic, strong) UITableView *altenateRouteTable;

@property(nonatomic, strong) UIActivityIndicatorView *loadingSpinner;

@property(nonatomic, strong) NSMutableArray *FinalSrc;
@property(nonatomic, strong) NSMutableArray *FinalDest;

@property(nonatomic, strong) UIButton *busServicesButton;

@property(nonatomic, strong) UIButton *autoButton;

-(IBAction)onHome:(id)sender;
-(IBAction)onSearch:(id)sender;
-(IBAction)onMap:(id)sender;
-(IBAction)onMRTS:(id)sender;
-(IBAction)onAutoButton:(id)sender;
-(IBAction)onSourceCurrentLocation:(id)sender;
-(IBAction)onSwapLocations:(id)sender;
-(void)updateDetailsTableForSource:(NSString *)source andDestination:(NSString *)destincation;
-(IBAction)searchPlace:(id)sender;

-(IBAction)onBusServices:(id)sender;
@end
