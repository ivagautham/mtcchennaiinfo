//
//  MRTSMapViewController.m
//  MTCChennaiInfo
//
//  Created by Gautham on 29/10/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "MRTSMapViewController.h"
#import "Singleton.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@interface MRTSMapViewController ()

@end

@implementation MRTSMapViewController
@synthesize baseScrollView, imageMapView, backButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIImage *image = [UIImage imageNamed:@"ChennaiMRTSmap.png"];
    self.imageMapView = [[UIImageView alloc] initWithImage:image];
    self.imageMapView.frame = (CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=image.size};
    [self.baseScrollView addSubview:self.imageMapView];
    
    // Tell the scroll view the size of the contents
    self.baseScrollView.contentSize = image.size;
    
    [self.view setBackgroundColor:mSingleton.bgColor];
    [self.imageMapView removeFromSuperview];
    [self.baseScrollView addSubview:self.imageMapView];
    [self.baseScrollView setScrollEnabled:YES];
    [self.baseScrollView setClipsToBounds:YES];
    [self.baseScrollView showsHorizontalScrollIndicator];
    [self.baseScrollView showsVerticalScrollIndicator];
    [self.baseScrollView setContentSize:CGSizeMake(800, 863)];
    [self.imageMapView setContentMode:UIViewContentModeScaleAspectFit];
    
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [self.baseScrollView addGestureRecognizer:doubleTapRecognizer];
    
    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
    twoFingerTapRecognizer.numberOfTapsRequired = 1;
    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
    [self.baseScrollView addGestureRecognizer:twoFingerTapRecognizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (IS_IPHONE_5)
        baseScrollView.frame = CGRectMake(0, 0, 320, 568);
    else
           baseScrollView.frame = CGRectMake(0, 0, 320, 480);

    
    CGRect scrollViewFrame = self.baseScrollView.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / self.baseScrollView.contentSize.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / self.baseScrollView.contentSize.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    
    self.baseScrollView.minimumZoomScale = minScale;
    self.baseScrollView.maximumZoomScale = 2.0;
    self.baseScrollView.zoomScale = minScale * 2;
    
    [self centerScrollViewContents];
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that we want to zoom
    return self.imageMapView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so we need to re-center the contents
    [self centerScrollViewContents];
}

- (void)centerScrollViewContents {
    CGSize boundsSize = self.baseScrollView.bounds.size;
    CGRect contentsFrame = self.imageMapView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.imageMapView.frame = contentsFrame;
}

- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
    // Get the location within the image view where we tapped
    CGPoint pointInView = [recognizer locationInView:self.imageMapView];
    
    // Get a zoom scale that's zoomed in slightly, capped at the maximum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.baseScrollView.zoomScale * 1.5f;
    newZoomScale = MIN(newZoomScale, self.baseScrollView.maximumZoomScale);
    
    // Figure out the rect we want to zoom to, then zoom to it
    CGSize scrollViewSize = self.baseScrollView.bounds.size;
    
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    
    [self.baseScrollView zoomToRect:rectToZoomTo animated:YES];
}

- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer {
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.baseScrollView.zoomScale / 1.5f;
    newZoomScale = MAX(newZoomScale, self.baseScrollView.minimumZoomScale);
    [self.baseScrollView setZoomScale:newZoomScale animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(IBAction)onBackButton:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
    
}

@end
