//
//  MapViewController.m
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 20-8-13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "MapViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Singleton.h"
#import "MRTSViewController.h"
#import "StageViewController.h"
#import "RouteViewController.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@interface MapViewController ()
{
    NSMutableDictionary *currentLocationCoordinates;
    NSMutableArray *pointArray;
    BOOL allStages;
}

@end

@implementation MapViewController
@synthesize prevFaceButton;
@synthesize mapView;
@synthesize firstLaunch;
@synthesize stageDetails;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.layer.cornerRadius = 10;
        UISwipeGestureRecognizer *swipeUpforHome = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onHome:)];
        swipeUpforHome.direction = UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer: swipeUpforHome];
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (allStages) {
        // [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(plotAllStages) object:nil];
        allStages = FALSE;
        [locationManager stopUpdatingLocation];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [locationManager startUpdatingLocation];
    if (IS_IPHONE_5)
        mapView.frame = CGRectMake(0, 0, 320, 568);
    else
        mapView.frame = CGRectMake(0, 0, 320, 480);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    stageDetails = [mSingleton.stageDetails mutableCopy];
    self.mapView.delegate = self;
    [self.mapView setShowsUserLocation:YES];
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [locationManager startUpdatingLocation];
    //Set the first launch instance variable to allow the map to zoom on the user location when first launched.
    firstLaunch=YES;
    
    [prevFaceButton setFrame:CGRectMake(133, 0, 54, 54)];
    [self.mapView addSubview:prevFaceButton];
    [self.mapView bringSubviewToFront:prevFaceButton];
    allStages = FALSE;
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:mSingleton.bgColor];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)plotPlace:(MapPoint *)point
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    [mapView addAnnotation:point];
    [mapView selectAnnotation:point animated:YES];
    [self.mapView addSubview:prevFaceButton];
    [self.mapView bringSubviewToFront:prevFaceButton];
}

-(void)plotStage:(NSMutableDictionary *)path
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView addSubview:prevFaceButton];
    [self.mapView bringSubviewToFront:prevFaceButton];
    
    if([path count] == 0)
        return;
    
    double lat = [[path objectForKey:@"latitude"] doubleValue];
    double lon = [[path objectForKey:@"longitude"] doubleValue];
    NSString *name = [path objectForKey:@"stage"];
    NSString *vicinity = [NSString stringWithFormat:@"Stage Importance:%@",[[path objectForKey:@"importance"] stringValue]];
    CLLocationCoordinate2D coordinates= CLLocationCoordinate2DMake(lat, lon);
    
    if (![[path allKeys] containsObject:@"importance"])
        vicinity = @"";

    MapPoint *placeObject = [[MapPoint alloc] initWithName:name address:vicinity coordinate:coordinates andtype:MapPointTypeStage];
    [mapView addAnnotation:placeObject];
    [mapView selectAnnotation:placeObject animated:YES];
}

-(void)plotRoute:(NSMutableArray *)path
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView addSubview:prevFaceButton];
    [self.mapView bringSubviewToFront:prevFaceButton];
    
    if(!pointArray)
        pointArray = [[NSMutableArray alloc] init];
    [pointArray removeAllObjects];
    
    if ([path count] == 0)
        return;
    
    for (NSMutableDictionary* dict in path)
    {
        double lat = [[dict objectForKey:@"latitude"] doubleValue];
        double lon = [[dict objectForKey:@"longitude"] doubleValue];
        NSString *name = [dict objectForKey:@"stage"];
        NSString *vicinity = [NSString stringWithFormat:@"Stage Importance:%@",[[dict objectForKey:@"importance"] stringValue]];
        CLLocationCoordinate2D coordinates= CLLocationCoordinate2DMake(lat, lon);
        
        MapPoint *placeObject = [[MapPoint alloc] initWithName:name address:vicinity coordinate:coordinates andtype:MapPointTypeRoute];
        [mapView addAnnotation:placeObject];
        [mapView selectAnnotation:placeObject animated:NO];
        [pointArray addObject:placeObject];
    }
    [self centerMap];
    
    if ([mSingleton isConnectedToInternet])
        [self performSelectorInBackground:@selector(showDirections:) withObject:pointArray];
    
}

-(void)showDirections:(NSMutableArray *)routeArray
{
    NSMutableArray *overlays = [[NSMutableArray alloc] init];
    for (int i=0 ; i < ([routeArray count] -1); i++)
    {
        MapPoint *from = [routeArray objectAtIndex:i];
        MapPoint *to = [routeArray objectAtIndex:i+1];
        [self showRouteFrom:from to:to inArray:overlays];
    }
    [self.mapView performSelectorOnMainThread:@selector(addOverlays:) withObject:overlays waitUntilDone:FALSE];
}

-(void)plotAllStages
{
    allStages = TRUE;
    for (NSDictionary *dict in stageDetails) {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        if (TRUE)
        {
            BOOL condition = !([[childDict valueForKey:@"display_name"] isKindOfClass:[NSNull class]]);
            condition = condition && !([[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]]);
            condition = condition && !([[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]]);
            condition = condition && !([[childDict valueForKey:@"importance"] isKindOfClass:[NSNull class]]);
            
            if (condition)
            {
                double lat = [[childDict objectForKey:@"latitude"] doubleValue];
                double lon = [[childDict objectForKey:@"longitude"] doubleValue];
                NSString *name = [childDict objectForKey:@"display_name"];
                NSString *vicinity = [NSString stringWithFormat:@"Stage Importance:%@",[[childDict objectForKey:@"importance"] stringValue]];
                CLLocationCoordinate2D coordinates= CLLocationCoordinate2DMake(lat, lon);
                MapPoint *placeObject = [[MapPoint alloc] initWithName:name address:vicinity coordinate:coordinates andtype:MapPointTypeStage];
                [mapView addAnnotation:placeObject];
            }
        }
    }
}

-(void)plotSorce:(NSMutableDictionary *)source andDestination:(NSMutableDictionary *)destination
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    if (source && source.count > 0)
    {
        double lat = [[source objectForKey:@"latitude"] doubleValue];
        double lon = [[source objectForKey:@"longitude"] doubleValue];
        if (lat > 0 || lon > 0)
        {
            NSString *name = [source objectForKey:@"stage"];
            NSString *vicinity = [NSString stringWithFormat:@"Stage Importance:%@",[[source objectForKey:@"importance"] stringValue]];
            CLLocationCoordinate2D coordinates= CLLocationCoordinate2DMake(lat, lon);
            
            MapPoint *placeObject = [[MapPoint alloc] initWithName:name address:vicinity coordinate:coordinates andtype:MapPointTypeStage];
            [mapView addAnnotation:placeObject];
            [mapView selectAnnotation:placeObject animated:YES];
        }
    }
    if (destination && destination.count > 0)
    {
        double lat = [[destination objectForKey:@"latitude"] doubleValue];
        double lon = [[destination objectForKey:@"longitude"] doubleValue];
        if (lat > 0 || lon > 0)
        {
            NSString *name = [destination objectForKey:@"stage"];
            NSString *vicinity = [NSString stringWithFormat:@"Stage Importance:%@",[[destination objectForKey:@"importance"] stringValue]];
            CLLocationCoordinate2D coordinates= CLLocationCoordinate2DMake(lat, lon);
            
            MapPoint *placeObject = [[MapPoint alloc] initWithName:name address:vicinity coordinate:coordinates andtype:MapPointTypeStage];
            [mapView addAnnotation:placeObject];
            [mapView selectAnnotation:placeObject animated:YES];
        }
    }
    
    [self.mapView addSubview:prevFaceButton];
    [self.mapView bringSubviewToFront:prevFaceButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onHome:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromBottom;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
   /*
    for (int idx = ([self.navigationController.viewControllers count] - 1); idx > 0; idx --)
    {
        UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:idx];
        if ([vc isMemberOfClass:[MRTSViewController class]]) {
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
        else if ([vc isMemberOfClass:[StageViewController class]]) {
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
        else if ([vc isMemberOfClass:[RouteViewController class]]) {
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    [self.navigationController popToRootViewControllerAnimated:NO];
    */
    
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    /*CLLocationCoordinate2D centre = [mv centerCoordinate];
     MKCoordinateRegion region;
     if (firstLaunch) {
     CLLocationCoordinate2D coord;
     if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
     {
     currentCentre.latitude = 13.052413900000000000;
     currentCentre.longitude = 80.250824599999990000;
     coord = currentCentre;
     currenDist = 50000;
     }
     else
     {
     coord = locationManager.location.coordinate;
     }
     region = MKCoordinateRegionMakeWithDistance(coord,3000,3000);
     firstLaunch=NO;
     }
     else {
     if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized)
     {
     currentCentre.latitude = 13.052413900000000000;
     currentCentre.longitude = 80.250824599999990000;
     centre = currentCentre;
     //currenDist = 50000;
     }
     //Set the center point to the visible region of the map and change the radius to match the search radius passed to the Google query string.
     region = MKCoordinateRegionMakeWithDistance(centre,currenDist,currenDist);
     }
     
     [mv setRegion:region animated:YES];
     */
    
    if(!allStages)
    {
        MKAnnotationView *annotationView = [views objectAtIndex:0];
        id <MKAnnotation> mp = [annotationView annotation];
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 3250,3250);
        [mv setRegion:region animated:FALSE];
        allStages = TRUE;
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    //Define our reuse indentifier.
    static NSString *identifier = @"MapPoint";
    if ([annotation isKindOfClass:[MapPoint class]]) {
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        } else {
            annotationView.annotation = annotation;
        }
        
        MapPoint *mp = (MapPoint *)annotation;
        if (mp.type == MapPointTypePlace)
            annotationView.pinColor = MKPinAnnotationColorRed;
        else if (mp.type == MapPointTypeStage)
            annotationView.pinColor = MKPinAnnotationColorGreen;
        else if (mp.type == MapPointTypeRoute)
            annotationView.pinColor = MKPinAnnotationColorPurple;
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.animatesDrop = YES;
        annotationView.draggable = NO;
        
        return annotationView;
    }
    
    return nil;
}


- (NSMutableArray *)decodePolyLine: (NSMutableString *)encoded
{
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\" options:NSLiteralSearch range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len)
    {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        //printf("[%f,", [latitude doubleValue]);
        //printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:loc];
    }
    return array;
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    NSError* error = nil;
    NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSASCIIStringEncoding error:&error];
    
    if (error)
        return nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"points:\\\"([^\\\"]*)\\\"" options:0 error:NULL];
    NSTextCheckingResult *match = [regex firstMatchInString:apiResponse options:0 range:NSMakeRange(0, [apiResponse length])];
    NSString *encodedPoints = [apiResponse substringWithRange:[match rangeAtIndex:1]];
    return [self decodePolyLine:[encodedPoints mutableCopy]];
}

-(void) centerMap
{
    MKCoordinateRegion region;
    CLLocationDegrees maxLat = -90.0;
    CLLocationDegrees maxLon = -180.0;
    CLLocationDegrees minLat = 90.0;
    CLLocationDegrees minLon = 180.0;
    for(int idx = 0; idx < pointArray.count; idx++)
    {
        MapPoint *mp = [pointArray objectAtIndex:idx];
        double lat = mp.coordinate.latitude;
        double lon = mp.coordinate.longitude;
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(lat, lon);
        if(coord.latitude > maxLat)
            maxLat = coord.latitude;
        if(coord.latitude < minLat)
            minLat = coord.latitude;
        if(coord.longitude > maxLon)
            maxLon = coord.longitude;
        if(coord.longitude < minLon)
            minLon = coord.longitude;
    }
    region.center.latitude     = (maxLat + minLat) / 2.0;
    region.center.longitude    = (maxLon + minLon) / 2.0;
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    
    region.span.latitudeDelta  = ((maxLat - minLat)<=0.0)?0.1:(maxLat - minLat);
    region.span.longitudeDelta = ((maxLon - minLon)<0.0)?0.1:(maxLon - minLon);
    if (pointArray.count > 0)
        [mapView setRegion:region animated:YES];
}

-(void) showRouteFrom:(MapPoint*)f to:(MapPoint*)t inArray:(NSMutableArray *)overlays
{
    if (!(f && t))
        return;
    
    routes = [self calculateRoutesFrom:f.coordinate to:t.coordinate];
    NSInteger numberOfSteps = routes.count;
    
    CLLocationCoordinate2D coordinates[numberOfSteps];
    for (NSInteger index = 0; index < numberOfSteps; index++)
    {
        CLLocation *location = [routes objectAtIndex:index];
        CLLocationCoordinate2D coordinate = location.coordinate;
        coordinates[index] = coordinate;
    }
    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:coordinates count:numberOfSteps];
    [overlays addObject:polyLine];
    //[self.mapView addOverlay:polyLine level:MKOverlayLevelAboveLabels];
}

#pragma mark MKPolyline delegate functions

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolyline *route = overlay;
        MKPolylineRenderer *routeRenderer = [[MKPolylineRenderer alloc] initWithPolyline:route];
        routeRenderer.strokeColor = [UIColor purpleColor];
        routeRenderer.lineWidth = 5.0;
        return routeRenderer;
    }
    return nil;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor purpleColor];
    polylineView.lineWidth = 5.0;
    return polylineView;
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    if(firstLaunch)
    {
        CLLocationCoordinate2D coord;
        coord.latitude = 13.052413900000000000;
        coord.longitude = 80.250824599999990000;
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 10000,10000);
        [self.mapView setRegion:region animated:FALSE];
        firstLaunch = FALSE;
    }
}

@end
