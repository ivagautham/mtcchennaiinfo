//
//  RouteViewController.m
//  MTCChennaiInfo
//
//  Created by Gautham on 20/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "RouteViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomCellBackground.h"
#import "Constants.h"
#import "RouteDetailsCell.h"
#import "Singleton.h"
#import "SearchViewController.h"
#import "MapViewController.h"
#import "KeyLegendViewController.h"
#import "RoutePathCell.h"
#import "StageViewController.h"
#import "Flurry.h"
#import "MRTSViewController.h"
#import "AutoViewController.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@interface RouteViewController ()
{
    NSMutableDictionary *routeDetailDictionay;
    BOOL didLoadfromResource;
    BOOL isHiding;
}

-(void)loadRoutesFromResource;
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope;
-(void)setTableHidden:(BOOL)flag;
-(NSDictionary *)getDetailsforRoute:(NSString *)route;
@end

@implementation RouteViewController
@synthesize allRoutes,filteredListContent;
@synthesize routeInfo,stageDetails;
@synthesize routeSearchBox,routeSearchBoxAssistTable,routeSearchDetailsTable,routeSearchPathTable,noMatchLabel;
@synthesize busServicesButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        didLoadfromResource = FALSE;
        self.view.layer.cornerRadius = 10;
        UISwipeGestureRecognizer *swipeLeftforKeyLegend = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onMRTS:)];
        swipeLeftforKeyLegend.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer: swipeLeftforKeyLegend];
        
        UISwipeGestureRecognizer *swipeRightforHome = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onHome:)];
        swipeRightforHome.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer: swipeRightforHome];
        
        UISwipeGestureRecognizer *swipeDownforSearch = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSearch:)];
        swipeDownforSearch.direction = UISwipeGestureRecognizerDirectionDown;
        [self.view addGestureRecognizer: swipeDownforSearch];
        
        UISwipeGestureRecognizer *swipeUpforMap = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onAutoButton:)];
        swipeUpforMap.direction = UISwipeGestureRecognizerDirectionUp;
        [self.view addGestureRecognizer: swipeUpforMap];
        
        UITapGestureRecognizer *tapToResignResponder = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignResponder)];
        tapToResignResponder.numberOfTapsRequired = 1;
        [self.view addGestureRecognizer: tapToResignResponder];
        [tapToResignResponder setCancelsTouchesInView:NO];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!didLoadfromResource)
        [self loadRoutesFromResource];
    
    [routeSearchBoxAssistTable setHidden:TRUE];
    [routeSearchPathTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [routeSearchBox resignFirstResponder];
    if (! routeSearchBoxAssistTable.hidden)
        [self setTableHidden:TRUE];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!didLoadfromResource)
        [self loadRoutesFromResource];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_SHOW_QOS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qosChanged:) name:NOTIFICATION_HIDE_QOS object:nil];
    
    routeSearchBox.layer.cornerRadius = 5;
    routeSearchBox.layer.borderWidth = 2;
    routeSearchBox.layer.borderColor = mSingleton.elementColor1.CGColor;
    
    //    mapButton.layer.cornerRadius = 5;
    //    mapButton.layer.borderWidth = 2;
    //    mapButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    //
    //    busServicesButton.layer.cornerRadius = 5;
    //    busServicesButton.layer.borderWidth = 2;
    //    busServicesButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    [busServicesButton setHidden:FALSE];
    
    [routeSearchBox setDelegate:self];
    CustomCellBackground * backgroundView = [[CustomCellBackground alloc] init];
    [routeSearchBox addSubview: backgroundView];
    [routeSearchBox sendSubviewToBack: backgroundView];
    
    routeSearchBoxAssistTable.layer.cornerRadius = 5;
    routeSearchBoxAssistTable.layer.borderWidth = 1;
    routeSearchBoxAssistTable.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    //    routeSearchDetailsTable.layer.cornerRadius = 10;
    //    routeSearchDetailsTable.layer.borderWidth = 2;
    routeSearchDetailsTable.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    //    routeSearchPathTable.layer.cornerRadius = 10;
    //    routeSearchPathTable.layer.borderWidth = 2;
    routeSearchPathTable.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    [routeSearchBoxAssistTable setHidden:TRUE];
    [routeSearchDetailsTable setHidden:TRUE];
    [routeSearchPathTable setHidden:TRUE];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:mSingleton.bgColor];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadRoutesFromResource
{
    [allRoutes removeAllObjects];
    allRoutes = [mSingleton.allRoutes mutableCopy];
    
    
    [routeInfo removeAllObjects];
    routeInfo = [mSingleton.routeInfo mutableCopy];
    
    [stageDetails removeAllObjects];
    stageDetails = [mSingleton.stageDetails mutableCopy];
    
    if (!filteredListContent)
        filteredListContent = [[NSMutableArray alloc] initWithArray:allRoutes copyItems:YES];
    
    
    didLoadfromResource = TRUE;
}

-(IBAction)onHome:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = @"cube";
    transition.subtype = kCATransitionFromLeft;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(IBAction)onSearch:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromBottom;
    
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[SearchViewController class]]) {
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    
    SearchViewController *searchViewController = [[mSingleton getViewControllerManagerInstance] getSearchViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchViewController animated:NO];
}

-(IBAction)onMap:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    NSMutableArray *pathArray = [[NSMutableArray alloc] init];
    if (![[routeDetailDictionay objectForKey:FIELD_STAGE_TYPE] isEqualToString:@"Small Bus"])
    {
        for (NSString *path in [routeDetailDictionay objectForKey:FIELD_STAGE_ROUTE]) {
            if ([self isLocationAvailableforStage:path]) {
                [pathArray addObject:[self getLocationDetailsForStage:path]];
            }
        }
        [mapViewController plotRoute:pathArray];
    }
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
}

-(IBAction)onMRTS:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromRight;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[MRTSViewController class]]) {
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:vc animated:NO];
            return;
        }
    }
    MRTSViewController *MRTSViewController = [[mSingleton getViewControllerManagerInstance] getMRTSViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:MRTSViewController animated:NO];
}

-(IBAction)onBusServices:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    KeyLegendViewController *keyLegendViewController = [[mSingleton getViewControllerManagerInstance] getKeyLegendViewController];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:keyLegendViewController animated:NO];
}

-(IBAction)onMapButton:(UIButton *)sender
{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    RoutePathCell *cell = (RoutePathCell*)[routeSearchPathTable cellForRowAtIndexPath:myIP];
    
    NSMutableDictionary *stageMarker = [self getLocationDetailsForStage:cell.pathLabel.text];
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    
    MapViewController *mapViewController = [[mSingleton getViewControllerManagerInstance] getMapViewController];
    
    [mapViewController plotStage:stageMarker];
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mapViewController animated:NO];
    
}

-(IBAction)onAutoButton:(UIButton *)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.type = @"cube";
    transition.subtype = kCATransitionFromTop;
    
    MapPoint *stageMapPoint = nil;
    if ([sender isMemberOfClass:[UIButton class]])
    {
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        RoutePathCell *cell = (RoutePathCell*)[routeSearchPathTable cellForRowAtIndexPath:myIP];
        stageMapPoint = [self getLocationMarkerForStage:cell.pathLabel.text];
    }
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isMemberOfClass:[AutoViewController class]]) {
            AutoViewController *autoViewController = (AutoViewController *)vc;
            if ([sender isMemberOfClass:[UIButton class]])
            {
                if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
                {
                    autoViewController.destinationPoint = autoViewController.sourcePoint;
                    autoViewController.sourcePoint = stageMapPoint;
                }
                else
                {
                    autoViewController.sourcePoint = stageMapPoint;
                }
                
                [autoViewController updateLocationDetails];
            }
            [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            [self.navigationController popToViewController:autoViewController animated:NO];
            return;
        }
    }
    AutoViewController *autoViewController = [[mSingleton getViewControllerManagerInstance] getAutoViewController];
    if ([sender isMemberOfClass:[UIButton class]])
    {
        if(autoViewController.sourcePoint && !autoViewController.destinationPoint)
        {
            autoViewController.destinationPoint = autoViewController.sourcePoint;
            autoViewController.sourcePoint = stageMapPoint;
        }
        else
        {
            autoViewController.sourcePoint = stageMapPoint;
        }
        
        [autoViewController updateLocationDetails];
    }
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    //[self.navigationController pushViewController:autoViewController animated:NO];
    [self.navigationController pushViewController:autoViewController animated:NO];
}

-(NSMutableDictionary *)getLocationDetailsForStage:(NSString *)stage
{
    NSMutableDictionary *Marker;
    for (NSDictionary *dict in stageDetails) {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[NSMutableDictionary alloc] init];
            [Marker setValue:stage forKey:@"stage"];
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                id latitude = [childDict valueForKey:@"latitude"];
                [Marker setValue:latitude forKey:@"latitude"];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                id longitude = [childDict valueForKey:@"longitude"];
                [Marker setValue:longitude forKey:@"longitude"];
            }
            if (![[childDict valueForKey:@"importance"] isKindOfClass:[NSNull class]])
            {
                id importance = [childDict valueForKey:@"importance"];
                [Marker setValue:importance forKey:@"importance"];
            }
            if (![[childDict valueForKey:@"is_terminus"] isKindOfClass:[NSNull class]])
            {
                id is_terminus = [childDict valueForKey:@"is_terminus"];
                [Marker setValue:is_terminus forKey:@"is_terminus"];
            }
        }
        
    }
    return Marker;
}

-(MapPoint *)getLocationMarkerForStage:(NSString *)stage
{
    MapPoint *Marker;
    
    CLLocationDegrees latitude;
	CLLocationDegrees longitude;
    for (NSDictionary *dict in stageDetails)
    {
        NSDictionary *childDict = [dict valueForKey:@"fields"];
        NSString *dictValue = [childDict valueForKey:@"display_name"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]])
        {
            Marker = [[MapPoint alloc] init];
            Marker.name = stage;
            if (![[childDict valueForKey:@"latitude"] isKindOfClass:[NSNull class]])
            {
                latitude = [[childDict valueForKey:@"latitude"] doubleValue];
            }
            if (![[childDict valueForKey:@"longitude"] isKindOfClass:[NSNull class]])
            {
                longitude = [[childDict valueForKey:@"longitude"] doubleValue];
            }
        }
    }
    Marker.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    return Marker;
}

-(void)resignResponder
{
    [routeSearchBox resignFirstResponder];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
    for (NSString *section in self.allRoutes) {
        if ([[section uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound)
        {
            [self.filteredListContent addObject:section];
        }
    }
    
    if([self.filteredListContent count] == 0)
    {
        [routeSearchBoxAssistTable setHidden:TRUE];
        [noMatchLabel setHidden:FALSE];
    }
    else
        if (routeSearchBoxAssistTable.hidden)
            [self setTableHidden:FALSE];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSString *string in self.filteredListContent)
    {
        if ([string hasPrefix:searchText])
            [tempArray addObject:string];
    }
    
    [self.filteredListContent removeObjectsInArray:tempArray];
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [tempArray count])];
    [self.filteredListContent insertObjects:tempArray atIndexes:indexSet];
    //    for (NSString *string in tempArray)
    //        [self.filteredListContent insertObject:string atIndex:0];
    
    [self.routeSearchBoxAssistTable reloadData];
}

-(void)quickHide:(BOOL)flag
{
    if (!isHiding)
    {
        [routeSearchBoxAssistTable setHidden:TRUE];
        [noMatchLabel setHidden:!flag];
    }
}

-(void)setTableHidden:(BOOL)flag
{
    isHiding = TRUE;
    [routeSearchBoxAssistTable setHidden:FALSE];
    NSTimeInterval animationDuration = 0.5;/* determine length of animation */;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    if (flag)
        routeSearchBoxAssistTable.frame = CGRectMake(routeSearchBoxAssistTable.frame.origin.x, routeSearchBoxAssistTable.frame.origin.y, routeSearchBoxAssistTable.frame.size.width, 0);
    else
        routeSearchBoxAssistTable.frame = CGRectMake(routeSearchBoxAssistTable.frame.origin.x, routeSearchBoxAssistTable.frame.origin.y, routeSearchBoxAssistTable.frame.size.width, routeSearchBoxAssistTable.frame.size.height);
    
    [UIView commitAnimations];
    [noMatchLabel setHidden:!flag];
    isHiding = FALSE;
}

-(NSDictionary *)getDetailsforRoute:(NSString *)route
{
    for (NSDictionary *dict in routeInfo) {
        if ([[[dict objectForKey:FIELD_STAGE_NUMBER] uppercaseString] isEqual:[route uppercaseString]]) {
            return dict;
        }
    }
    return nil;
}

-(void)updateDetailsTableForRoute:(NSString *)route
{
    NSDictionary *articleParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     route, @"Route",
     nil];
    
    [Flurry logEvent:@"Searched_Route" withParameters:articleParams];
    
    if (!didLoadfromResource)
        [self loadRoutesFromResource];
    
    routeSearchBox.text = route;
    if(!routeDetailDictionay)
        routeDetailDictionay = [[NSMutableDictionary alloc] init];
    [routeDetailDictionay removeAllObjects];
    
    if (!routeSearchBoxAssistTable.hidden)
        [self quickHide:TRUE];
    
    [routeDetailDictionay setDictionary:[self getDetailsforRoute:route]];
    if(routeDetailDictionay && [routeDetailDictionay count] > 0)
    {
        [noMatchLabel setHidden:TRUE];
        [self.routeSearchDetailsTable setHidden:FALSE];
        [self.routeSearchPathTable setHidden:FALSE];
        [self.routeSearchDetailsTable reloadData];
        [self.routeSearchPathTable reloadData];
    }
    else
    {
        [noMatchLabel setHidden:FALSE];
        [self.routeSearchDetailsTable setHidden:TRUE];
        [self.routeSearchPathTable setHidden:TRUE];
        [routeSearchBox setFont:[mSingleton getItalicFontOfSize:12]];
        [routeSearchBox setTextColor:[UIColor redColor]];
    }
}

-(BOOL)isLocationAvailableforStage:(NSString *)stage
{
    for (NSDictionary *dict in stageDetails) {
        NSString *dictValue = [[dict valueForKey:@"fields"] valueForKey:@"display_name"];
        id latitude = [[dict valueForKey:@"fields"] valueForKey:@"latitude"];
        id longitude = [[dict valueForKey:@"fields"] valueForKey:@"longitude"];
        if ([[dictValue uppercaseString] isEqual:[stage uppercaseString]] && ![latitude isMemberOfClass:[NSNull class]] && ![longitude isMemberOfClass:[NSNull class]]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark-
#pragma mark tableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == routeSearchBoxAssistTable)
    {
        CGRect tableHeight = routeSearchBoxAssistTable.frame;
        if (IS_IPHONE_5)
        {
            if ([filteredListContent count] < 5)
            {
                tableHeight.size.height = 44 * [filteredListContent count];
            }
            else
            {
                tableHeight.size.height = 220;
                [routeSearchBoxAssistTable flashScrollIndicators];
            }
        }
        else
        {
            if ([filteredListContent count] < 3)
            {
                tableHeight.size.height = 44 * [filteredListContent count];
            }
            else
            {
                tableHeight.size.height = 132;
                [routeSearchBoxAssistTable flashScrollIndicators];
            }
        }
        routeSearchBoxAssistTable.frame = tableHeight;
        return [filteredListContent count];
    }
    else if (tableView == routeSearchDetailsTable && [routeDetailDictionay count] > 0)
        return 2;
    else if (tableView == routeSearchPathTable && [routeDetailDictionay count] > 0 && [[routeDetailDictionay objectForKey:FIELD_STAGE_ROUTE] count] > 0)
    {
        int count = [[routeDetailDictionay objectForKey:FIELD_STAGE_ROUTE] count];
        
        [routeSearchBox setFont:[mSingleton getFontOfSize:14]];
        [routeSearchBox setTextColor:[UIColor blackColor]];
        
        return count;
    }
    else if (tableView == routeSearchPathTable)
    {
        [routeSearchBox setFont:[mSingleton getItalicFontOfSize:12]];
        [routeSearchBox setTextColor:[UIColor redColor]];
        
        return 0;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == routeSearchBoxAssistTable || tableView == routeSearchDetailsTable || tableView == routeSearchPathTable)
        return 1;
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == routeSearchBoxAssistTable)
    {
        static NSString * CellIdentifier = @"routeSearchBoxAssistTableCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        cell.textLabel.text = [filteredListContent objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        
        return cell;
    }
    if (tableView == routeSearchDetailsTable)
    {
        static NSString * CellIdentifier = @"routeSearchDetailsTableCell";
        RouteDetailsCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RouteDetailsCell" owner:self options:nil];
            cell = (RouteDetailsCell *)[nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (indexPath.row == 1) {
            cell.source.text = [[routeDetailDictionay objectForKey:FIELD_STAGE_SOURCE] capitalizedString];
            cell.destination.text = [[routeDetailDictionay objectForKey:FIELD_STAGE_DESTINATION] capitalizedString];
            cell.duration.text =[NSString stringWithFormat:@"%@ mins",[routeDetailDictionay objectForKey:FIELD_STAGE_DURATION]];
            cell.service.text = [routeDetailDictionay objectForKey:FIELD_STAGE_TYPE];
            
            cell.source.backgroundColor = [UIColor clearColor];
            cell.destination.backgroundColor = [UIColor clearColor];
            cell.duration.backgroundColor = [UIColor clearColor];
            cell.service.backgroundColor = [UIColor clearColor];
            
            cell.source.textColor = mSingleton.textColor1;
            cell.destination.textColor = mSingleton.textColor1;
            cell.duration.textColor = mSingleton.textColor1;
            cell.service.textColor = mSingleton.textColor1;
            
            cell.source.font = [mSingleton getBoldFontOfSize:13];
            cell.destination.font = [mSingleton getBoldFontOfSize:13];
            cell.duration.font = [mSingleton getBoldFontOfSize:13];
            cell.service.font = [mSingleton getBoldFontOfSize:13];
            
        }
        return cell;
    }
    if (tableView == routeSearchPathTable)
    {
        static NSString * CellIdentifier = @"routeSearchPathTableCell";
        RoutePathCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RoutePathCell"owner:self options:nil];
            cell = (RoutePathCell *)[topLevelObjects objectAtIndex:0];
        }
        cell.userInteractionEnabled = TRUE;
        cell.pathLabel.alpha = 1.0;
        
        cell.pathLabel.text = [[[routeDetailDictionay objectForKey:FIELD_STAGE_ROUTE] objectAtIndex:indexPath.row] uppercaseString];
        cell.mapitButton.tag = indexPath.row;
        cell.autoButton.tag = indexPath.row;
        if ([self isLocationAvailableforStage:cell.pathLabel.text])
        {
            [cell.mapitButton addTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.autoButton addTarget:self action:@selector(onAutoButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.mapitButton setHidden:FALSE];
            [cell.autoButton setHidden:FALSE];
            [cell.contentView setUserInteractionEnabled: NO];
        }
        else
        {
            if ([[routeDetailDictionay objectForKey:FIELD_STAGE_TYPE] isEqualToString:@"Small Bus"])
            {
                cell.userInteractionEnabled = FALSE;
                cell.pathLabel.alpha = 0.7;
            }
            
            [cell.mapitButton removeTarget:self action:@selector(onMapButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.mapitButton setHidden:TRUE];
            
            [cell.autoButton removeTarget:self action:@selector(onAutoButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.autoButton setHidden:TRUE];
            
        }
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        
        [cell setBackgroundColor:[UIColor whiteColor]];
        return cell;
    }
    
    return nil;
}
#pragma mark-
#pragma mark tableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == routeSearchBoxAssistTable)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        routeSearchBox.text = cell.textLabel.text;
        if (! routeSearchBoxAssistTable.hidden)
            [self setTableHidden:TRUE];
        [self updateDetailsTableForRoute:routeSearchBox.text];
    }
    if(tableView == routeSearchPathTable)
    {
        RoutePathCell * cell = (RoutePathCell *)[tableView cellForRowAtIndexPath:indexPath];
        CATransition *transition = [CATransition animation];
        transition.delegate = self;
        transition.duration = 0.6;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.subtype = kCATransitionFromTop;
        
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isMemberOfClass:[StageViewController class]]) {
                transition.type = @"cameraIris";
                StageViewController *stageViewController = (StageViewController *)vc;
                [stageViewController updateDetailsTableForSource:[cell.pathLabel.text uppercaseString] andDestination:nil];
                [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
                [self.navigationController popToViewController:vc animated:NO];
                return;
            }
        }
        StageViewController *stageViewController = [[mSingleton getViewControllerManagerInstance] getStageViewController];
        transition.type = @"cameraIris";
        [stageViewController updateDetailsTableForSource:[cell.pathLabel.text uppercaseString] andDestination:nil];
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:stageViewController animated:NO];
        
    }
}

#pragma mark-
#pragma mark TextFiled Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == routeSearchBox)
    {
        [routeSearchBox setFont:[mSingleton getFontOfSize:14]];
        [routeSearchBox setTextColor:[UIColor blackColor]];
        
        [noMatchLabel setHidden:TRUE];
        [self.routeSearchDetailsTable setHidden:TRUE];
        [self.routeSearchPathTable setHidden:TRUE];
        [self filterContentForSearchText:routeSearchBox.text scope:nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == routeSearchBox)
        [self filterContentForSearchText:[textField.text stringByReplacingCharactersInRange:range withString:string] scope:nil];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == routeSearchBox)
    {
        [textField resignFirstResponder];
        if (! routeSearchBoxAssistTable.hidden)
            [self setTableHidden:TRUE];
        
        if (([self.filteredListContent count] > 0) && (![self.filteredListContent containsObject:[textField.text uppercaseString]]))
        {
            BOOL didgetPrefix = FALSE;
            for (NSString *string in self.filteredListContent)
            {
                if ([string hasPrefix:textField.text])
                {
                    textField.text = string;
                    didgetPrefix = TRUE;
                    break;
                }
            }
            if (!didgetPrefix)
                textField.text = [self.filteredListContent objectAtIndex:0];
        }
        
        [self updateDetailsTableForRoute:textField.text];
    }
    return YES;
}

@end
