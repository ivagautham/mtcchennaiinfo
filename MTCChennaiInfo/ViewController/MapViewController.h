//
//  MapViewController.h
//  MTCChennaiInfo
//
//  Created by VA Gautham  on 20-8-13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapPoint.h"

@interface MapViewController : UIViewController< MKMapViewDelegate, CLLocationManagerDelegate>
{
    IBOutlet UIButton *prevFaceButton;
    CLLocationCoordinate2D stageMarker;
    BOOL firstLaunch;
    
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentCentre;
    int currenDist;
    NSArray* routes;
    NSArray *stageDetails;
}

@property(nonatomic, strong) UIButton *prevFaceButton;
@property(nonatomic, retain) UIView *customView;
@property(nonatomic) BOOL firstLaunch;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property(nonatomic, strong) NSArray *stageDetails;

-(IBAction)onHome:(id)sender;
-(void)plotPlace:(MapPoint *)point;
-(void)plotStage:(NSMutableDictionary *)path;
-(void)plotRoute:(NSMutableArray *)path;
-(void)plotAllStages;
-(void)plotSorce:(NSMutableDictionary *)source andDestination:(NSMutableDictionary *)destination;

@end
