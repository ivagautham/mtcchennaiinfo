//
//  AppDelegate.m
//  MTCChennaiInfo
//
//  Created by Gautham on 12/08/13.
//  Copyright (c) 2013 Gautham. All rights reserved.
//

#import "AppDelegate.h"
#import "Singleton.h"
#import "HomeViewController.h"
#import "Flurry.h"
#import "Appirater.h"

#define mSingleton 	((Singleton *) [Singleton getSingletonInstance])

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Init Flurry
    [self setupFlurry];
    
    //Init Appirater
    [self setupAppirater];
    
    [[Reachability reachabilityForInternetConnection] startNotifier];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    HomeViewController *homeViewController = [[mSingleton getViewControllerManagerInstance] getHomeViewController];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
	[navController.navigationBar setBarStyle:UIBarStyleBlack];
	self.navigationController = navController;
    [self.navigationController setNavigationBarHidden:TRUE];
    self.window.rootViewController = self.navigationController;
    [self registerNotifications];
    [self.window makeKeyAndVisible];
    return YES;
}

-(void)setupFlurry
{
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"Q5MC53KNMZ4C8NB234XZ"];
}

-(void)setupAppirater
{
    [Appirater setUsesAnimation:YES];
    [Appirater setAppId:@"718101880"];
    [Appirater setDaysUntilPrompt:0.0001];
    [Appirater setUsesUntilPrompt:2];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:2];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [Appirater appEnteredForeground:TRUE];

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_LOAD_STAGE_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_LOAD_ROUTE_SUCCESS object:nil];
}

- (void) localNotificationhandler:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:NOTIFICATION_LOAD_STAGE_SUCCESS])
    {
        NSData * JSONData = [NSJSONSerialization dataWithJSONObject:[notification object] options:kNilOptions error:nil];
        [[mSingleton getFileManagerInstance] writeContents:JSONData ToTextFile:@"Stage.json"];
        NSArray *array = [[mSingleton getWebScrapingManagerInstance] fetchRouteByPlaceDetails];
        NSData * JSONData2 = [NSJSONSerialization dataWithJSONObject:array options:kNilOptions error:nil];
        [[mSingleton getFileManagerInstance] writeContents:JSONData2 ToTextFile:@"RouteByPlace.json"];
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_LOAD_ROUTE_SUCCESS])
    {
        NSData * JSONData = [NSJSONSerialization dataWithJSONObject:[notification object] options:kNilOptions error:nil];
        [[mSingleton getFileManagerInstance] writeContents:JSONData ToTextFile:@"Route.json"];
        [[mSingleton getWebScrapingManagerInstance] fetchRouteInfoDetails];
    }
    if ([[notification name] isEqualToString:NOTIFICATION_LOAD_ROUTE_INFO_SUCCESS])
    {
        NSData * JSONData = [NSJSONSerialization dataWithJSONObject:[notification object] options:kNilOptions error:nil];
        [[mSingleton getFileManagerInstance] writeContents:JSONData ToTextFile:@"RouteInfo.json"];
    }
}
@end
